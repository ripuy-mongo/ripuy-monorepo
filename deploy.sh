#!/bin/bash


echo ">> Building and deploy services..."

docker-compose up -d

OK=1
while [[ $OK -eq 1 ]]; do
  docker-compose ps --status running | grep mongo
  sleep 5
  if [[ $? -eq 0 ]]; then
    docker-compose exec mongo bash -c "source ./launchLocalDB.sh && launch"
    OK=0
  fi
done;

echo ">> Build success!."
echo "------------------------------------------------------------------"
echo "NOTE: Add the next domains in the /etc/hots file:"
echo "    - dev.ripuy.edu.co"
echo "These domains must point to localhost(27.0.0.1)"
echo "------------------------------------------------------------------"