deploy:
	./deploy.sh launch

restore.db:
	@echo ">> Restoring repuy-udenar db..."
	docker-compose exec mongo bash -c "source ./launchLocalDB.sh && restoreDB"