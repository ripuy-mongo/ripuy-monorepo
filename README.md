# Manual Del Programador Ripuy

El desarrollo del aplicativo Ripuy fue dividido en 3 paquetes que se encuentran debidamente documentados en los siguientes links:

[Paquete Request](https://www.notion.so/Paquete-Request-c0feb7d3a3f74ed093db9c29ce20bf7f)

[Paquete API](https://www.notion.so/Paquete-API-6bad08c9f2c34089b4dc26c428fbc530)

El aplicativo siguió el patrón de diseño arquitectónico cliente-servidor, donde el cliente se comunica al servidor para realizar las peticiones de información y el servidor devolver esta para que el cliente renderise su contenido. El servidor en resumidas palabras se encarga de la gestión de la información y  el procesamiento de la misma. El cliente es la parte visual, la inter-fase con la cual interactúan los usuario final.

## Estructura de los directorios

```bash
.
├── db
│   ├── backup
│   ├── data
│   ├── launchLocalDB.sh
│   ├── launchTestDB.sh
│   └── mongo-init.js
├── deploy.sh
├── docker-compose.yaml
├── lerna.json
├── Makefile
├── nginx
│   ├── certs
│   ├── conf.d
│   ├── nginx.conf
│   ├── README.md
│   └── upstream
├── package.json
├── packages
│   ├── api
│   ├── request
│   └── web
└── README.md
```

## Despliegue local

### Requerimientos

- Tener instalado **Docker** y **docker-compose,** seguir la siguiente [documentación](https://docs.docker.com/engine/install/)
- Agregar el dominio [dev.ripuy.edu.co](http://dev.ripuy.edu.co) al archivo hosts apuntando a [localhost](http://localhost) o 127.0.0.1 (en linux ubicado en /etc/hosts).
- **make** para ejecutar scripts (instalado por defecto en sistemas Unix/Linux)

### Configuración

Para efectos de realizar pruebas en un entorno local se cuenta con una configuración para desplegar todos los recursos necesario para tener el aplicativo funcionando con data de prueba. 

Todos estos servicios se orquestados con contenedores de [Docker](https://www.docker.com/) haciendo uso de [docker-compose](https://docs.docker.com/compose/) para poder administrar todo mediante archivos manifiestos configurados. Para poder hacer el despliegue local es necesario tener instaladas las anteriores herramientas, esta instalación debe seguirse dependiendo del sistema operativo con el cual se cuente:

- [Instalación de docker](https://docs.docker.com/engine/install/)
- [Instalación de docker-compose](https://docs.docker.com/compose/install/)

Para comprobar que estén instalados correctamente ejecutar:

```bash
docker --version
# Out 
# Docker version 20.10.12, build e91ed5707e

docker-compose --version
# Out 
# Docker Compose version 2.3.3
```

Dentro de la estructura de los directorios se encuentran los archivos **docker-compose.yaml** que tiene las configuraciones de los servicios. Los servicios son :

- api: que despliega el API backend que gestiona la información y se conecta a la base de datos.
- web: es el cliente que renderiza las vistas.
- mongo: Es el gestor de base de datos MongoDB que contiene la data de prueba.
- nginx: Es el servidor web que sirve la pagina web y los redireccionamientos seguros por HTTPS con certificados SSL.

```yaml
version: '3.4'

services:
  api:
    container_name: ripuy-api
    image: node:14.5.0-alpine
    restart: unless-stopped
    environment:
      - NODE_ENV=offline
    deploy:
      resources:
        limits:
          cpus: "0.8"
          memory: "800M"
        reservations:
          cpus: "0.25"
          memory: "128M"
    working_dir: /home/node/api
    command: "yarn offline"
    ports:
      - 8180:8180
    volumes:
      - ./packages/api:/home/node/api
    depends_on:
      - mongo
    networks:
      - backend

  web:
    container_name: ripuy-web
    image: node:14.5.0-alpine
    working_dir: /home/node/web
    restart: unless-stopped
    command: "yarn offline"
    environment:
      - NODE_ENV=offline
    ports:
      - 3000:3000
    volumes:
      - ./:/home/node/web
    depends_on:
      - api

  mongo:
    image: mongo:4.2.9
    container_name: mongo
    command: mongod --serviceExecutor adaptive
    restart: always
    volumes:
      - ./db/data:/data/db
      - ./db/launchLocalDB.sh:/launchLocalDB.sh
      - ./db/backup/ripuy-test:/dump/ripuy-udenar
      - ./db/mongo-init.js:/docker-entrypoint-initdb.d/mongo-init.js
    environment:
      MONGO_INITDB_ROOT_USERNAME: admin
      MONGO_INITDB_ROOT_PASSWORD: M0ng048
      MONGO_INITDB_DATABASE: ripuy-udenar
    networks:
      - backend

  nginx:
    container_name: nginx-local
    image: nginx:latest
    volumes:
      - ./nginx/conf.d:/etc/nginx/conf.d
      - ./nginx/nginx.conf:/etc/nginx/nginx.conf
      - ./nginx/certs:/etc/nginx/certs
      - ./nginx/upstream:/etc/nginx/upstream
    network_mode: "host"

volumes:
  nginx: ~

networks:
  backend:
```

### Desplegar los servicios

Para hacer el despliegue se debe ejecutar el comando:

```bash
make deploy
```

Este comando deberá desplegar todos los servicios y accediendo a [https://dev.ripuy.edu.co](https://dev.ripuy.edu.co) deberá renderizar el aplicativo web.

> Nota: los certificados SSL generado son de prueba por lo que el navegador los tomara como no validos. Para saltar este problema solo darle en avanzar, aceptar el riesgo y continuar.
> 

### Credenciales de ingreso

En el sistema se encuentran registrados varios usuarios de prueba, unos con rol administrador y otros con rol egresado.

Usuario Administrador: 

- identificación: 123456789
- contraseña: Ripuy123

Usuario Egresado

- identificación: 100000000x (La x puede ser remplazada por cualquier dígito, cada combinación es un usuario diferente.)
- contraseña: Ripuy123

### Restauración de la base de datos

Si en algún momento se quiere restaurar la base de datos nuevamente con los datos de prueba inicial ejecutar el comando:

```bash
make restore.db
```

### Visualización de logs

Par poder monitorear y ver los logs de los servicios se ejecuta:

```bash
# Todos los logs
docker-compose logs -f

# Un servicio en especifico (api, web, mongo, nginx) 
docker-compose logs -f api
```

### Bajar los servicios

Para poder detener los servicio desplegados solo se ejecuta el siguiente comando:

```bash
docker-compose down
```