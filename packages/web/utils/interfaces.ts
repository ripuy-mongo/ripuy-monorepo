export interface ResponseLogin {
    code: number;
    code_grant: string;
    first_login?: boolean;
}

export interface OffersFilters {
    text?: string;
}

export enum UserRole {
    GRADUATE = 'GRADUATE',
    ADMIN = 'ADMIN'
}

export enum AdminPostMenuAction {
    UpdatePost = 'UpdatePost',
    DeletePost = 'DeletePost',
    InterestedList = 'InterestedList'
}

export enum AdminEventMenuAction {
    UpdatePost = 'UpdatePost',
    DeletePost = 'DeletePost',
}

export interface OfferMenuItems {
    icon?: string, title: string, action?: AdminPostMenuAction
}

export interface EventMenuItems {
    icon?: string, title: string, action?: AdminEventMenuAction
}

export interface MenuOptions {
    name: string;
    path?: string;
    id?: string;
    icon?: string;
    event?: string;
    tab?: boolean;
    show?: boolean;
}

export interface User {
    _id: string;
    role: string;
    identificacion: string;
    tipo_identificacion?: string;
    pais_expedicion: any;
    nombres: string;
    apellidos?: string;
    genero?: string;
    fecha_nacimiento?: Date;
    correo?: string;
    celular?: string;
    permiso?: boolean;
    primer_ingreso?: boolean;
}
