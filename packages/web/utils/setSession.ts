import { User } from '@/utils/interfaces';

export async function setSession ( store: any ): Promise<User | null> {
    const resp = await fetch( `${process.env.API_SERVER}/user`, {
        credentials: 'include'
    } );
    if ( resp.status === 200 ) {
        const user: User = await resp.json();
        store.commit( 'sessionStorage/setUser', user );
        return Promise.resolve( user );
    }
    store.commit( 'sessionStorage/setUser', null );
    return Promise.resolve( null );
}
