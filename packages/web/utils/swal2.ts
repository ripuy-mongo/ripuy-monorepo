export const displayAlert = ( type: 'warning'|'error'|'info'|'success', text: string, timer: number ) => {
    // @ts-ignore
    Swal.fire( {
        toast: true,
        text: `${text}`,
        position: 'bottom-start',
        showConfirmButton: false,
        showCloseButton: true,
        timer,
        timerProgressBar: true,
        customClass: {
            popup: `ripuy-popup ripuy-popup-${type}`,
            icon: 'ripuy-icon',
            cancelButton: 'ripuy-cancelButton',
            content: 'ripuy-content',
            container: 'ripuy-container',
            header: 'ripuy-header'
        }
    } );
};
export const displayPopUp = ( icon: 'warning'|'error'|'info'|'success', text: string, confirmButtonText: string ) => {
    // @ts-ignore
    Swal.fire( {
        text: `${text}`,
        icon: `${icon}`,
        showConfirmButton: true,
        confirmButtonText,
        showCloseButton: true,
        customClass: {
            cancelButton: 'ripuy-cancelButton'
        }
    } );
};
