import { User } from '@/utils/interfaces';

declare interface LocalStorage {
    user?: User | null;
}

export const state = (): LocalStorage => ( {
    user: null
} );

export const mutations = {
    setUser ( state: LocalStorage, user: User ) {
        state.user = user;
    }
};

export const getters = {
    getUser ( state: LocalStorage ) {
        return state.user;
    },
    getUserId ( state: LocalStorage ) {
        return state.user?._id || '';
    }
};
