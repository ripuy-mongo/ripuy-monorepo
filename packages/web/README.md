# Manual Del Programador

# Paquete Web

Este documento contiene la información de la estructura y construcción del Front-end del aplicativo Ripuy donde se utilizo el framework Nuxt JS, el CLI universal de vueJS [NuxtJS](https://nuxtjs.org/).

Es importante tener en cuenta que la estructura de este paquete es recreado por defecto como lo establece NuxtJS es decir una vez se crea un proyecto NuxtJS Por defecto siempre va tener el mismo orden de directorios y lo archivos existentes necesarios para la ejecución de la aplicación.

Este paquete se encuentra ubicado en el directorio ***packages/web*** y contiene la siguiente estructura de directorios:

```bash
packages/web
├── assets
├── components
│   ├── commons
│   ├── events
│   ├── header
│   ├── login
│   └── posts
├── layouts
├── pages
│   ├── profile
│   └── restore
├── plugins
├── static
├── store
└── utils
```
En assets se tiene todo lo que tiene que ver con archivos multimedia y la paleta de colores que va tener establecido el proyecto, también se tiene aquí variables globales css y los media query que se establecen de manera general para toda la aplicación.

Como podemos notar existen varios directorios de componentes donde se implementaron varios componentes para luego ser usados en las pages, los componentes es una parte importante del proyecto ya que esto contribuye a la reutilización de código y que el mantenimiento de este sea 
más facil.

En layouts se encuentran dos archivos que se consideran van estar trabajando en todas y cada una de las paginas es por esto que el archivo default.vue contiene el header, footer entre otros ya que es código que se va usar a lo largo de todo el proyecto.

Las pages renderizan cada una de las rutas que se tiene establecidas, aquí también se lleva a cabo las peticiones a las queries y mutations necesarias para cada una de las vistas.

En plugins se tiene archivos de configuración que son luego usados en nuxt.config.ts, que traen librerias externas o componentes externos para ser usados, como es el caso de scroll.ts que es el archivo que contiene la importación de un scroll de tipo vertical que se está usando en las vistas como publicaciones o eventos.

En store se tiene el LocalStorage definido que es el que se usa a lo largo de toda la aplicación para que la data no se pierda y se mantenga en el tiempo, en este caso la data del usuario.


## Stack

Para la construcción del frontend RIPUY se uso el siguiente stask de tecnologías:

- [JavaScript](https://www.javascript.com/): Es el lenguaje de programación interpretado tanto del lado del cliente como del servidor más usado en el desarrollo web.
- [Webpack](https://webpack.js.org/): Es un empaquetador de módulos para JavaScript que permite optimizar los recursos y empaquetar todo el contenido de una aplicación en archivos más simples.
- [Typescript](https://www.typescriptlang.org/): Es un lenguaje que agrega sintaxis adicional a JavaScript para admitir una integración más estrecha con su editor. Detecte errores temprano en su editor.
- [GraphQL](https://graphql.org/): Es un lenguaje de consulta para API para cumplir con esas consultas con los datos existentes. Brinda a los clientes el poder de solicitar exactamente lo que necesitan y nada más, facilita la evolución de las API con el tiempo y habilita herramientas poderosas para desarrolladores.
- [NuxtJS](https://nuxtjs.org/): Que es un framework basado en vue.js  y escrito en javascript

## Instalación y configuración del entorno de desarrollo

### Instalación de NuxtJS

Inicialmente se instala [Yarn](https://yarnpkg.com/), que es una alternativa a NPM pero a diferencia esta permitirá gestionar workspaces. 
La guía de instalación la pueden encontrar [aquí](https://yarnpkg.com/getting-started/install) e igualmente como las anteriores instalaciones es posible comprobar su instalacion con el comando

```bash
yarn --version
# 1.22.17
```

Luego de esto se debe seguir la guía de instalación en la docuemtación de NuxtJS [aquí](https://nuxtjs.org/docs/get-started/installation)


### Instalación de dependencias

Ubicado en el directorio **packages/web/** ejecutar el comando

```bash
yarn install
```

Una vez todo este instalado ya es posible ejecutar el proyecto ejecutando el comando

```bash
# Para ejecutar el server en entorno de development, 
# mientras de esta desarrollando se iran refrescando los cambios
yarn dev

# Para ejecutar el server en entorno de staging
yarn start

# Para ejecutar el server en entorno de production
yarn prod
```

Esto deberá habilitar el servicio mediante [https://dev.ripuy.edu.co](https://dev.ripuy.edu.co) es claro que este host se debe configurar en el equipo que se va utilizar, en el caso de linux en la terminal se debe modificar el archivo /etc/hosts agregando el nuevo host.

### Variables de entorno

Para el correcto funcionamiento del web es importante que las variables de entorno este correctamente configuradas, estas contienen información de conexión a base de datos y otros importantes valores. Estas variables se encuentran dentro de la raiz del package web en los archivos **.env.dev** para desarrollo, **.env.off** y **.env.prod** para el entorno **production** 

## GraphQL
Ya definido en el readme del paquete api, en el paquete web hacemos el llamado a todas las queries  para abastecer de data la aplicación y mutaciones para hacer las peticiones  de creación, modificación y elimición de la data.
