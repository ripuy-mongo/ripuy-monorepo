import { APIClient } from 'ripuy-request';

declare module 'vue/types/vue' {
    interface Vue {
        $t: any;
        $authEvent: Vue;
        $apiClient: APIClient;
        $Swal: any;
        $SwalPopUp: any;
    }
}

declare type window = any;

declare module '*.svg' {
    const content: any;
    const VueComponent: any;
    export default content;
}
