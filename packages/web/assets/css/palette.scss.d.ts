interface CssExports {
    'primary': string;
    'secondary': string;
}

export const cssExports: CssExports;
export default cssExports;