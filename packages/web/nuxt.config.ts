// @ts-ignore
import colors from 'vuetify/es5/util/colors';
// @ts-ignore
import * as routes from './routes.json';
const ENVIRONMENT: any = {
    development: 'dev',
    production: 'prod',
    offline: 'off'
};

const ENV = `${process.env.NODE_ENV}`;

export default {
    target: 'server',

    head: {
        titleTemplate: '%s - ripuy',
        title: 'ripuy',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ],
        script: [
            { src: 'https://cdn.jsdelivr.net/npm/sweetalert2@10' },
            { src: 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css' },
            { src: 'https://kit.fontawesome.com/063a60c7bc.js', crossorigin: 'anonymous' },
            { src: "https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.10.1/html2pdf.bundle.min.js",
                integrity: "sha512-GsLlZN/3F2ErC5ifS5QtgpiJtWd43JWSuIgh7mbzZ8zBps+dvLusV+eNQATqgA/HdeKFVgA5v3S/cIrLF7QnIg==",
                crossorigin: "anonymous", referrerpolicy: "no-referrer"
            }

        ]
    },
    css: [
        "aos/dist/aos.css"
    ],
    styleResources: {
        scss: [
            '@/assets/css/palette.scss',
            '@/assets/css/variables.scss',
            '@/assets/css/global.scss'
        ]
    },

    plugins: [
        '@/plugins/utils',
        '@/plugins/scroll',
        { src: '@/plugins/apolloClient', ssr: false },
        { src: '@/plugins/moment', ssr: false },
        { src: '@/plugins/aos', ssr: false },
        { src: '@/plugins/vue3-html2pdf', ssr: false },
        { src: '@/plugins/vue-html2pdf', mode: 'client' },
        // { src: '@/plugins/html2pdf', ssr: false }
    ],

    // components: true,

    buildModules: [
        '@nuxt/typescript-build',
        '@nuxtjs/dotenv',
        '@nuxtjs/vuetify',
        '@nuxtjs/style-resources',
        [
            'nuxt-compress',
            {
                gzip: {
                    cache: true
                }
            }
        ]
    ],

    modules: [
        '@nuxtjs/style-resources',
        'nuxt-vuex-localstorage'
    ],
    dotenv: {
        filename: `.env.${ENVIRONMENT[ENV]}`
    },

    generate: {
        routes () {
            const routesId = process.argv.indexOf( '-r' );
            if ( routesId && routesId >= 0 ) {
                const routesList = process.argv[routesId + 1];
                return routesList ? routesList.split( ',' ) : [];
            }
            return routes.routes;
        },
        fallback: 'error.html',
        dir: './dist'
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                loader: 'ts-loader'
            }
        ]
    },

    vuetify: {
        customVariables: ['@/assets/css/variables.scss'],
        treeShake: true,
        defaultAssets: {
            font: {
                family: 'Open Sans'
            },
            icons: 'mdi'
        },
        theme: {
            dark: false,
            themes: {
                dark: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3
                }
            }
        }
    },

    build: {
        filenames: {
            css: ( { isDev }:any ) => isDev ? '[name].css' : '[contenthash].css'
        },
        extractCSS: {
            ignoreOrder: true
        },
        optimization: {
            splitChunks: {
                cacheGroups: {
                    styles: {
                        name: 'styles',
                        test: /\.(scss|vue)$/,
                        chunks: 'all',
                        enforce: true
                    }
                }
            }
        },
        extend ( config: any, { isDev, isClient }: { isDev: boolean, isClient: boolean } ) {
            if ( isDev && isClient && process.env.ESLINT ) {
                config.module.rules.push( {
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)|(commons)|(\.nuxt)|(\.svg$)/
                } );
            }
        },
        vendor: ["aos"]
    }
};
