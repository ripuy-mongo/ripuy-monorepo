import Vue from 'vue';

// import AOS from 'aos';

const AOS =  require( 'aos');

import 'aos/dist/aos.css';

export default({ app } : any, inject: any ) => {
    app.AOS = new AOS.init();
}