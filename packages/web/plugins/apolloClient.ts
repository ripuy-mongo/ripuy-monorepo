import Vue from 'vue';
import { APIClient, ApolloClientManager } from 'ripuy-request';
import VueApollo from 'vue-apollo';

export default function apolloClient ( vue: Vue, inject: any ) {
    const apolloClientManager = new ApolloClientManager( `${process.env.API_SERVER}/graphql` );
    const apolloProvider = new VueApollo( {
        defaultClient: apolloClientManager.getClient()
    } );
    const apiClient = new APIClient( apolloClientManager );
    inject( 'apolloProvider', apolloProvider );
    inject( 'apiClient', apiClient );
}
