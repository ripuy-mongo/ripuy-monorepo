import Vue from 'vue';
import moment from 'moment';

const Moment = {
    install ( Vue: any ) {
        moment.locale( navigator.language );
        Vue.prototype.$moment = moment;
        Vue.prototype.$offerDate = ( date: string ) => {
            return moment( date ).format( 'DD MMMM YYYY' );
        };
    }
};

export default () => {
    Vue.use( Moment );
};
