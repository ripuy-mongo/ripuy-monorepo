module.exports = {
    client: {
        service: {
            name: 'Ripuy',
            url: 'https://ripuy.charlitoro.com/api/graphql',
        },
        includes: [
            'src/queries/*.ts'
        ],
        excludes: [
            'node_modules/*'
        ],
        tagName: 'gql',
    }
};
