import { ApolloClientManager } from './ApolloClientManager';
import { Personal } from '../services/Personal';
import { Post } from '../services/Post';
import { Professional } from '../services/Professional';
import { Commons } from '../services/Commons';
import { Academic } from '../services/Academic';

class APIClient {
    clientManager: ApolloClientManager;
    _personal: Personal;
    _post: Post;
    _professional: Professional
    _commons: Commons
    _academic: Academic

    constructor ( clientManager: ApolloClientManager ) {
        this.clientManager = clientManager;
    }

    public get personal(): Personal {
        if ( !this._personal )
            this._personal = new Personal( this.clientManager );
        return this._personal;
    }

    public get post(): Post {
        if( !this._post )
            this._post = new Post( this.clientManager );
        return this._post;
    }

    public get professional(): Professional {
        if( !this._professional )
            this._professional = new Professional( this.clientManager );
        return this._professional;
    }

    public get commons(): Commons {
        if( !this._commons )
            this._commons = new Commons( this.clientManager );
        return this._commons;
    }

    public get academic(): Academic {
        if( !this._academic )
            this._academic = new Academic( this.clientManager );
        return this._academic;
    }
}

export {
    APIClient
};
