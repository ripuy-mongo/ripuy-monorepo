import { ApolloClient, NormalizedCacheObject, FetchPolicy } from "@apollo/client/core";

enum CLIENT {
    DEFAULT = 'default',
    IMPLICIT = 'implicit',
    CODE = 'code'
}

type FetchFunction = ( uri: string, options: any ) => Promise<any>;
type TokenFunction = () => string;

type ApolloClients = {
    [key in CLIENT]: {
        client: ApolloClient<NormalizedCacheObject>|null;
        fetch?: FetchFunction;
        credentials?: string;
        getToken?: TokenFunction;
    }
}

interface ApolloOptions {
    fetchPolicy?: FetchPolicy;
}

interface MutationApolloOptions {
    fetchPolicy?: Extract<FetchPolicy, 'no-cache'>;
}

interface ClientOptions {
    client?: CLIENT;
    apolloOptions?: ApolloOptions;
}

interface MutationClientOptions {
    client?: CLIENT;
    apolloOptions?: MutationApolloOptions;
}

export {
    ApolloClients,
    ApolloOptions,
    FetchFunction,
    TokenFunction,
    ClientOptions,
    MutationClientOptions,
    CLIENT
};
