import { ApolloClients, CLIENT, FetchFunction, TokenFunction } from './interfaces';
import { HttpLink, InMemoryCache, NormalizedCacheObject, ApolloClient } from '@apollo/client/core';
import { setContext } from '@apollo/client/link/context';

class ApolloClientManager {
    apolloClients: ApolloClients = {
        [CLIENT.DEFAULT]: {
            client: null
        },
        [CLIENT.IMPLICIT]: {
            client: null
        },
        [CLIENT.CODE]: {
            client: null,
            credentials: 'include'
        }
    };
    cache: InMemoryCache;
    uri: string;

    //TODO: Make getToken a string not a function?
    constructor ( uri: string, getToken?: TokenFunction, fetch?: FetchFunction ) {
        if( !uri ) {
            throw new Error( 'URI is empty' );
        }
        this.uri = uri;
        this.cache = new InMemoryCache( { addTypename: false } );
        this.apolloClients[CLIENT.DEFAULT].fetch = fetch;
        this.apolloClients[CLIENT.IMPLICIT].fetch = fetch;
        this.apolloClients[CLIENT.IMPLICIT].getToken = getToken;
        this.apolloClients[CLIENT.CODE].fetch = fetch;
    }

    getClient ( name: CLIENT = CLIENT.DEFAULT, ssrMode: boolean = false ): ApolloClient<NormalizedCacheObject> {
        if ( !this.apolloClients[name].client ) {
            let authLink: any;
            if ( this.apolloClients[name].getToken ) {
                const getToken = this.apolloClients[name].getToken as TokenFunction;
                authLink = setContext( ( _, { headers } ) => {
                    const token: string = getToken();
                    return {
                        headers: {
                            ...headers,
                            Authorization: token
                        }
                    };
                } );
            }
            const httpLink = new HttpLink( {
                uri: this.uri,
                fetch: this.apolloClients[name].fetch,
                credentials: this.apolloClients[name].credentials || 'same-origin',
                fetchOptions: {
                    credentials: this.apolloClients[name].credentials || 'same-origin'
                }
            } );
            this.apolloClients[name].client = new ApolloClient( {
                link: authLink ? authLink.concat( httpLink ) : httpLink,
                cache: this.cache,
                ssrMode
            } );
        }
        return this.apolloClients[name].client as ApolloClient<NormalizedCacheObject>;
    }
}

export {
    ApolloClientManager
};
