import { Service } from './Service';
import * as queries from '../queries/personal';
import * as Types from '../queries/types';
import { ClientOptions, MutationClientOptions } from '../client/interfaces';

class Personal extends Service {

    // Queries
    clasesEtnograficas( clientOptions?: ClientOptions ): Promise<Types.clasesEtnograficas>{
        return this.doQuery<Types.clasesEtnograficas, undefined>(
            queries.clasesEtnograficas, undefined, clientOptions
        );
    }

    claseEtnografica( variables: Types.claseEtnograficaVariables, clientOptions?: ClientOptions ): Promise<Types.claseEtnografica>{
        return this.doQuery<Types.claseEtnografica, Types.claseEtnograficaVariables>(
            queries.claseEtnografica, variables, clientOptions
        );
    }

    discapacidades( clientOptions?: ClientOptions ): Promise<Types.discapacidades>{
        return this.doQuery<Types.discapacidades, undefined>(
            queries.discapacidades, undefined, clientOptions
        );
    }

    informacionPersonal( variables: Types.informacionPersonalVariables, clientOptions?: ClientOptions ): Promise<Types.informacionPersonal>{
        return this.doQuery<Types.informacionPersonal, Types.informacionPersonalVariables>(
            queries.informacionPersonal, variables, clientOptions
        );
    }

    egresado( variables: Types.egresadoVariables, clientOptions?: ClientOptions ): Promise<Types.egresado>{
        return this.doQuery<Types.egresado, Types.egresadoVariables>(
            queries.egresado, variables, clientOptions
        );
    }
    
    setPermisos( variables: Types.setPermisosVariables, clientOptions?: MutationClientOptions ): Promise<Types.setPermisos>{
        return this.doMutation<Types.setPermisos, Types.setPermisosVariables>( 
            queries.setPermisos, variables, clientOptions
        );
    }

    changeFirstLoginStatus( clientOptions?: MutationClientOptions ): Promise<Types.changeFirstLoginStatus>{
        return this.doMutation<Types.changeFirstLoginStatus, undefined>(
            queries.changeFirstLoginStatus, undefined, clientOptions
        );
    }

    updateInformacionPersonal( variables: Types.updateInformacionPersonalVariables, clientOptions?: MutationClientOptions ): Promise<Types.updateInformacionPersonal>{
        return this.doMutation<Types.updateInformacionPersonal, Types.updateInformacionPersonalVariables>(
            queries.updateInformacionPersonal, variables, clientOptions
        );
    }

}

export { Personal };
