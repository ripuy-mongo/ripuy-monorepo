import { ApolloClientManager } from '../client/ApolloClientManager';
import { CLIENT, ClientOptions, MutationClientOptions } from '../client/interfaces';
import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { DocumentNode } from 'graphql';

class Service {
    protected clientManager: ApolloClientManager;

    constructor ( clientManager: ApolloClientManager ) {
        this.clientManager = clientManager;
    }

    protected getClient( options?: ClientOptions, defaultClient: CLIENT = CLIENT.IMPLICIT ): ApolloClient<NormalizedCacheObject> {
        return this.clientManager.getClient( options?.client || defaultClient );
    }

    protected async doQuery<T, U> (
        query: any,
        variables?: U,
        clientOptions?: ClientOptions
    ): Promise<T> {
        const client = this.getClient( clientOptions );
        const resp = await client.query( { query, variables, ...clientOptions?.apolloOptions } );
        return Service.getResponse<T>( resp );
    }

    protected async doMutation<T, U> (
        mutation: any,
        variables?: U,
        clientOptions?: MutationClientOptions
    ): Promise<T> {
        const client = this.getClient( clientOptions );
        const resp = await client.mutate( { mutation, variables, ...clientOptions?.apolloOptions } );
        return Service.getResponse<T>( resp );
    }

    private static getResponse<T>( response: any ): T {
        if ( response.data ) {
            return response.data;
        }
        throw response;
    }
}

export {
    Service
};
