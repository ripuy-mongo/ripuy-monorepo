import { Service } from './Service';
import * as queries from '../queries/commons';
import * as Types from '../queries/types';
import { ClientOptions } from '../client/interfaces';

class Commons extends Service {
    paises( clientOptions?: ClientOptions ): Promise<Types.paises> {
        return this.doQuery<Types.paises, undefined>(
            queries.paises, undefined, clientOptions
        );
    }

    municipios( clientOptions?: ClientOptions ): Promise<Types.municipios> {
        return this.doQuery<Types.municipios, undefined>(
            queries.municipios, undefined, clientOptions
        );
    }
}

export { Commons };
