import { Service } from './Service';
import * as queries from '../queries/professional';
import * as Types from '../queries/types';
import { ClientOptions, MutationClientOptions } from '../client/interfaces';

class Professional extends Service {

    empresas( variables?: Types.empresasVariables, clientOptions?: ClientOptions ): Promise<Types.empresas>{
        return this.doQuery<Types.empresas, Types.empresasVariables>(
            queries.empresas, variables, clientOptions
        );
    }

    empresa( variables: Types.empresaVariables, clientOptions?: ClientOptions ): Promise<Types.empresa>{
        return this.doQuery<Types.empresa, Types.empresaVariables>(
            queries.empresas, variables, clientOptions
        );
    }

    actividadesActuales( clientOptions?: ClientOptions ): Promise<Types.actividadesActuales>{
        return this.doQuery<Types.actividadesActuales, undefined>(
            queries.actividadesActuales, undefined, clientOptions
        );
    }

    canalesComunicacion( clientOptions?: ClientOptions ): Promise<Types.canalesComunicacion>{
        return this.doQuery<Types.canalesComunicacion, undefined>(
            queries.canalescomunicacion, undefined, clientOptions
        );
    }

    razonesDificultadTrabajo( clientOptions?: ClientOptions ): Promise<Types.razonesDificultadTrabajo>{
        return this.doQuery<Types.razonesDificultadTrabajo, undefined>(
            queries.razonesDificultadTrabajo, undefined, clientOptions
        );
    }

    informacionLaboral( variables: Types.informacionLaboralVariables, clientOptions?: ClientOptions ): Promise<Types.informacionLaboral>{
        return this.doQuery<Types.informacionLaboral, Types.informacionLaboralVariables>(
            queries.informacionLaboral, variables, clientOptions
        );
    }

    informacionNoLaboral( variables: Types.informacionNoLaboralVariables, clientOptions?: ClientOptions ): Promise<Types.informacionNoLaboral>{
        return this.doQuery<Types.informacionNoLaboral, Types.informacionNoLaboralVariables>(
            queries.informacionNoLaboral, variables, clientOptions
        );
    }

    updateInformacionLaboral( variables: Types.updateInformacionLaboralVariables, clientOptions?: MutationClientOptions ): Promise<Types.updateInformacionLaboral>{
        return this.doMutation<Types.updateInformacionLaboral, Types.updateInformacionLaboralVariables>(
            queries.updateInformacionLaboral, variables, clientOptions
        );
    }

    updateInformacionNoLaboral( variables: Types.updateInformacionNoLaboralVariables, clientOptions?: MutationClientOptions ): Promise<Types.updateInformacionNoLaboral>{
        return this.doMutation<Types.updateInformacionNoLaboral, Types.updateInformacionNoLaboralVariables>(
            queries.updateInformacionNoLaboral, variables, clientOptions
        );
    }
}

export { Professional };
