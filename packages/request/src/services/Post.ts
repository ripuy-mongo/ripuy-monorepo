import { Service } from './Service';
import * as queries from '../queries/post';
import * as Types from '../queries/types';
import { ClientOptions, MutationClientOptions } from '../client/interfaces';

class Post extends Service {

    categorias( clientOptions?: ClientOptions ): Promise<Types.categorias>{
        return this.doQuery<Types.categorias, undefined>(
            queries.categorias, undefined, clientOptions
        );
    }

    eventos( clientOptions?: ClientOptions ): Promise<Types.eventos>{
        return this.doQuery<Types.eventos, undefined>(
            queries.eventos, undefined, clientOptions
        );
    }

    ofertas( variables: Types.ofertasVariables, clientOptions?: ClientOptions ): Promise<Types.ofertas>{
        return this.doQuery<Types.ofertas, Types.ofertasVariables>(
            queries.ofertas, variables, clientOptions
        );
    }

    oferta( variables: Types.ofertaVariables, clientOptions?: ClientOptions ): Promise<Types.oferta>{
        return this.doQuery<Types.oferta, Types.ofertaVariables>(
            queries.ofertas, variables, clientOptions
        );
    }

    interesados( variables: Types.interesadosVariables, clientOptions?: ClientOptions ): Promise<Types.interesados>{
        return this.doQuery<Types.interesados, Types.interesadosVariables>(
            queries.interesados, variables, clientOptions
        );
    }

    egresados( variables?: Types.egresadosVariables, clientOptions?: ClientOptions ): Promise<Types.egresados>{
        return this.doQuery<Types.egresados, Types.egresadosVariables>(
            queries.egresados, variables, clientOptions
        );
    }

    createOferta( variables: Types.createOfertaVariables, clientOptions?: MutationClientOptions ): Promise<Types.createOferta>{
        return this.doMutation<Types.createOferta, Types.createOfertaVariables>(
            queries.createOferta, variables, clientOptions
        );
    }

    updateOferta( variables: Types.updateOfertaVariables, clientOptions?: MutationClientOptions ): Promise<Types.updateOferta>{
        return this.doMutation<Types.updateOferta, Types.updateOfertaVariables>(
            queries.updateOferta, variables, clientOptions
        );
    }

    deleteOferta( variables: Types.deleteOfertaVariables, clientOptions?: MutationClientOptions ):  Promise<Types.deleteOferta>{
        return this.doMutation<Types.deleteOferta, Types.deleteOfertaVariables>(
            queries.deleteOferta, variables, clientOptions
        );
    }

    createInteresado( variables: Types.createInteresadoVariables, clientOptions?: MutationClientOptions ): Promise<Types.createInteresado>{
        return this.doMutation<Types.createInteresado, Types.createInteresadoVariables>(
            queries.createInteresado, variables, clientOptions
        );
    }

    deleteInteresado( variables: Types.deleteInteresadoVariables, clientOptions?: MutationClientOptions ): Promise<Types.deleteInteresado>{
        return this.doMutation<Types.deleteInteresado, Types.deleteInteresadoVariables>(
            queries.deleteInteresado, variables, clientOptions
        );
    }

    createEvento( variables: Types.createEventoVariables, clientOptions?: MutationClientOptions ): Promise<Types.createEvento>{
        return this.doMutation<Types.createEvento, Types.createEventoVariables>(
            queries.createEvento, variables, clientOptions
        );
    }

    updateEvento( variables: Types.updateEventoVariables, clientOptions?: MutationClientOptions ): Promise<Types.updateEvento> {
        return this.doMutation<Types.updateEvento, Types.updateEventoVariables>(
            queries.updateEvento, variables, clientOptions
        );
    }

    deleteEvento( variables: Types.deleteEventoVariables, clientOptions?: MutationClientOptions ): Promise<Types.deleteEvento>{
        return this.doMutation<Types.deleteEvento, Types.deleteEventoVariables>(
            queries.deleteEvento, variables, clientOptions
        );
    }
}

export { Post };
