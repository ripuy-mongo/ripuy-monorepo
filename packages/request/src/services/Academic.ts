import { Service } from './Service';
import * as queries from '../queries/academic';
import * as Types from '../queries/types';
import { ClientOptions, MutationClientOptions } from '../client/interfaces';

class Academic extends Service {
    colegios( clientOptions?: ClientOptions ): Promise<Types.colegios> {
        return this.doQuery<Types.colegios, undefined>(
            queries.colegios, undefined, clientOptions
        );
    }

    motivosIngreso( clientOptions?: ClientOptions ): Promise<Types.motivosIngreso>{
        return this.doQuery<Types.motivosIngreso, undefined>(
            queries.motivosIngreso, undefined, clientOptions
        );
    }

    formasFinanciacion( clientOptions?: ClientOptions ): Promise<Types.formasFinanciacion>{
        return this.doQuery<Types.formasFinanciacion, undefined>(
            queries.formasFinanciacion, undefined, clientOptions
        );
    } 

    academicFormData( clientOptions: ClientOptions ):Promise<Types.academicFormData> {
        return this.doQuery<Types.academicFormData, undefined>(            queries.academicFormData, undefined, clientOptions
        );
    }

    informacionAcademica( variables: Types.informacionAcademicaVariables, clientOptions: ClientOptions ): Promise<Types.informacionAcademica>{
        return this.doQuery<Types.informacionAcademica, Types.informacionAcademicaVariables>(
            queries.informacionAcademica, variables, clientOptions
        );
    }

    infoAcademicaAdmin( clientOptions: ClientOptions ): Promise<Types.infoAcademicaAdmin> {
        return this.doQuery<Types.infoAcademicaAdmin, undefined>( queries.infoAcademicaAdmin, undefined, clientOptions );
    }

    updateInformacionAcademica( variables: Types.updateInformacionAcademicaVariables, clientOptions?: MutationClientOptions ): Promise<Types.updateInformacionAcademica>{
        return this.doMutation<Types.updateInformacionAcademica, Types.updateInformacionAcademicaVariables>(
            queries.updateInformacionAcademica, variables, clientOptions
        );
    }
}

export { Academic };