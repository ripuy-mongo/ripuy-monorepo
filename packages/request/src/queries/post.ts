import gql from "graphql-tag";

// Queries
const categorias = gql`
    query categorias{
        categorias{ id nombre icon }
    }
`;

const eventos = gql`
    query eventos{
        eventos{
            id nombre descripcion poster direccion telefono email url fechas
        }
    }
`;

const ofertas = gql`
    query ofertas( $take: Int $cursor: CursorPaginationInput $where: OfertaWhereInput){
        ofertas( take:$take cursor:$cursor where:$where){
            id descripcion telefono correo fecha_publicacion
            categoria{id nombre icon}
            solicitante{ nombre telefono correo }
            aplicado
        }
    }
`;

const oferta = gql`
    query oferta($where: OfertaWhereUniqueInput!){
        oferta(where:$where){
            id descripcion telefono correo fecha_publicacion
            categoria{id nombre id}
            solicitante{ nombre telefono correo }
            aplicado
        }
    }
`;

const interesados = gql`
    query interesados( $skip: Int $take: Int $where: InteresadosWhereInput!){
        interesados( skip:$skip take:$take where: $where){
            totalCount
            interesados {
                fecha_aplicacion
                user {
                    id identificacion tipo_identificacion
                    pais_expedicion{id codigo codigo_alfanumerico}
                    nombres apellidos genero fecha_nacimiento
                    correo celular permiso
                }
            }
        }
    }
`;

const egresados = gql`
    query egresados( $take: Int $cursor: CursorPaginationInput $where: EgresadoWhereInput ){
        egresados( take:$take cursor:$cursor where: $where ){
            id identificacion tipo_identificacion
            pais_expedicion{id codigo codigo_alfanumerico}
            nombres apellidos genero fecha_nacimiento
            correo celular permiso
        }
    }
`;

// Mutations

const createOferta = gql`
    mutation createOferta($data: CreateOfertaDataInput!){
        createOferta(data:$data){
            id descripcion telefono correo fecha_publicacion
            categoria{id nombre icon}
            solicitante{ nombre telefono correo }
        }
    }
`;

const updateOferta = gql`
    mutation updateOferta($where: OfertaWhereUniqueInput! $data: UpdateOfertaDataInput!){
        updateOferta(where:$where data:$data){
            id descripcion telefono correo fecha_publicacion
            categoria{id nombre icon}
            solicitante{nombre telefono correo}
        }
    }
`;

const deleteOferta = gql`
    mutation deleteOferta($where: OfertaWhereUniqueInput!){
        deleteOferta(where:$where){ id }
    }
`;

const createInteresado = gql`
    mutation createInteresado($data:CreateInteresadoDataInput!){
        createInteresado(data:$data){ id }
    }
`;
const deleteInteresado = gql`
    mutation deleteInteresado( $where: DeleteInteresadoWhereInput! ){
      deleteInteresado(where: $where){ id }
    }
`;

const createEvento = gql`
    mutation createEvento($data: CreateEventoDataInput!){
        createEvento(data: $data ){
            id nombre poster descripcion direccion telefono email url
        }
    }
`;

const updateEvento = gql`
    mutation updateEvento($where: EventoWhereInput! $data: UpdateEventoWhereInput!){
        updateEvento( where: $where data: $data){
            id nombre poster descripcion direccion telefono email url
        }
    }
`;

const deleteEvento = gql`
    mutation deleteEvento($where: EventoWhereInput!){
        deleteEvento(where: $where){
            id
        }
    }
`;

export {
    categorias,
    eventos,
    ofertas,
    oferta,
    interesados,
    egresados,
    createOferta,
    updateOferta,
    deleteOferta,
    createInteresado,
    deleteInteresado,
    createEvento,
    updateEvento,
    deleteEvento
};
