import gql from "graphql-tag";

// Queries
const clasesEtnograficas = gql`
    query clasesEtnograficas{
        clasesEtnograficas{ id nombre }
    }
`;

const claseEtnografica = gql`
    query claseEtnografica($where: ClaseEtnograficaWhereUniqueInput!){
        claseEtnografica(where: $where){id nombre}
    }
`;

const discapacidades = gql`
    query discapacidades{
        discapacidades{ id nombre grupo{id nombre} }
    }
`;

const informacionPersonal = gql`
    query informacionPersonal( $where: UserWhereUniqueInput! ){
        informacionPersonal(where: $where){
            id municipio_recidencia{id codigo nombre}
            municipio_nacimiento{id codigo nombre}
            clase_etnografica{id nombre}
            tiene_discapacidad discapacidad{id nombre}
            tiene_discapacidad
            motivo_discapacidad
        }
    }
`;

const egresado = gql`
query egresado( $where: UserWhereUniqueInput! ){
    egresado(where: $where){
        id identificacion tipo_identificacion
        pais_expedicion{id codigo codigo_alfanumerico}
        nombres apellidos genero fecha_nacimiento
        correo celular permiso
    }
}
`;

// Mutations
const updateInformacionPersonal = gql`
    mutation updateInformacionPersonal(
        $where: UpdateInformacionPersonalWhereInput!
        $data: UpdateInformacionPersonalDataInput!
    ){
        updateInformacionPersonal(where: $where data: $data ){
            id identificacion tipo_identificacion pais_expedicion{id codigo nombre codigo_alfanumerico} nombres
            apellidos genero fecha_nacimiento correo celular permiso
        }
    }
`;

const changeFirstLoginStatus = gql`
    mutation changeFirstLoginStatus{
        changeFirstLoginStatus{ status }
    }
`;

const setPermisos = gql`
    mutation setPermisos($data: SetPermisoData!){
      setPermisos(data: $data){ allowed }
    }
`;

export {
    claseEtnografica,
    clasesEtnograficas,
    discapacidades,
    updateInformacionPersonal,
    informacionPersonal,
    changeFirstLoginStatus,
    setPermisos,
    egresado
};
