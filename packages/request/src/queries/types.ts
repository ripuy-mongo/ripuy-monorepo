/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: colegios
// ====================================================

export interface colegios_colegios {
  __typename: "Colegio";
  id: string;
  nombre: string;
  codigo: string | null;
}

export interface colegios {
  colegios: colegios_colegios[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: motivosIngreso
// ====================================================

export interface motivosIngreso_motivosIngreso {
  __typename: "MotivoIngreso";
  id: string;
  descripcion: string;
}

export interface motivosIngreso {
  motivosIngreso: motivosIngreso_motivosIngreso[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: formasFinanciacion
// ====================================================

export interface formasFinanciacion_formasFinanciacion {
  __typename: "Financiacion";
  id: string;
  descripcion: string;
}

export interface formasFinanciacion {
  formasFinanciacion: formasFinanciacion_formasFinanciacion[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: academicFormData
// ====================================================

export interface academicFormData_formasFinanciacion {
  __typename: "Financiacion";
  id: string;
  descripcion: string;
}

export interface academicFormData_motivosIngreso {
  __typename: "MotivoIngreso";
  id: string;
  descripcion: string;
}

export interface academicFormData_colegios_municipio {
  __typename: "Municipio";
  nombre: string;
  id: string;
}

export interface academicFormData_colegios {
  __typename: "Colegio";
  id: string;
  codigo: string | null;
  nombre: string;
  calendario: SchoolCalendarType | null;
  naturaleza: SchoolType | null;
  municipio: academicFormData_colegios_municipio | null;
}

export interface academicFormData {
  sedes: IesCampus[] | null;
  modalidades: ModalidadAcademica[] | null;
  periodos: PeriodoAcademico[] | null;
  jornadas: JornadaAcademica[] | null;
  formasFinanciacion: academicFormData_formasFinanciacion[] | null;
  motivosIngreso: academicFormData_motivosIngreso[] | null;
  colegios: academicFormData_colegios[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: informacionAcademica
// ====================================================

export interface informacionAcademica_informacionAcademica_programa {
  __typename: "Programa";
  id: string;
  codigo: string;
  nombre: string;
}

export interface informacionAcademica_informacionAcademica_colegio {
  __typename: "Colegio";
  id: string;
  nombre: string;
}

export interface informacionAcademica_informacionAcademica_motivo_ingreso {
  __typename: "MotivoIngreso";
  id: string;
  descripcion: string;
}

export interface informacionAcademica_informacionAcademica_financiacion {
  __typename: "Financiacion";
  id: string;
  descripcion: string;
}

export interface informacionAcademica_informacionAcademica {
  __typename: "Academico";
  id: string;
  fecha_registro: any | null;
  sede: IesCampus | null;
  programa: informacionAcademica_informacionAcademica_programa | null;
  colegio: informacionAcademica_informacionAcademica_colegio | null;
  modalidad_academica: ModalidadAcademica | null;
  motivo_ingreso: informacionAcademica_informacionAcademica_motivo_ingreso | null;
  es_primera_opcion: boolean | null;
  intentos_admision: number | null;
  fecha_inicio_programa: any | null;
  fecha_finalizacion_materias: any | null;
  fecha_grado: any | null;
  periodo: PeriodoAcademico | null;
  jornada: JornadaAcademica | null;
  trabajo_grado: DegreeWorkType | null;
  titulo_trabajo_grado: string | null;
  linea_investigacion: string | null;
  financiacion: informacionAcademica_informacionAcademica_financiacion | null;
}

export interface informacionAcademica {
  informacionAcademica: informacionAcademica_informacionAcademica | null;
}

export interface informacionAcademicaVariables {
  where: InformacionAcademicaWhereInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: infoAcademicaAdmin
// ====================================================

export interface infoAcademicaAdmin {
  jornadas: JornadaAcademica[] | null;
  periodos: PeriodoAcademico[] | null;
  modalidades: ModalidadAcademica[] | null;
  sedes: IesCampus[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: updateInformacionAcademica
// ====================================================

export interface updateInformacionAcademica_updateInformacionAcademica_programa {
  __typename: "Programa";
  id: string;
  codigo: string;
  nombre: string;
}

export interface updateInformacionAcademica_updateInformacionAcademica_motivo_ingreso {
  __typename: "MotivoIngreso";
  id: string;
  descripcion: string;
}

export interface updateInformacionAcademica_updateInformacionAcademica_financiacion {
  __typename: "Financiacion";
  id: string;
  descripcion: string;
}

export interface updateInformacionAcademica_updateInformacionAcademica {
  __typename: "Academico";
  id: string;
  fecha_registro: any | null;
  sede: IesCampus | null;
  programa: updateInformacionAcademica_updateInformacionAcademica_programa | null;
  modalidad_academica: ModalidadAcademica | null;
  motivo_ingreso: updateInformacionAcademica_updateInformacionAcademica_motivo_ingreso | null;
  es_primera_opcion: boolean | null;
  intentos_admision: number | null;
  fecha_inicio_programa: any | null;
  fecha_finalizacion_materias: any | null;
  fecha_grado: any | null;
  periodo: PeriodoAcademico | null;
  jornada: JornadaAcademica | null;
  trabajo_grado: DegreeWorkType | null;
  titulo_trabajo_grado: string | null;
  linea_investigacion: string | null;
  financiacion: updateInformacionAcademica_updateInformacionAcademica_financiacion | null;
}

export interface updateInformacionAcademica {
  updateInformacionAcademica: updateInformacionAcademica_updateInformacionAcademica | null;
}

export interface updateInformacionAcademicaVariables {
  where: InformacionAcademicaWhereInput;
  data: InformacionAcademicaDataInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: paises
// ====================================================

export interface paises_paises {
  __typename: "Pais";
  id: string;
  nombre: string;
  codigo: string;
  codigo_alfanumerico: string | null;
}

export interface paises {
  paises: paises_paises[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: municipios
// ====================================================

export interface municipios_municipios_departamento_pais {
  __typename: "Pais";
  id: string;
  codigo: string;
  nombre: string;
}

export interface municipios_municipios_departamento {
  __typename: "Departamento";
  id: string;
  codigo: string;
  nombre: string;
  codigo_alfanumerico: string | null;
  pais: municipios_municipios_departamento_pais | null;
}

export interface municipios_municipios_region {
  __typename: "Region";
  id: string;
  codigo: string;
  nombre: string;
}

export interface municipios_municipios {
  __typename: "Municipio";
  id: string;
  codigo: string;
  nombre: string;
  departamento: municipios_municipios_departamento;
  region: municipios_municipios_region | null;
}

export interface municipios {
  municipios: municipios_municipios[];
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: clasesEtnograficas
// ====================================================

export interface clasesEtnograficas_clasesEtnograficas {
  __typename: "ClaseEtnografica";
  id: string;
  nombre: string;
}

export interface clasesEtnograficas {
  clasesEtnograficas: clasesEtnograficas_clasesEtnograficas[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: claseEtnografica
// ====================================================

export interface claseEtnografica_claseEtnografica {
  __typename: "ClaseEtnografica";
  id: string;
  nombre: string;
}

export interface claseEtnografica {
  claseEtnografica: claseEtnografica_claseEtnografica | null;
}

export interface claseEtnograficaVariables {
  where: ClaseEtnograficaWhereUniqueInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: discapacidades
// ====================================================

export interface discapacidades_discapacidades_grupo {
  __typename: "GrupoDiscapacidad";
  id: string;
  nombre: string;
}

export interface discapacidades_discapacidades {
  __typename: "Discapacidad";
  id: string;
  nombre: string;
  grupo: discapacidades_discapacidades_grupo;
}

export interface discapacidades {
  discapacidades: (discapacidades_discapacidades | null)[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: informacionPersonal
// ====================================================

export interface informacionPersonal_informacionPersonal_municipio_recidencia {
  __typename: "Municipio";
  id: string;
  codigo: string;
  nombre: string;
}

export interface informacionPersonal_informacionPersonal_municipio_nacimiento {
  __typename: "Municipio";
  id: string;
  codigo: string;
  nombre: string;
}

export interface informacionPersonal_informacionPersonal_clase_etnografica {
  __typename: "ClaseEtnografica";
  id: string;
  nombre: string;
}

export interface informacionPersonal_informacionPersonal_discapacidad {
  __typename: "Discapacidad";
  id: string;
  nombre: string;
}

export interface informacionPersonal_informacionPersonal {
  __typename: "Personal";
  id: string;
  municipio_recidencia: informacionPersonal_informacionPersonal_municipio_recidencia | null;
  municipio_nacimiento: informacionPersonal_informacionPersonal_municipio_nacimiento | null;
  clase_etnografica: informacionPersonal_informacionPersonal_clase_etnografica | null;
  tiene_discapacidad: boolean | null;
  discapacidad: informacionPersonal_informacionPersonal_discapacidad | null;
  motivo_discapacidad: string | null;
}

export interface informacionPersonal {
  informacionPersonal: informacionPersonal_informacionPersonal | null;
}

export interface informacionPersonalVariables {
  where: UserWhereUniqueInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: egresado
// ====================================================

export interface egresado_egresado_pais_expedicion {
  __typename: "Pais";
  id: string;
  codigo: string;
  codigo_alfanumerico: string | null;
}

export interface egresado_egresado {
  __typename: "User";
  id: string;
  identificacion: string;
  tipo_identificacion: IdentityType | null;
  pais_expedicion: egresado_egresado_pais_expedicion | null;
  nombres: string;
  apellidos: string;
  genero: Gender | null;
  fecha_nacimiento: any | null;
  correo: any;
  celular: string;
  permiso: boolean | null;
}

export interface egresado {
  egresado: egresado_egresado | null;
}

export interface egresadoVariables {
  where: UserWhereUniqueInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: updateInformacionPersonal
// ====================================================

export interface updateInformacionPersonal_updateInformacionPersonal_pais_expedicion {
  __typename: "Pais";
  id: string;
  codigo: string;
  nombre: string;
  codigo_alfanumerico: string | null;
}

export interface updateInformacionPersonal_updateInformacionPersonal {
  __typename: "User";
  id: string;
  identificacion: string;
  tipo_identificacion: IdentityType | null;
  pais_expedicion: updateInformacionPersonal_updateInformacionPersonal_pais_expedicion | null;
  nombres: string;
  apellidos: string;
  genero: Gender | null;
  fecha_nacimiento: any | null;
  correo: any;
  celular: string;
  permiso: boolean | null;
}

export interface updateInformacionPersonal {
  updateInformacionPersonal: updateInformacionPersonal_updateInformacionPersonal | null;
}

export interface updateInformacionPersonalVariables {
  where: UpdateInformacionPersonalWhereInput;
  data: UpdateInformacionPersonalDataInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: changeFirstLoginStatus
// ====================================================

export interface changeFirstLoginStatus_changeFirstLoginStatus {
  __typename: "FirstLoginStatus";
  status: boolean;
}

export interface changeFirstLoginStatus {
  changeFirstLoginStatus: changeFirstLoginStatus_changeFirstLoginStatus;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: setPermisos
// ====================================================

export interface setPermisos_setPermisos {
  __typename: "Permiso";
  allowed: boolean;
}

export interface setPermisos {
  setPermisos: setPermisos_setPermisos;
}

export interface setPermisosVariables {
  data: SetPermisoData;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: categorias
// ====================================================

export interface categorias_categorias {
  __typename: "Categoria";
  id: string;
  nombre: string | null;
  icon: string | null;
}

export interface categorias {
  categorias: categorias_categorias[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: eventos
// ====================================================

export interface eventos_eventos {
  __typename: "Event";
  id: string;
  nombre: string;
  descripcion: string | null;
  poster: string;
  direccion: string | null;
  telefono: string | null;
  email: any | null;
  url: any | null;
  fechas: (any | null)[] | null;
}

export interface eventos {
  eventos: eventos_eventos[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ofertas
// ====================================================

export interface ofertas_ofertas_categoria {
  __typename: "Categoria";
  id: string;
  nombre: string | null;
  icon: string | null;
}

export interface ofertas_ofertas_solicitante {
  __typename: "Solicitante";
  nombre: string;
  telefono: string | null;
  correo: string | null;
}

export interface ofertas_ofertas {
  __typename: "Oferta";
  id: string;
  descripcion: string;
  telefono: string | null;
  correo: any | null;
  fecha_publicacion: any;
  categoria: ofertas_ofertas_categoria | null;
  solicitante: ofertas_ofertas_solicitante | null;
  aplicado: boolean | null;
}

export interface ofertas {
  ofertas: ofertas_ofertas[] | null;
}

export interface ofertasVariables {
  take?: number | null;
  cursor?: CursorPaginationInput | null;
  where?: OfertaWhereInput | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: oferta
// ====================================================

export interface oferta_oferta_categoria {
  __typename: "Categoria";
  id: string;
  nombre: string | null;
}

export interface oferta_oferta_solicitante {
  __typename: "Solicitante";
  nombre: string;
  telefono: string | null;
  correo: string | null;
}

export interface oferta_oferta {
  __typename: "Oferta";
  id: string;
  descripcion: string;
  telefono: string | null;
  correo: any | null;
  fecha_publicacion: any;
  categoria: oferta_oferta_categoria | null;
  solicitante: oferta_oferta_solicitante | null;
  aplicado: boolean | null;
}

export interface oferta {
  oferta: oferta_oferta | null;
}

export interface ofertaVariables {
  where: OfertaWhereUniqueInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: interesados
// ====================================================

export interface interesados_interesados_interesados_user_pais_expedicion {
  __typename: "Pais";
  id: string;
  codigo: string;
  codigo_alfanumerico: string | null;
}

export interface interesados_interesados_interesados_user {
  __typename: "User";
  id: string;
  identificacion: string;
  tipo_identificacion: IdentityType | null;
  pais_expedicion: interesados_interesados_interesados_user_pais_expedicion | null;
  nombres: string;
  apellidos: string;
  genero: Gender | null;
  fecha_nacimiento: any | null;
  correo: any;
  celular: string;
  permiso: boolean | null;
}

export interface interesados_interesados_interesados {
  __typename: "Interesado";
  fecha_aplicacion: any | null;
  user: interesados_interesados_interesados_user;
}

export interface interesados_interesados {
  __typename: "InterestedList";
  totalCount: number | null;
  interesados: interesados_interesados_interesados[] | null;
}

export interface interesados {
  interesados: interesados_interesados;
}

export interface interesadosVariables {
  skip?: number | null;
  take?: number | null;
  where: InteresadosWhereInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: egresados
// ====================================================

export interface egresados_egresados_pais_expedicion {
  __typename: "Pais";
  id: string;
  codigo: string;
  codigo_alfanumerico: string | null;
}

export interface egresados_egresados {
  __typename: "User";
  id: string;
  identificacion: string;
  tipo_identificacion: IdentityType | null;
  pais_expedicion: egresados_egresados_pais_expedicion | null;
  nombres: string;
  apellidos: string;
  genero: Gender | null;
  fecha_nacimiento: any | null;
  correo: any;
  celular: string;
  permiso: boolean | null;
}

export interface egresados {
  egresados: egresados_egresados[] | null;
}

export interface egresadosVariables {
  take?: number | null;
  cursor?: CursorPaginationInput | null;
  where?: EgresadoWhereInput | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: createOferta
// ====================================================

export interface createOferta_createOferta_categoria {
  __typename: "Categoria";
  id: string;
  nombre: string | null;
  icon: string | null;
}

export interface createOferta_createOferta_solicitante {
  __typename: "Solicitante";
  nombre: string;
  telefono: string | null;
  correo: string | null;
}

export interface createOferta_createOferta {
  __typename: "Oferta";
  id: string;
  descripcion: string;
  telefono: string | null;
  correo: any | null;
  fecha_publicacion: any;
  categoria: createOferta_createOferta_categoria | null;
  solicitante: createOferta_createOferta_solicitante | null;
}

export interface createOferta {
  createOferta: createOferta_createOferta | null;
}

export interface createOfertaVariables {
  data: CreateOfertaDataInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: updateOferta
// ====================================================

export interface updateOferta_updateOferta_categoria {
  __typename: "Categoria";
  id: string;
  nombre: string | null;
  icon: string | null;
}

export interface updateOferta_updateOferta_solicitante {
  __typename: "Solicitante";
  nombre: string;
  telefono: string | null;
  correo: string | null;
}

export interface updateOferta_updateOferta {
  __typename: "Oferta";
  id: string;
  descripcion: string;
  telefono: string | null;
  correo: any | null;
  fecha_publicacion: any;
  categoria: updateOferta_updateOferta_categoria | null;
  solicitante: updateOferta_updateOferta_solicitante | null;
}

export interface updateOferta {
  updateOferta: updateOferta_updateOferta | null;
}

export interface updateOfertaVariables {
  where: OfertaWhereUniqueInput;
  data: UpdateOfertaDataInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: deleteOferta
// ====================================================

export interface deleteOferta_deleteOferta {
  __typename: "ObjectId";
  id: string | null;
}

export interface deleteOferta {
  deleteOferta: deleteOferta_deleteOferta | null;
}

export interface deleteOfertaVariables {
  where: OfertaWhereUniqueInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: createInteresado
// ====================================================

export interface createInteresado_createInteresado {
  __typename: "ObjectId";
  id: string | null;
}

export interface createInteresado {
  createInteresado: createInteresado_createInteresado | null;
}

export interface createInteresadoVariables {
  data: CreateInteresadoDataInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: deleteInteresado
// ====================================================

export interface deleteInteresado_deleteInteresado {
  __typename: "ObjectId";
  id: string | null;
}

export interface deleteInteresado {
  deleteInteresado: deleteInteresado_deleteInteresado | null;
}

export interface deleteInteresadoVariables {
  where: DeleteInteresadoWhereInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: createEvento
// ====================================================

export interface createEvento_createEvento {
  __typename: "Event";
  id: string;
  nombre: string;
  poster: string;
  descripcion: string | null;
  direccion: string | null;
  telefono: string | null;
  email: any | null;
  url: any | null;
}

export interface createEvento {
  createEvento: createEvento_createEvento | null;
}

export interface createEventoVariables {
  data: CreateEventoDataInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: updateEvento
// ====================================================

export interface updateEvento_updateEvento {
  __typename: "Event";
  id: string;
  nombre: string;
  poster: string;
  descripcion: string | null;
  direccion: string | null;
  telefono: string | null;
  email: any | null;
  url: any | null;
}

export interface updateEvento {
  updateEvento: updateEvento_updateEvento | null;
}

export interface updateEventoVariables {
  where: EventoWhereInput;
  data: UpdateEventoWhereInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: deleteEvento
// ====================================================

export interface deleteEvento_deleteEvento {
  __typename: "ObjectId";
  id: string | null;
}

export interface deleteEvento {
  deleteEvento: deleteEvento_deleteEvento | null;
}

export interface deleteEventoVariables {
  where: EventoWhereInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: empresas
// ====================================================

export interface empresas_empresas {
  __typename: "Empresa";
  id: string;
  nombre: string;
  direccion: string | null;
  telefono: string | null;
  correo: any | null;
}

export interface empresas {
  empresas: empresas_empresas[] | null;
}

export interface empresasVariables {
  take?: number | null;
  cursor?: CursorPaginationInput | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: empresa
// ====================================================

export interface empresa_empresa {
  __typename: "Empresa";
  id: string;
  nombre: string;
  direccion: string | null;
  telefono: string | null;
  correo: any | null;
}

export interface empresa {
  empresa: empresa_empresa | null;
}

export interface empresaVariables {
  where: EmpresaWhereUniqueInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: actividadesActuales
// ====================================================

export interface actividadesActuales_actividadesActuales {
  __typename: "ActividadNoLaboral";
  id: string;
  descripcion: string;
}

export interface actividadesActuales {
  actividadesActuales: actividadesActuales_actividadesActuales[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: canalesComunicacion
// ====================================================

export interface canalesComunicacion_canalesComunicacion {
  __typename: "CanalComunicacion";
  id: string;
  descripcion: string;
}

export interface canalesComunicacion {
  canalesComunicacion: (canalesComunicacion_canalesComunicacion | null)[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: razonesDificultadTrabajo
// ====================================================

export interface razonesDificultadTrabajo_razonesDificultadTrabajo {
  __typename: "RazonDificultad";
  id: string;
  descripcion: string;
}

export interface razonesDificultadTrabajo {
  razonesDificultadTrabajo: (razonesDificultadTrabajo_razonesDificultadTrabajo | null)[] | null;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: informacionLaboral
// ====================================================

export interface informacionLaboral_informacionLaboral_municipio {
  __typename: "Municipio";
  id: string;
  codigo: string;
  nombre: string;
}

export interface informacionLaboral_informacionLaboral_empresa {
  __typename: "Empresa";
  id: string;
  nombre: string;
}

export interface informacionLaboral_informacionLaboral {
  __typename: "Laboral";
  id: string;
  fecha_inicio: any;
  fecha_registro: any;
  salario: Salario | null;
  ocupacion: string | null;
  tipo_contrato: TipoContrato | null;
  municipio: informacionLaboral_informacionLaboral_municipio;
  empresa: informacionLaboral_informacionLaboral_empresa | null;
  horas_trabajo: number | null;
  grado_satisfaccion: GradoSatisfaccion | null;
  role: string | null;
}

export interface informacionLaboral {
  informacionLaboral: informacionLaboral_informacionLaboral | null;
}

export interface informacionLaboralVariables {
  where: InformacionLaboralWhereInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: informacionNoLaboral
// ====================================================

export interface informacionNoLaboral_informacionNoLaboral_razon_dificultad {
  __typename: "RazonDificultad";
  id: string;
  descripcion: string;
}

export interface informacionNoLaboral_informacionNoLaboral_canal_busqueda {
  __typename: "CanalComunicacion";
  id: string;
  descripcion: string;
}

export interface informacionNoLaboral_informacionNoLaboral_actividad_actual {
  __typename: "ActividadNoLaboral";
  id: string;
  descripcion: string;
}

export interface informacionNoLaboral_informacionNoLaboral {
  __typename: "NoLaboral";
  id: string;
  fecha_registro: any;
  razon_dificultad: informacionNoLaboral_informacionNoLaboral_razon_dificultad | null;
  canal_busqueda: informacionNoLaboral_informacionNoLaboral_canal_busqueda | null;
  tiempo_busqueda: number | null;
  actividad_actual: informacionNoLaboral_informacionNoLaboral_actividad_actual | null;
}

export interface informacionNoLaboral {
  informacionNoLaboral: informacionNoLaboral_informacionNoLaboral | null;
}

export interface informacionNoLaboralVariables {
  where: InformacionNoLaboralWhereInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: updateInformacionLaboral
// ====================================================

export interface updateInformacionLaboral_updateInformacionLaboral_pais_expedicion {
  __typename: "Pais";
  id: string;
  codigo: string;
  codigo_alfanumerico: string | null;
}

export interface updateInformacionLaboral_updateInformacionLaboral {
  __typename: "User";
  id: string;
  identificacion: string;
  tipo_identificacion: IdentityType | null;
  pais_expedicion: updateInformacionLaboral_updateInformacionLaboral_pais_expedicion | null;
  nombres: string;
  apellidos: string;
  genero: Gender | null;
  fecha_nacimiento: any | null;
  correo: any;
  celular: string;
  permiso: boolean | null;
}

export interface updateInformacionLaboral {
  updateInformacionLaboral: updateInformacionLaboral_updateInformacionLaboral | null;
}

export interface updateInformacionLaboralVariables {
  where: InformacionLaboralWhereInput;
  data: UpdateInformacionLaboralDataInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: updateInformacionNoLaboral
// ====================================================

export interface updateInformacionNoLaboral_updateInformacionNoLaboral_pais_expedicion {
  __typename: "Pais";
  id: string;
  codigo: string;
  codigo_alfanumerico: string | null;
}

export interface updateInformacionNoLaboral_updateInformacionNoLaboral {
  __typename: "User";
  id: string;
  identificacion: string;
  tipo_identificacion: IdentityType | null;
  pais_expedicion: updateInformacionNoLaboral_updateInformacionNoLaboral_pais_expedicion | null;
  nombres: string;
  apellidos: string;
  genero: Gender | null;
  fecha_nacimiento: any | null;
  correo: any;
  celular: string;
  permiso: boolean | null;
}

export interface updateInformacionNoLaboral {
  updateInformacionNoLaboral: updateInformacionNoLaboral_updateInformacionNoLaboral | null;
}

export interface updateInformacionNoLaboralVariables {
  where: InformacionNoLaboralWhereInput;
  data: UpdateInformacionNoLaboralDataInput;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum DegreeWorkType {
  APLICATIVO = "APLICATIVO",
  PASANTIA = "PASANTIA",
  TESIS = "TESIS",
}

export enum Gender {
  FEMENINO = "FEMENINO",
  MASCULINO = "MASCULINO",
  OTRO = "OTRO",
}

export enum GradoSatisfaccion {
  INSATISFECHO = "INSATISFECHO",
  MUY_INSATISFECHO = "MUY_INSATISFECHO",
  MUY_SATISFECHO = "MUY_SATISFECHO",
  REGULAR = "REGULAR",
  SATISFECHO = "SATISFECHO",
}

export enum IdentityType {
  CC = "CC",
  CE = "CE",
  TI = "TI",
}

export enum IesCampus {
  EXTENSION = "EXTENSION",
  PRINCIPAL = "PRINCIPAL",
}

export enum JornadaAcademica {
  DIURNO = "DIURNO",
  NOCTURNO = "NOCTURNO",
}

export enum ModalidadAcademica {
  DIPLOMADO = "DIPLOMADO",
  POSGRADO = "POSGRADO",
  PREGRADO = "PREGRADO",
}

export enum PeriodoAcademico {
  ANUAL = "ANUAL",
  SEMESTRAL = "SEMESTRAL",
}

export enum Salario {
  MAS_CUATRO_SMLV = "MAS_CUATRO_SMLV",
  MENOS_UNO_SMLV = "MENOS_UNO_SMLV",
  TRES_CUATRO_SMLV = "TRES_CUATRO_SMLV",
  UNO_DOS_SMLV = "UNO_DOS_SMLV",
}

export enum SchoolCalendarType {
  A = "A",
  B = "B",
}

export enum SchoolType {
  NO_OFICIAL = "NO_OFICIAL",
  OFICIAL = "OFICIAL",
}

export enum TipoContrato {
  APRENDIZAJE = "APRENDIZAJE",
  OCACIONAL = "OCACIONAL",
  PRESTACION_SERVICIOS = "PRESTACION_SERVICIOS",
  TEMRINO_INDEFINIDO = "TEMRINO_INDEFINIDO",
  TERMINO_FIJO = "TERMINO_FIJO",
}

export interface ActividadActualWhereUniqueInput {
  id: string;
}

export interface CanalBusquedaWhereUniqueInput {
  id: string;
}

export interface CategoriaWhereUniqueInput {
  id: string;
}

export interface ClaseEtnograficaWhereUniqueInput {
  id: string;
}

export interface ColegioWhereUniqueInput {
  id: string;
}

export interface CreateEventoDataInput {
  nombre: string;
  descripcion?: string | null;
  poster: string;
  direccion?: string | null;
  telefono?: string | null;
  email?: string | null;
  url?: any | null;
  fechas?: any[] | null;
}

export interface CreateInteresadoDataInput {
  oferta: OfertaWhereUniqueInput;
}

export interface CreateOfertaDataInput {
  solicitante: any;
  categoria: CategoriaWhereUniqueInput;
  descripcion: string;
  telefono?: string | null;
  correo?: string | null;
}

export interface CursorPaginationInput {
  id: string;
}

export interface DeleteInteresadoWhereInput {
  oferta: OfertaWhereUniqueInput;
}

export interface DiscapacidadWhereUniqueInput {
  id: string;
}

export interface EgresadoWhereInput {
  search?: string | null;
}

export interface EmpresaWhereUniqueInput {
  id: string;
}

export interface EventoWhereInput {
  id: string;
}

export interface FinanciacionWhereUniqueInput {
  id: string;
}

export interface InformacionAcademicaDataInput {
  sede?: IesCampus | null;
  modalidad_academica?: ModalidadAcademica | null;
  motivo_ingreso?: MotivoIngresoWhereUniqueInput | null;
  es_primera_opcion?: boolean | null;
  intentos_admision?: number | null;
  fecha_inicio_programa?: any | null;
  fecha_finalizacion_materias?: any | null;
  fecha_grado?: any | null;
  periodo?: PeriodoAcademico | null;
  jornada?: JornadaAcademica | null;
  promedio_notas?: number | null;
  mension?: boolean | null;
  trabajo_grado?: DegreeWorkType | null;
  titulo_trabajo_grado?: string | null;
  linea_investigacion?: string | null;
  financiacion?: FinanciacionWhereUniqueInput | null;
  colegio?: ColegioWhereUniqueInput | null;
}

export interface InformacionAcademicaWhereInput {
  user: UserWhereUniqueInput;
}

export interface InformacionLaboralWhereInput {
  user: UserWhereUniqueInput;
}

export interface InformacionNoLaboralWhereInput {
  user: UserWhereUniqueInput;
}

export interface InteresadosWhereInput {
  oferta: OfertaWhereUniqueInput;
}

export interface MotivoIngresoWhereUniqueInput {
  id: string;
}

export interface MunicipioWhereUniqueInput {
  id: string;
}

export interface OfertaWhereInput {
  search?: string | null;
  applied?: boolean | null;
}

export interface OfertaWhereUniqueInput {
  id: string;
}

export interface RazonDificultadWhereUniqueInput {
  id: string;
}

export interface SetPermisoData {
  allow: boolean;
}

export interface UpdateEventoWhereInput {
  nombre?: string | null;
  descripcion?: string | null;
  poster?: string | null;
  direccion?: string | null;
  telefono?: string | null;
  email?: string | null;
  url?: any | null;
  fechas?: any[] | null;
}

export interface UpdateInformacionLaboralDataInput {
  empresa?: EmpresaWhereUniqueInput | null;
  ocupacion?: string | null;
  role?: string | null;
  tipo_contrato?: TipoContrato | null;
  salario?: Salario | null;
  fecha_inicio?: any | null;
  fecha_finalizacion?: any | null;
  municipio?: MunicipioWhereUniqueInput | null;
  horas_trabajo?: number | null;
  grado_satisfaccion?: GradoSatisfaccion | null;
}

export interface UpdateInformacionNoLaboralDataInput {
  actividad_actual?: ActividadActualWhereUniqueInput | null;
  canales_busqueda?: CanalBusquedaWhereUniqueInput[] | null;
  tiempo_busqueda?: number | null;
  razon_dificultad?: RazonDificultadWhereUniqueInput | null;
}

export interface UpdateInformacionPersonalDataInput {
  genero?: Gender | null;
  fecha_nacimiento?: any | null;
  celular?: string | null;
  correo?: string | null;
  municipio_recidencia?: MunicipioWhereUniqueInput | null;
  municipio_nacimiento?: MunicipioWhereUniqueInput | null;
  clase_etnografica?: ClaseEtnograficaWhereUniqueInput | null;
  tiene_discapacidad?: boolean | null;
  discapacidad?: DiscapacidadWhereUniqueInput | null;
  motivo_discapacidad?: string | null;
}

export interface UpdateInformacionPersonalWhereInput {
  user: UserWhereUniqueInput;
}

export interface UpdateOfertaDataInput {
  categoria?: CategoriaWhereUniqueInput | null;
  solicitante?: any | null;
  descripcion?: string | null;
  telefono?: string | null;
  correo?: string | null;
}

export interface UserWhereUniqueInput {
  id: string;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
