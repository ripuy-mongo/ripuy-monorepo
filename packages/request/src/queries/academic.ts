import gql from "graphql-tag";

const colegios = gql`
    query colegios{ colegios{ id nombre codigo } }
`;

const motivosIngreso = gql`
    query motivosIngreso{ motivosIngreso{ id descripcion } }
`;

const formasFinanciacion = gql`
    query formasFinanciacion{ formasFinanciacion{ id descripcion } }
`;

const academicFormData = gql`
    query academicFormData{
        sedes
        modalidades
        periodos
        jornadas
        formasFinanciacion{id descripcion}
        motivosIngreso{ id descripcion }
        colegios{
            id codigo nombre calendario naturaleza municipio{  nombre id}
        }
    }
`;

const informacionAcademica = gql`
    query informacionAcademica( $where: InformacionAcademicaWhereInput! ){
        informacionAcademica(where: $where){
            id fecha_registro sede programa{id codigo nombre}
            colegio{ id nombre }
            modalidad_academica motivo_ingreso{id descripcion}
            es_primera_opcion intentos_admision fecha_inicio_programa
            fecha_finalizacion_materias fecha_grado periodo jornada trabajo_grado titulo_trabajo_grado linea_investigacion financiacion{id descripcion}
        }
    }
`;


const infoAcademicaAdmin = gql`
    query infoAcademicaAdmin { jornadas periodos modalidades sedes }
`;

const updateInformacionAcademica = gql`
    mutation updateInformacionAcademica( $where: InformacionAcademicaWhereInput! $data: InformacionAcademicaDataInput! ){
        updateInformacionAcademica(where: $where data: $data){
            id fecha_registro sede programa{id codigo nombre}
            modalidad_academica motivo_ingreso{id descripcion}
            es_primera_opcion intentos_admision fecha_inicio_programa
            fecha_finalizacion_materias fecha_grado periodo jornada trabajo_grado titulo_trabajo_grado linea_investigacion financiacion{id descripcion}
        }
    }
`;

export {
    colegios,
    motivosIngreso,
    academicFormData,
    informacionAcademica,
    updateInformacionAcademica,
    formasFinanciacion,
    infoAcademicaAdmin
};