import gql from "graphql-tag";

const paises = gql`
    query paises{ paises{ id nombre codigo codigo_alfanumerico } }
`;

const municipios = gql`
    query municipios{
        municipios{
            id codigo nombre departamento{id codigo nombre codigo_alfanumerico pais{id codigo nombre}}
            region{id codigo nombre}
        }
    }
`;

export {
    paises,
    municipios
};
