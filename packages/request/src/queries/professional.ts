import gql from "graphql-tag";


// Queries

const empresas = gql`
    query empresas($take: Int $cursor: CursorPaginationInput){ 
        empresas(take:$take cursor:$cursor){ id nombre direccion telefono correo } 
    }
`;

const empresa = gql`
    query empresa($where:EmpresaWhereUniqueInput!){
        empresa(where:$where){id nombre direccion telefono correo}
    }
`;

const actividadesActuales = gql`
    query actividadesActuales{ actividadesActuales{ id descripcion } }
`;

const canalescomunicacion = gql`
    query canalesComunicacion{ canalesComunicacion{id descripcion} }
`;

const razonesDificultadTrabajo = gql`
    query razonesDificultadTrabajo{ razonesDificultadTrabajo{ id descripcion } }
`;

const informacionLaboral = gql`
    query informacionLaboral( $where: InformacionLaboralWhereInput! ){
      informacionLaboral( where: $where ){
        id fecha_inicio fecha_registro salario ocupacion
        tipo_contrato municipio{id codigo nombre}
        empresa{id nombre}
        horas_trabajo grado_satisfaccion role
      }
    }
`;

const informacionNoLaboral = gql`
    query informacionNoLaboral( $where: InformacionNoLaboralWhereInput! ){
      informacionNoLaboral( where: $where ){
        id fecha_registro razon_dificultad{id descripcion}
        canal_busqueda{id descripcion}
        tiempo_busqueda actividad_actual{id descripcion}
      }
    }
`;

// Mutations

const updateInformacionLaboral = gql`
    mutation updateInformacionLaboral($where:InformacionLaboralWhereInput! $data:UpdateInformacionLaboralDataInput!){
        updateInformacionLaboral(where:$where data:$data){
            id identificacion tipo_identificacion
            pais_expedicion{id codigo codigo_alfanumerico}
            nombres apellidos genero fecha_nacimiento
            correo celular permiso
        }
    }
`;

const updateInformacionNoLaboral = gql`
    mutation updateInformacionNoLaboral($where:InformacionNoLaboralWhereInput! $data:UpdateInformacionNoLaboralDataInput!){
        updateInformacionNoLaboral(where:$where data:$data){
            id identificacion tipo_identificacion
            pais_expedicion{id codigo codigo_alfanumerico}
            nombres apellidos genero fecha_nacimiento
            correo celular permiso
        }
    }
`;

export {
    empresas,
    empresa,
    actividadesActuales,
    canalescomunicacion,
    razonesDificultadTrabajo,
    updateInformacionLaboral,
    updateInformacionNoLaboral,
    informacionLaboral,
    informacionNoLaboral
};
