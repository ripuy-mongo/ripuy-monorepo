const path = require( 'path' );
const TerserPlugin = require( 'terser-webpack-plugin' );

module.exports = {
    mode: process.env.NODE_ENV,
    entry: './src/index.ts',
    output: {
        filename: 'index.js',
        path: path.resolve( __dirname, 'dist' ),
        libraryTarget: 'commonjs'
    },
    resolve: {
        extensions: ['.js', '.ts']
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.[jt]s$/,
                exclude: /node_modules|dist/,
                loader: 'eslint-loader'
            },
            {
                test: /\.[tj]s$/,
                exclude: /node_modules|dist/,
                loader: 'ts-loader'
            }
        ]
    },
    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin( {
            terserOptions: {
                output: {
                    comments: false
                }
            },
            extractComments: false
        } )]
    }
}
