# Paquete Request

👉 [Link documentación disponible en Notion](https://charlitoro.notion.site/Paquete-Request-c0feb7d3a3f74ed093db9c29ce20bf7f)

Este documento contiene la configuración de un paquete que funciona com interprete del api backend y que lo utiliza el cliente web para generar los tipos de typescript que le permitirán codificar fácilmente y con una menor margen de error las peticiones hacia el api. Esta información le permitirá a los desarrolladores web poder mantener e implementar nuevas funcionalidades el proyecto web utilizando este paquete que realiza las peticiones hacia el api.

Este paquete se encuentra ubicado en el directorio ***packages/request*** y contiene la siguiente estructura de directorios:

```bash
packages/request
├── dist
└── src
    ├── client
    ├── queries
    └── services
```

## Queries

En este apartado del paquete se definen y construyen las queries y mutaciones que se pueden enviar al api backend. Dentro del directorio **packages/request/src/queries/** se configuran las queries y mutaciones. Esta estructurada de la siguiente manera

```bash
packages/request/src/queries
├── academic.ts
├── commons.ts
├── personal.ts
├── post.ts
├── professional.ts
└── types.ts
```

Cada query debe ir como string y utilizar la función **gql** del paquete **graphql-tag** para parcear el string a objeto graphql. un ejemplo de consulta de ofertas laborales:

```tsx
import gql from "graphql-tag";

const ofertas = gql`
    query ofertas( $take: Int $cursor: CursorPaginationInput $where: OfertaWhereInput){
        ofertas( take:$take cursor:$cursor where:$where){
            id descripcion telefono correo fecha_publicacion
            categoria{id nombre icon}
            solicitante{ nombre telefono correo }
            aplicado
        }
    }
`;
```

## Servicios

En el directorio **packages/request/src/services** se configuran los servicios de las queries agrupadas. Cada servicio extiende de la clase ***Service*** que cuenta con las tipos y métodos que permiten la petición de queries y mutaciones. Por cada query se crea una función adicional en la clase del servicio. Los archivos de los servicios están estructurados de la siguiente manera

```bash
packages/request/src/services
├── Academic.ts
├── Commons.ts
├── Personal.ts
├── Post.ts
├── Professional.ts
└── Service.ts
```

Por ejemplo el servicio Post con la query de ofertas laborales y la mutación de creación de oferta:

```tsx
import { Service } from './Service';
import * as queries from '../queries/post';
import * as Types from '../queries/types';
import { ClientOptions, MutationClientOptions } from '../client/interfaces';

class Post extends Service {

    // Query
    ofertas( variables: Types.ofertasVariables, clientOptions?: ClientOptions ): Promise<Types.ofertas>{
        return this.doQuery<Types.ofertas, Types.ofertasVariables>(
            queries.ofertas, variables, clientOptions
        );
    }

    // Mutacion
    createOferta( variables: Types.createOfertaVariables, clientOptions?: MutationClientOptions ): Promise<Types.createOferta>{
        return this.doMutation<Types.createOferta, Types.createOfertaVariables>(
            queries.createOferta, variables, clientOptions
        );
    }
}

export { Post };
```

## Cliente Apollo

El cliente de apollo es el que se encarga se hacer las peticiones al api, donde se configuran los headers y los parámetros necesarios para que las peticiones se realicen correctamente. Los archivos están estructurados de la siguiente manera

```bash
packages/request/src/client
├── APIClient.ts
├── ApolloClientManager.ts
└── interfaces.ts
```

El archivo **ApolloClientManager.ts** es el encargado de hacer las peticiones al api y el archivo **APIClient.ts** es donde se importan los servicios creados que se describieron en el anterior apartado de este documento. Este APIClient exportado es el que se configura en el cliente web para hacer uso de los servicios y por tanto de las queries definidas.

## Generar Tipos

Una vez ya se tengan todas las queries se procede a generar las interfaces de typescript. Para generar los tipos se utiliza el paquete de apollo client, y requiere de un archivo configuración **apollo.config.js** con la siguiente configuración:

```jsx
module.exports = {
    client: {
        service: {
            name: 'Ripuy',
            url: 'http://localhost:8180/graphql',
        },
        includes: [
            'src/queries/*.ts'
        ],
        excludes: [
            'node_modules/*'
        ],
        tagName: 'gql',
    }
};
```

En este archivo es importante definir la url del api backend , en este caso suponiendo que el server esta corriendo localmente, se utiliza **[localhost](http://localhost)** y el puerto **8180.** En includes se establece la ruta donde están los archivos de las queries definidas a las cuales se les va generar los tipos.

Ejecutando el siguiente comando se generan los tipos y en el archivo package.json es posible ver que tanto se ejecuta en el script, y en el se encuentra el directorio y nombre del archivo donde se guardan los tipos

```bash
yarn apollo:generate
# apollo codegen:generate --outputFlat src/queries/types.ts --target typescript
```

Dentro del directorio de las queries en el archivo **types.ts** se generarían los tipos 

## Compilación

Para que el cliente web pueda hacer uso de este paquete e importar el APICliente se deben ejecutar los siguientes comandos

```bash
# ubicado en el directorio packages/request
yarn install && yarn build
```

Con esto en el cliente web ya es posible agregar a las dependencias del **package.json** tal como se puede ver en el siguiente código

```json
// package.json paquete web
"dependencies": {
    ...
    "ripuy-request": "^1.0.0",
    ...
  },
```

y para poder usarlo o importarlo en código solo vasta con hacer lo siguiente 

```tsx
import { APIClient, ApolloClientManager } from 'ripuy-request';
```