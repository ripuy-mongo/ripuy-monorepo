# Paquete API

👉 [Link documentación disponible en Notion](https://www.notion.so/Paquete-API-6bad08c9f2c34089b4dc26c428fbc530)

Este documento contiene la información de la estructura y construcción del Backend del aplicativo Ripuy donde se utilizo la arquitectura API (**Application Programming Interface**) [GraphQL](https://graphql.org/). Esta información permitirá que nuevos estudiantes o desarrolladores que deseen continuar con el proyecto puedan mantenerlo e implementar nuevas funcionalidades.

Este paquete se encuentra ubicado en el directorio ***packages/api*** y contiene la siguiente estructura de directorios:

```bash
packages/api
├── dist
└── src
    ├── models
    │   ├── academic
    │   ├── authentication
    │   ├── personal
    │   ├── posts
    │   └── professional
    ├── plugins
    │   ├── authentication
    │   ├── loaders
    │   ├── sendEmail
    │   └── session
    ├── resolvers
    │   ├── academic
    │   ├── commons
    │   ├── personal
    │   ├── post
    │   ├── professional
    │   ├── test
    │   │   ├── fixtures
    │   │   └── server
    │   │       └── backup
    │   │           └── ripuy-test
    │   └── utils
    ├── schema
    └── utils
```

## Stack

Para la construcción del API se uso el siguiente stask de tecnologías:

- [JavaScript](https://www.javascript.com/): Es el lenguaje de programación interpretado tanto del lado del cliente como del servidor más usado en el desarrollo web.
- [Espress - NodeJS](https://expressjs.com/): Espress es un framework que corre bajo el motor de NodeJS la cual proporciona métodos de utilidad HTTP y middleware a su disposición para crear una API robusta rápido y fácil.
- [Webpack](https://webpack.js.org/): Es un empaquetador de módulos para JavaScript que permite optimizar los recursos y empaquetar todo el contenido de una aplicación en archivos más simples.
- [Typescript](https://www.typescriptlang.org/): Es un lenguaje que agrega sintaxis adicional a JavaScript para admitir una integración más estrecha con su editor. Detecte errores temprano en su editor.
- [GraphQL](https://graphql.org/): Es un lenguaje de consulta para API para cumplir con esas consultas con los datos existentes. Brinda a los clientes el poder de solicitar exactamente lo que necesitan y nada más, facilita la evolución de las API con el tiempo y habilita herramientas poderosas para desarrolladores.
- [JestJS](https://jestjs.io/): Es un framework de JavaScript para hacer pruebas centrado en la simplicidad, utilizado para la construcción de las pruebas funcionales del API.

## Instalación y configuración del entorno de desarrollo

### Instalación de NodeJS

Lo primero a instalación en nuestro sistema operativo es el motor para JavaScript NodeJS. La version de Node utilizada en el momento del desarrollo es la [v14.17.0](https://nodejs.org/en/download/).

La guía de instalación depende del sistema operativo que se este utilizando, [aquí](https://nodejs.org/en/download/package-manager/) pueden encontrar como instalar Node desde el sistema de paquetes que tengan en su sistema.

Cuando se termine de instalar es posible comprobar ejecutando el siguiente comando en su terminal

```bash
node --version
# v14.17.0
```

Junto a NodeJS se instala **NPM** que es el sistema que gestiona los paquetes para JavaScript, con este es posible instalar todas las dependencias que se encuentran en el archivo **package.json** del directorio api. Para comprobar que este instalado y funcionando se ejecuta el comando

```bash
npm --version
# 6.14.13
```

adicional a esto se instala [Yarn](https://yarnpkg.com/), que es una alternativa a NPM pero a diferencia esta permitirá gestionar workspaces. La guía de instalación la pueden encontrar [aquí](https://yarnpkg.com/getting-started/install) e igualmente como las anteriores instalaciones es posible comprobar su instalacion con el comando

```bash
yarn --version
# 1.22.17
```

> En ocasiones suele haber problemas con permisos de usuario, sobre todo en sistemas Linux, esto se pude presentar en el siguiente paso que es la instalación de dependencias. Una solución en el caso de que ocurra algún error, también es posible instalar node y npm mediante **NVM**, [aquí](https://github.com/nvm-sh/nvm) les dejo como poder instalar nvm.
> 

### Instalación de Docker

Docker es una tecnología de contenerización que sera utilizado para lanzar el api en un entorno fuera de nuestro sistema local.

Su instalación depende del sistema operativo utilizado, seguir [esta](https://docs.docker.com/engine/install/) guía de instalación oficial de docker.

Una vez instalado para verificar que este instalado se ejecuta el comando en la terminal

```bash
sudo docker --version
# Docker version 20.10.12, build e91ed5707e

sudo docker ps
# CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

En los sistemas linux para poder envitar el usar sudo para poder ejecutar los comandos de docker es necesario agregar el grupo docker a los grupos del usuario del sistema en sesión, para esto se ejecuta

```bash
sudo usermod -aG docker $USER
```

Seguido a esto hay que cerrar la sesión o reiniciar el sistema y en terminal volver a ejecutar los comandos que de verificación pero sin permisos sudo

### Instalación de MongoDB

El servicio de MongoDB es necesario para poder hacer las prueba funcionales del api y si todo el entorno se va a trabajar localmente, entonces de seguro sera necesario para poder conectar el servicio a el API mediante las variables de entorno.

Este proceso de instalación depende del sistema operativo que se este usando, en el siguiente [link](https://dev-group.gitbook.io/mongodb-docs/) hemos descrito y documentado más a fondo como instalar y configurar MongoDB

para poder verificar que todo este funcionando bien se ejecuta

```bash
mongosh
# Current Mongosh Log ID: 61e1e432c7c156a0be15d8ff
# Connecting to:          mongodb://127.0.0.1:27017/?directConnection=true&serverSelectionTimeoutMS=2000
# Using MongoDB:          5.0.5
# Using Mongosh:          1.1.0
#
# For mongosh info see: https://docs.mongodb.com/mongodb-shell/
#
# ------
#    The server generated these startup warnings when booting:
#    2022-01-14T08:31:56.635-05:00: Using the XFS filesystem is strongly recommended with the WiredTiger storage engine. See http://dochub.mongodb.org/core/prodnotes-filesystem
#    2022-01-14T08:31:57.561-05:00: Access control is not enabled for the database. Read and write access to data and configuration is unrestricted
# ------
#
# Warning: Found ~/.mongorc.js, but not ~/.mongoshrc.js. ~/.mongorc.js will not be loaded.
#  You may want to copy or rename ~/.mongorc.js to ~/.mongoshrc.js.
# test>
```

Estando en el shell de mongo es necesario configurar un usuario con los permisos solo de acceso a la base de datos a la cual se conectara el api. para esto ejecutamos el siguiente comando:

```jsx
db.createUser({
    user: 'ripuy',
    pwd: 'R1pu7001',
    roles: [
        {
            role: 'dbOwner',
            db: 'ripuy-udenar',
        },
    ],
});
```

Lo anterior no indica que vamos a tener un usuario llamado **ripuy** con una contraseña y permisos de **owner** solo sobre la base de datos **ripuy-udenar**. Estas credenciales son esenciales para la conexión con la base de datos, estas deben ir en los archivos **.env** de variables de entorno, dependiendo de en que entorno se las requiera.

El puerto por defecto es 27017 pero si en la configuración de la instalación cambio el puerto es necesario especificar el puerto configurado.

### Instalación de dependencias

Ubicado en el directorio **packages/api/** ejecutar el comando

```bash
yarn install
```

Una vez todo este instalado ya es posible ejecutar el servicio creado para el api ejecutando el comando

```bash
# Para ejecutar el server en entorno de development, 
# mientras de esta desarrollando se iran refrescando los cambios
yarn dev

# Para ejecutar el server en entorno de staging
yarn start

# Para ejecutar el server en entorno de production
yarn prod
```

Esto deberá habilitar el servicio mediante [http://localhost:8180](http://localhost:8180/) y para probar que todo este bien, en el navegador accediendo a esa dirección, deberá retornar un “Hola Mundo!!”

### Variables de entorno

Para el correcto funcionamiento del API es importante que las variables de entorno este correctamente configuradas, estas contienen información de conexión a base de datos y otros importantes valores. Estas variables se encuentran dentro de la raiz del package api en los archivos **.env.dev** para el entorno **staging**, **.env.prod** para el entorno **production** y .env.test para la ejecución de **pruebas.**

## Base de datos

En este apartado se tratara la conexión a la base de datos desde código y la administración del modelo de la base de datos, para ello se utilizo la  librería [mongoose](https://mongoosejs.com/). Esta librería permite administrar el modelo de la base de datos, todas las peticiones y acciones CRUD.

### Conexión

para la conexión con la base de datos se necesita la siguiente información

- Host
- Puerto
- Usuario
- Contraseña
- Base de datos

Con estos datos se debe construir la siguiente cadena 

```jsx
const MONGO_URL="mongodb://<MONGO_USER>:<MONGO_PWD>@<MONGO_HOST>:<MONGO_PORT>/<MONGO_DB>"
// Ejemplo => "mongodb://admin:admin123@localhost:27017/ripuy" 
```

Esta cadena para la conexión ya esta configurada en el código, solo hay que agregar la información en las variables de entorno.

En el directorio **src/models** se encuentra el archivo **index.ts** en este archivo se encuentra el código para conectar con ayuda de mongoose

```tsx
import mongoose from 'mongoose'

const mongoConnection: any = () => {
    return mongoose.connect( MONGO_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    } )
}
```

Así como se abre una conexión es buena practica hacer cierre de esta cuando una petición a terminado de ejecutarse, para ello se tiene las siguientes instrucciones

```tsx
const closeConnection: any = () => {
    return mongoose.connection.close()
}
```

### Modelos

Si bien es conocido que mongo es schemaless, donde no se define un esquema o modelo, mongoose necesita saber con que atributos poder trabajar desde código, para ello es necesario utilizar las funciones **Schema** y **model**. Un pequeño ejemplo de como se crear un modelo es el siguiente 

```tsx
import { Schema, model } from 'mongoose'

const eventoSchema = new Schema( {
    nombre:{ type: String, required: true },
    email:{ type: String, required: false },
}, { collection: 'user', timestamps:{ createdAt: 'created_at', updatedAt: 'updated_at' } } )

const Evento = model( 'User', eventoSchema )
export default Evento
```

En el anterior ejemplo, en atributo **collection** hace referencia al nombre de la colección en la base de datos y el modelo con el cual se va a trabajar en el código para poder hacer peticiones es **“User”.**

Todos los modelos definidos para el proyecto están distribuidos de la siguiente manera

```bash
packages/api/src/models
├── academic
├── authentication
├── personal
├── posts
└── professional
```

## GraphQL

GraphQL es lenguaje de consulta como alternativa a las API REST, esta tecnología fue creada en el 2012 por Facebook para poder optimizar sus peticiones. De lo mas destacado de GraphQL se tiene:

- Describe sus datos
- Pide lo que quieres
- Obtén resultados predecibles

Para más información sobre esta tecnología ingresa [aquí](https://graphql.org/).

En resumidas palabras el funcionamiento de GraphQL se resume en la construcción de un **schema** y de **resolves**

### Esquema

La definición de un schema en GraphQL permite determinar cuales serán las **Queries** y **Mutations** que estarán habilitados para hacer las peticiones. El termino Query hace referencia a toda petición que retorna datos, mientras que un Mutation realiza acciones de creación, modificación o eliminación de datos, esto es estándar que se utiliza pero siempre se puede crear un grupo de peticiones como se desee. 

Este esquema también nos permite determinar que tipo de datos se espera y que datos puedo pedir en un petición, como una especia de tipado de consultas. ejemplo de definición de un schema simple

```graphql
type Query {
	users: [User!]
}
type Mutation {
	updateUser(where: UserWhereUniqueInput! data: UserUpdateDataInput): User!
}

type User {
	id: String!
	nombre: String
	email: String
}

input UserWhereUniqueInput {
	id: String!
}
input UserUpdateDataInput {
	name: String
	email: String
}
```

En este ejemplo se ha creado un dos peticiones, la primera es “users” que es de tipo query y lo que hace es retornar un array de users. La segunda petición es “updateUser” de tipo mutation donde recibe un parámetro “where” que contiene el id del usuario a actualizar y el parámetro “data” que trae la data que se pretende actualizar, al final esta petición retorna el usuario actualizado

El esquema del proyecto de encuentra estructurado de la siguiente manera

```bash
packages/api/src/schema
├── academic.ts
├── commons.ts
├── index.ts
├── personal.ts
├── post.ts
└── professional.ts
```

### Resolvers

Los resolvers son la lógica que se realiza en cada petición definida en el esquema, en el anterior ejemplo están definidas “users” y “updateUser”. Los resolvers para estas dos peticiones serian las siguientes funciones

```tsx
const users = async ( _, _args, { models } ) => {
		// Consulta a mongo mendinte los models definidos con mongoose
    return models.User.find( {} )
}

const updateUser = async ( _, args, { models } ) => {
    // parametros where y data enviados
		const { id } = get( args, 'where', {} )
    const data = get( args, 'data', {} )
		// Peticion de actualización a mongo
    const user = await models.User.findByIdAndUpdate( id, data, { new: true } )
    if ( !user )
        throw new Error( 'Event not found' )
    return user
}
```

Los resolvers construidos en el proyecto se encuentran estructurados de la siguiente manera

```bash
packages/api/src/resolvers
├── academic
│   ├── index.ts
│   ├── mutations.ts
│   └── queries.ts
├── auth.ts
├── commons
│   ├── index.ts
│   └── queries.ts
├── index.ts
├── personal
│   ├── index.ts
│   ├── mutations.ts
│   └── queries.ts
├── post
│   ├── index.ts
│   ├── mutations.ts
│   ├── queries.ts
│   └── utils.ts
├── professional
│   ├── index.ts
│   ├── mutations.ts
│   ├── queries.ts
│   └── utils.ts
├── test.LOCAL.ts
└── utils
    ├── pagination.ts
    └── validateRole.ts
```

### Apollo GraphQL

[Apollo](https://www.apollographql.com/) es una plataforma para crear una capa de comunicación que lo ayuda a administrar el flujo de datos entre sus clientes de aplicaciones (como aplicaciones web) y sus servicios de backend que utilizan GraphQL como lenguaje de consulta.

Apollo sera el modulo al cual se le proporcionara el esquema y los resolvers para que se encargue de proporcionar una servidor de comunicación para el api, esta configuración esta en el archivo **src/graphql.ts**

```tsx
import { ApolloServer } from 'apollo-server-express'
import resolvers from './resolvers'
import schema from './schema'
import { models } from './models'
import { userSession } from './plugins/authentication'
import { ApolloServerExpressConfig } from 'apollo-server-express/src/ApolloServer'
import { Context } from './utils'

const config: ApolloServerExpressConfig = {
    playground: true,
    typeDefs: schema,
    resolvers,
    context: async ( { req } ): Promise<Context> => {
        if( req.method === 'POST' ) {
            return {
                models,
                session: await userSession( req, models )
            }
        }
    }
}
const server = new ApolloServer( config )

export default server
```

En este archivo también es posible enviar in contexto en cada petición realizada, en este caso hacemos una verificación de autenticación del usuario que esta realizando la petición y la información de este es enviada en el contexto al igual que los models definidos mediante mongoose para poder hacer peticiones a la base de datos de mongo.

## Sesión

### Flujo de autenticación

El sistema de autenticacion esta basado en el estándar [OAuth2.0](https://oauth.net/), utilizando la verificación por código y almacenando el token de sesión en las cookies del navegador para mantener sesión activa. El siguiente diagrama de secuencia muestra como es el flujo de autenticacino implementado. 

[Flujo de Autenticacion Ripuy.jpg](https://drive.google.com/file/d/1dEVjEnN2cmabAgFjnalfi9-cUAwDdStK/view?usp=sharing)

Todo el código utilizado para la autenticacion se encuentra estructurado de la siguiente manera

```bash
packages/api/src/plugins/authentication
├── authSession.ts
├── index.ts
├── loginValidation.ts
└── userSession.ts
```

### Primer ingreso y recuperación de contraseña

El sistema no cuenta con un registro inicial puesto que los datos de los egresados son cargados a la base de datos proporcionados por el departamento de ingeniería de sistemas, en ese sentido debe haber un primer inicio donde el usuario pueda restaurar su contraseña y poder ingresar. Este mismo flujo esta pensado para la recuperación de contraseña cuando el usuario la ha olvidado, es importante que el usuario tenga claro cual es su email registrado en la universidad porque es a ese corre donde se le enviara un email de recuperación.

El flujo de recuperación de contraseña es el siguiente

[Restore password flow ripuy.jpg](https://drive.google.com/file/d/1sfG2OEEdlzU798sCYCa30n-ou3m6tnxk/view?usp=sharing)

## Rutas del API

En este apartado vamos a listar todas las rutas o paths configurados en el api

- **/**: Path tipo GET de prueba que retorna un Hola mundo!! usado como identificador que el api se encuentra funcionando
- **/login**: Path tipo POST para iniciar el flujo de autenticación, recibe como body la identification y password y retorna el **code_grant** de la sesión y **first_login** para identificar si es la primera vez que inicia sesión
- **/token**: Path tipo GET que hace parte del flujo de autenticación que recibe en los params el **code_grant** y el **redirec_uri** a donde se debe establecer la cookie de la sesión, este path hace un redireccionamiento 302 hacia el cliente web de Ripuy
- **/user**: Path tipo GET utilizado para saber que usuario se encuentra en sesión según la cookie recibida en los headers de la petición, si hay sesión se retorna la información del usuario de lo contrario se retorna mensaje 401 de no autorizado
- **/logout**: Path tipo GET que elimina todas las sesión del usuario y elimina la cookie del navegador haciendo un redireccionamiento 302
- **/password/email**: Path tipo POST que hace parte del proceso de recuperación de contraseña, este recibe **identification** en el body de la petición y este se encarga de enviar un email de recuperación de contraseña con un token especial con un tiempo mínimo de espiración
- **/password/restore**: Path tipo POST que hace parte del proceso final de recuperación de contraseña que recibe la **new_password** y el **token** de recuperación que fue enviado previamente al email. Si todo esta correcto genera una nuevo hash de contraseña y retornara un 200 con estado successful
- **/graphql**: Path tipo POST creado por Apollo server que se encarga de recibir todas las peticiones graphql para las acciones CRUD hacia la base de datos. GraphQL proporciona una documentación de su esquema y resolvers con solo acceder al path, para hacer esto es necesario instalar o usar algún cliente grqphql como [GraphQL Playground](https://www.graphqlbin.com/) o [Altair](https://altair.sirmuel.design/)

## Pruebas funcionales

El set de pruebas funcionales creadas tiene el objetivo de probar el api en cada una de las consultas GraphQL, ya sean para solicitud, creación, modificación y eliminación de datos (CRUD). 

Estas pruebas necesitan de la instalación del **MongoDB Community Edition** en la version 5.0 en el sistema local, puesto que se conecta localmente a este server y luego se restauran un set de datos de prueba. Para la instalación de mongo seguir la siguiente [guía](https://docs.mongodb.com/manual/administration/install-community/).

Una vez mongo este instalado se ejecuta el siguiente comando para correr las pruebas

```bash
yarn test
# output
# Test Suites: 5 passed, 5 total
# Tests:       78 passed, 78 total
# Snapshots:   0 total
# Time:        25.134 s
# Ran all test suites.
# Done in 31.73s.
```

Este set de pruebas se encuentra configurado y estructurado de la siguiente manera

```bash
packages/api/src/resolvers/test
├── academic.test.ts
├── commons.test.ts
├── fixtures
│   ├── academic.ts
│   ├── commons.ts
│   ├── personal.ts
│   ├── post.ts
│   └── professional.ts
├── personal.test.ts
├── post.test.ts
├── professional.test.ts
└── server
    ├── apolloServerTest.ts
    ├── backup
    └── launchTestDB.sh
```

## Contenerización del API

Este apartado no es necesarimente obligatorio, pero durante el desarrollo esta proceso fue muy util, sobre todo con la puesta del servidor api en **staging** y **production**. La contenerizacion consiste en aislar el api en un ambiente que no es accesible por tu sistema pero que cuenta con todo los requerimientos suficientes para poder funcionar. Para esto se ha utilizado la tecnología **Docker**, y para iniciar hay que intalar docker en nuestro sistema (explicado en la parte de [configuración del entorno de desarrollo](https://www.notion.so/Paquete-API-6bad08c9f2c34089b4dc26c428fbc530))

Una vez ya instalado docker junto a docker-compose, en el directorio del paquee api hay un archivo **docker-compose.yaml** que tiene la configuración del contenedor par el api, solo basta con ejecutar el siguiente comando

```bash
docker-compose up -d 

#Para comprobar que el container este arriba ejecutar
docker-compose ps
```

Si el container ya esta arriba entonces se tendrá el server saliendo por el puerto 8180. Al hacerlo de esta manera nos permitirá fácilmente llevar el proyecto a un servidor y ejecutar el server sin necesidad de instalar nodejs y todas las demás dependencias del proyecto, ya que el contenedor internamente se encarga de eso.