import { gql } from 'apollo-server-express'

export default gql`
    extend type Query {
        empresas( take: Int, cursor: CursorPaginationInput ): [Empresa!]
        empresa( where: EmpresaWhereUniqueInput! ): Empresa
        actividadesActuales: [ActividadNoLaboral!]
        canalesComunicacion: [CanalComunicacion]
        razonesDificultadTrabajo: [RazonDificultad]
        informacionLaboral( where: InformacionLaboralWhereInput! ): Laboral
        informacionNoLaboral( where: InformacionNoLaboralWhereInput! ): NoLaboral
    }
    extend type Mutation {
        updateInformacionLaboral( 
            where: InformacionLaboralWhereInput! 
            data: UpdateInformacionLaboralDataInput! 
        ): User
        updateInformacionNoLaboral( 
            where: InformacionNoLaboralWhereInput! 
            data: UpdateInformacionNoLaboralDataInput! 
        ): User
    }
    type Empresa{
        id: String!
        nombre: String!
        direccion: String
        sitio_web: String
        telefono: String
        correo: Email
    }
    type Laboral{
        id: String!
        fecha_registro: Date!
        salario: Salario
        fecha_inicio: Date!
        fecha_finalizacion: Date
        ocupacion: String
        tipo_contrato: TipoContrato
        municipio: Municipio!
        empresa: Empresa
        horas_trabajo: Int
        grado_satisfaccion: GradoSatisfaccion
        role: String
    }
    type RazonDificultad{
        id: String!
        descripcion: String!
    }
    type CanalComunicacion{
        id: String!
        descripcion: String!
    }
    type ActividadNoLaboral{
        id: String!
        descripcion: String!
    }
    type NoLaboral{
        id: String!
        fecha_registro: Date!
        razon_dificultad: RazonDificultad
        canal_busqueda: CanalComunicacion
        tiempo_busqueda: Int
        actividad_actual: ActividadNoLaboral
    }
    enum Salario{
        MENOS_UNO_SMLV
        UNO_DOS_SMLV
        TRES_CUATRO_SMLV
        MAS_CUATRO_SMLV
    }
    enum TipoContrato{
        PRESTACION_SERVICIOS
        TERMINO_FIJO
        TEMRINO_INDEFINIDO
        APRENDIZAJE
        OCACIONAL
    }
    enum GradoSatisfaccion{
        MUY_SATISFECHO
        SATISFECHO
        REGULAR
        INSATISFECHO
        MUY_INSATISFECHO
    }
    input EmpresaWhereUniqueInput{ id: String! }
    input ActividadActualWhereUniqueInput{ id: String!}
    input CanalBusquedaWhereUniqueInput{ id: String! }
    input RazonDificultadWhereUniqueInput{ id: String! }
    input InformacionLaboralWhereInput{ user: UserWhereUniqueInput! }   
    input InformacionNoLaboralWhereInput{ user: UserWhereUniqueInput! }
    input UpdateInformacionLaboralDataInput{
        empresa: EmpresaWhereUniqueInput
        ocupacion: String
        role: String
        tipo_contrato: TipoContrato
        salario: Salario
        fecha_inicio: Date
        fecha_finalizacion: Date
        municipio: MunicipioWhereUniqueInput
        horas_trabajo: Int
        grado_satisfaccion: GradoSatisfaccion
    }
    input UpdateInformacionNoLaboralDataInput{
        actividad_actual: ActividadActualWhereUniqueInput
        canales_busqueda: [CanalBusquedaWhereUniqueInput!]
        tiempo_busqueda: Int
        razon_dificultad: RazonDificultadWhereUniqueInput
    }
`
