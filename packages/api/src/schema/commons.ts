import { gql } from 'apollo-server-express'

export default gql`
    extend type Query {
        pais( where: PaisWhereUniqueInput! ): Pais
        paises: [Pais!]!
        municipio( take: Int, cursor: CursorPaginationInput, where: MunicipioWhereUniqueInput! ): Municipio
        municipios: [Municipio!]!
    }
    type ObjectId{ id: String }
    type Pais{ 
        id: String!
        codigo: String!
        nombre: String!
        codigo_alfanumerico: String        
    }
    type Departamento{
        id: String!
        codigo: String!
        nombre: String!
        codigo_alfanumerico: String
        pais: Pais
    }
    type Region{
        id: String!
        codigo: String!
        nombre: String!
    }
    type Municipio{
        id: String!
        codigo: String!
        nombre: String!
        departamento: Departamento!
        region: Region
    }
    input PaisWhereUniqueInput{ id: String! }
    input MunicipioWhereUniqueInput{ id: String! }
`
