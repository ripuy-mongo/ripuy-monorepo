import { gql } from 'apollo-server-express'

export default gql`    
    extend type Query {
        clasesEtnograficas: [ClaseEtnografica!]
        claseEtnografica( where: ClaseEtnograficaWhereUniqueInput! ): ClaseEtnografica
        discapacidades: [Discapacidad]
        informacionPersonal( where: UserWhereUniqueInput! ): Personal
        egresado( where: UserWhereUniqueInput! ): User
    }
    extend type Mutation {
#        createInformacionPersonal( 
#            where: CreateInformacionPersonalWhereInput! 
#            data: CreateInformacionPersonalDataInput! 
#        ): User
        updateInformacionPersonal( 
            where: UpdateInformacionPersonalWhereInput! 
            data: UpdateInformacionPersonalDataInput! 
        ): User
        changeFirstLoginStatus: FirstLoginStatus!
        setPermisos( data: SetPermisoData! ): Permiso!
    }
    type User{
        id: String!
        identificacion: String!
        tipo_identificacion: IdentityType
        pais_expedicion: Pais
        nombres: String!
        apellidos: String!
        genero: Gender
        fecha_nacimiento: Date
        correo: Email!
        celular: String!
        permiso: Boolean
        primer_ingreso: Boolean
    }
    type Personal{
        id: String!
        municipio_nacimiento: Municipio
        municipio_recidencia: Municipio
        clase_etnografica: ClaseEtnografica
        tiene_discapacidad: Boolean
        discapacidad: Discapacidad
        motivo_discapacidad: String
    }
    type Permiso {
        allowed: Boolean!
    }
    type FirstLoginStatus{
        status: Boolean!
    }
    type ClaseEtnografica{
        id: String!
        nombre: String!
    }
    type GrupoDiscapacidad{
        id: String!
        nombre: String!
    }
    type Discapacidad{
        id: String!
        nombre: String!
        grupo: GrupoDiscapacidad!
    }
    enum IdentityType{
        CC CE TI
    }
    enum Gender{
        MASCULINO
        FEMENINO
        OTRO
    }
    input SetPermisoData{ allow: Boolean! }
    input ClaseEtnograficaWhereUniqueInput{ id: String! }
    input DiscapacidadWhereUniqueInput{ id: String! }
    input UserWhereUniqueInput{ id: String! }
#    input CreateInformacionPersonalWhereInput{ user: UserWhereUniqueInput! }
    input UpdateInformacionPersonalWhereInput{ user: UserWhereUniqueInput! }
#    input CreateInformacionPersonalDataInput{ 
#        genero: Gender
#        fecha_nacimiento: Date
#        celular: String
#        correo: String
#        municipio_recidencia: MunicipioWhereUniqueInput
#        municipio_nacimiento: MunicipioWhereUniqueInput
#        clase_etnografica: ClaseEtnograficaWhereUniqueInput
#        tiene_discapacidad: Boolean
#        discapacidad: DiscapacidadWhereUniqueInput
#        motivo_discapacidad: String
#    }
    input UpdateInformacionPersonalDataInput{
        genero: Gender
        fecha_nacimiento: Date
        celular: String
        correo: String
        municipio_recidencia: MunicipioWhereUniqueInput
        municipio_nacimiento: MunicipioWhereUniqueInput
        clase_etnografica: ClaseEtnograficaWhereUniqueInput
        tiene_discapacidad: Boolean
        discapacidad: DiscapacidadWhereUniqueInput
        motivo_discapacidad: String
    }
`
