import { gql } from 'apollo-server-express'

export default gql`
    extend type Query {
        eventos: [Event!]
        evento( where: EventWhereUniqueInput ): Event
        ofertas( take: Int, cursor: CursorPaginationInput, where: OfertaWhereInput ): [Oferta!]
        oferta( where: OfertaWhereUniqueInput ): Oferta
        interesados( skip: Int, take: Int, where: InteresadosWhereInput! ): InterestedList!
        categorias: [Categoria!]
        egresados( take: Int, cursor: CursorPaginationInput, where: EgresadoWhereInput ): [User!]  
    }
    extend type Mutation {
        createInteresado( data: CreateInteresadoDataInput! ): ObjectId
        createOferta( data: CreateOfertaDataInput! ): Oferta
        updateOferta( where: OfertaWhereUniqueInput! data: UpdateOfertaDataInput! ): Oferta
        deleteOferta( where: OfertaWhereUniqueInput! ): ObjectId
        deleteInteresado( where: DeleteInteresadoWhereInput! ): ObjectId
        createEvento( data: CreateEventoDataInput! ): Event
        updateEvento( where: EventoWhereInput! data: UpdateEventoWhereInput! ): Event
        deleteEvento( where: EventoWhereInput! ): ObjectId
    }
    type Event{
        id: String!
        nombre: String!
        descripcion: String
        poster: String!
        direccion: String
        telefono: String
        email: Email
        url: URL
        fechas: [Date]
    }
    type Categoria{
        id: String!
        nombre: String
        icon: String
    }
    type Solicitante {
        nombre: String!
        telefono: String
        correo: String
    }
    type Oferta{
        id: String!
        descripcion: String!
        telefono: String
        correo: Email
        fecha_publicacion: DateTime!
        categoria: Categoria
        solicitante: Solicitante
        aplicado: Boolean
    }
    type Interesado {
        user: User!
        fecha_aplicacion: DateTime
    }
    type InterestedList{
        totalCount: Int
        interesados: [Interesado!]
    }
    enum OrganizationType{
        PUBLICA
        PRIVADA
        INDEPENDIENTE
    }
    input EventWhereUniqueInput{ id: String! }
    input OfertaWhereUniqueInput{ id: String! }
    input OfertaWhereInput{ search: String, applied: Boolean }
    input EgresadoWhereInput{ search: String }
    input CategoriaWhereUniqueInput{ id: String! }
    input SolicitanteWhereUniqueInput{ id: String! }
    input InteresadosWhereInput{ oferta: OfertaWhereUniqueInput! }
    input EventoWhereInput{ id: String! }
    input CreateInteresadoDataInput{ 
        oferta: OfertaWhereUniqueInput!
    }
    input DeleteInteresadoWhereInput {
        oferta: OfertaWhereUniqueInput!
    }
    input UpdateOfertaDataInput{
        categoria: CategoriaWhereUniqueInput
        solicitante: Json
        descripcion: String
        telefono: String
        correo: String
    }
    input CreateOfertaDataInput{
        solicitante: Json!
        categoria: CategoriaWhereUniqueInput!
        descripcion: String!
        telefono: String
        correo: String
    }
    input CreateEventoDataInput {
        nombre: String!
        descripcion: String
        poster: String!
        direccion: String
        telefono: String
        email: String
        url: URL
        fechas: [Date!]
    }
    input UpdateEventoWhereInput {
        nombre: String
        descripcion: String
        poster: String
        direccion: String
        telefono: String
        email: String
        url: URL
        fechas: [Date!]
    }
`
