import { gql } from 'apollo-server-express'

export default gql`
    extend type Query {
        sedes: [IesCampus!]
        modalidades: [ModalidadAcademica!]
        motivosIngreso: [MotivoIngreso!]
        periodos: [PeriodoAcademico!]
        jornadas: [JornadaAcademica!]
        formasFinanciacion: [Financiacion!]
        colegios( take: Int, cursor: CursorPaginationInput ): [Colegio!]
        informacionAcademica( where: InformacionAcademicaWhereInput! ): Academico
    }
    extend type Mutation {
        updateInformacionAcademica( 
            where: InformacionAcademicaWhereInput! 
            data: InformacionAcademicaDataInput! 
        ): Academico
    }
    
    type Academico{
        id: String!
        fecha_registro: Date
        sede: IesCampus
        programa: Programa
        modalidad_academica: ModalidadAcademica
        motivo_ingreso: MotivoIngreso
        es_primera_opcion: Boolean
        intentos_admision: Int
        fecha_inicio_programa: Date
        fecha_finalizacion_materias: Date
        fecha_grado: Date
        duracion_programa: Int
        periodo: PeriodoAcademico
        jornada: JornadaAcademica
        promedio_notas: Float
        mension: Boolean
        trabajo_grado: DegreeWorkType
        titulo_trabajo_grado: String
        linea_investigacion: String
        financiacion: Financiacion
        tipo_beca: TipoBeca
        colegio: Colegio
    }
    type Ies{
        id: String!
        codigo: String!
        nombre: String!
        seccional: IesSeccional!
        #        naturaleza_juridica: 
        sector: IesSector!
        nivel_academico: IesType!
        municipio: Municipio!
    }
    type Colegio{
        id: String!
        codigo: String
        nombre: String!
        municipio: Municipio
        calendario: SchoolCalendarType
        naturaleza: SchoolType
        jornada: SchoolTimeEnum
    }
    type NivelAcademico{
        id: String!
        nombre: String!
    }
    type NivelFormacion{
        id: String!
        nombre: String!
        nivel_academico: NivelAcademico
    }
    type Programa{
        id: String!
        codigo: String!
        nombre: String!
        nivel_formacion: NivelFormacion
    }
    type MotivoIngreso{
        id: String!
        descripcion: String!
    }
    type Financiacion{
        id: String!
        descripcion: String!
    }
    type TipoBeca{
        id: String!
        description: String!
    }
    enum IesCampus{
        PRINCIPAL
        EXTENSION
    }
    enum ModalidadAcademica{
        PREGRADO
        POSGRADO
        DIPLOMADO
    }
    enum PeriodoAcademico{
        SEMESTRAL
        ANUAL
    }
    enum JornadaAcademica{
        DIURNO
        NOCTURNO
    }
    enum IesSeccional{
        PRINCIPAL
        EXTENCION
    }
    enum IesSector{
        PUBLICA
        PRIVADA
    }
    enum IesType{
        TECHNICAL
        TECHNOLOGICAL
        PROFESSIONAL
    }
    enum SchoolCalendarType {
        A
        B
    }
    enum SchoolType {
        OFICIAL
        NO_OFICIAL
    }
     enum SchoolTimeEnum{
        MANANA
        TARDE
        NOCHE
        FIN_SEMANA
    }
    enum DegreeWorkType{    
        TESIS
        PASANTIA
        APLICATIVO
    }
    input MotivoIngresoWhereUniqueInput{ id: String! }
    input FinanciacionWhereUniqueInput{ id: String! }
    input ColegioWhereUniqueInput{ id: String! }
    input InformacionAcademicaWhereInput {
        user: UserWhereUniqueInput!
    }
    input InformacionAcademicaDataInput {
        sede: IesCampus
        modalidad_academica: ModalidadAcademica
        motivo_ingreso: MotivoIngresoWhereUniqueInput
        es_primera_opcion: Boolean
        intentos_admision: Int
        fecha_inicio_programa: Date
        fecha_finalizacion_materias: Date
        fecha_grado: Date
        periodo: PeriodoAcademico
        jornada: JornadaAcademica
        promedio_notas: Float
        mension: Boolean
        trabajo_grado: DegreeWorkType
        titulo_trabajo_grado: String
        linea_investigacion: String
        financiacion: FinanciacionWhereUniqueInput
        colegio: ColegioWhereUniqueInput
    }
`
