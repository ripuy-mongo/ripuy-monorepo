import { gql } from 'apollo-server-express'
import Commons from './commons'
import Personal from './personal'
import Post from './post'
import Academic from './academic'
import Professional from './professional'

const schema = gql`
    scalar Date
    scalar DateTime
    scalar Email
    scalar URL
    scalar UUID
    scalar Json
    
    input CursorPaginationInput { id: String! }
    
    type Query { _: Boolean }
    type Mutation { 
        _: Boolean
    }
`
export default [ schema, Personal, Post, Commons, Academic, Professional ]
