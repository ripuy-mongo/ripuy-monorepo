import mongoose from 'mongoose'
import { MONGO_URL } from '../utils'
import AuthModels from './authentication'
import PersonalModels from './personal'
import AcademicModels from './academic'
import CommonsModels from './commons'
import ProfessionalModels from './professional'
import PostModels from './posts'

const mongoConnection: any = () => {
    return mongoose.connect( MONGO_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    } )
}

const closeConnection: any = () => {
    return mongoose.connection.close()
}

const models: any = {
    ...PersonalModels,
    ...AuthModels,
    ...AcademicModels,
    ...CommonsModels,
    ...ProfessionalModels,
    ...PostModels
}

export { mongoConnection, closeConnection, models }
