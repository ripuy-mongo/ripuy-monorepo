import { Schema, model } from 'mongoose'
import { ContractType, Salarios, SatisfactionLevel } from './utils'

const laboralSchema = new Schema( {
    user_id:{ type: Schema.Types.ObjectId, ref: 'User', required: true, unique: true },
    salario:{ type: String, enum: Salarios, required: false },
    fecha_inicio:{ type: Date, required: true },
    fecha_finalizacion:{ type: Date, required: false },
    ocupacion:{ type: String, required: false },
    tipo_contrato:{ type: String, enum: ContractType, required: false },
    municipio_id:{ type: Schema.Types.ObjectId, ref: 'Municipio', required: true },
    empresa_id:{ type: Schema.Types.ObjectId, ref: 'Empresa', required: true },
    horas_trabajo:{ type: Number, required: false },
    grado_satisfaccion:{ type: String, enum: SatisfactionLevel, required: false },
    role:{ type: String, required: true }
}, { collection: 'laborales', timestamps:{ createdAt: 'fecha_registro', updatedAt: 'fecha_actualizacion' } } )

const Laboral = model( 'Laboral', laboralSchema )
export default Laboral
