enum ContractTypeEnum{
    PRESTACION_SERVICIOS = 'PRESTACION_SERVICIOS',
    TERMINO_FIJO = 'TERMINO_FIJO',
    TEMRINO_INDEFINIDO = 'TEMRINO_INDEFINIDO',
    APRENDIZAJE = 'APRENDIZAJE',
    OCACIONAL = 'OCACIONAL'
}
export const ContractType = [
    ContractTypeEnum.PRESTACION_SERVICIOS,
    ContractTypeEnum.TERMINO_FIJO,
    ContractTypeEnum.TEMRINO_INDEFINIDO,
    ContractTypeEnum.APRENDIZAJE,
    ContractTypeEnum.OCACIONAL
]

enum SatisfactionLevelEnum{
    MUY_SATISFECHO = 'MUY_SATISFECHO',
    SATISFECHO = 'SATISFECHO',
    REGULAR = 'REGULAR',
    INSATISFECHO = 'INSATISFECHO',
    MUY_INSATISFECHO = 'MUY_INSATISFECHO'
}
export const SatisfactionLevel = [
    SatisfactionLevelEnum.MUY_SATISFECHO,
    SatisfactionLevelEnum.SATISFECHO,
    SatisfactionLevelEnum.REGULAR,
    SatisfactionLevelEnum.INSATISFECHO,
    SatisfactionLevelEnum.MUY_INSATISFECHO
]

enum SalarioEnum{
    MENOS_UNO_SMLV = 'MENOS_UNO_SMLV',
    UNO_DOS_SMLV = 'UNO_DOS_SMLV',
    TRES_CUATRO_SMLV = 'TRES_CUATRO_SMLV',
    MAS_CUATRO_SMLV = 'MAS_CUATRO_SMLV'
}
export const Salarios = [
    SalarioEnum.MENOS_UNO_SMLV,
    SalarioEnum.UNO_DOS_SMLV,
    SalarioEnum.TRES_CUATRO_SMLV,
    SalarioEnum.MAS_CUATRO_SMLV,
]
