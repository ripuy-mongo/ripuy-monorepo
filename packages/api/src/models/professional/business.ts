import { Schema, model } from 'mongoose'

const empresaSchema = new Schema( {
    nombre:{ type: String, required: true},
    direccion:{ type: String },
    sitio_web:{ type: String },
    telefono:{ type: String },
    correo:{ type: String }
}, { collection: 'empresas' } )

const razonDificultadEmpresa = new Schema( {
    descripcion:{ type: String, required: true },
}, { collection: 'razonDificultadEmpresa' } )

const BusinessModels = {
    Empresa: model( 'Empresa', empresaSchema ),
    RazonDificultadEmpresa: model( 'RazonDificultadEmpresa', razonDificultadEmpresa )
}
export default BusinessModels
