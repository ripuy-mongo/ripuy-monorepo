// import OccupationModels from './occupation'
import BusinessModels from './business'
import Laboral from './job'
import JoblessModels from './jobless'

const ProfessionalModels = {
    // ...OccupationModels,
    ...BusinessModels,
    ...JoblessModels,
    Laboral,
}

export default ProfessionalModels
