import { Schema, model } from 'mongoose'

const dificultadLaboralSchema = new Schema( {
    descripcion:{ type: String, required: true }
}, { collection: 'dificultadLaboral' } )

const canalComunicacionSchema = new Schema( {
    descripcion:{ type: String, required: true }
}, { collection: 'canalComunicacion' } )

const actividadNoLaboralSchema = new Schema( {
    descripcion:{ type: String, required: true }
}, { collection: 'actividadNoLaboral' } )

const noLaboralSchema = new Schema( {
    user_id:{ type: Schema.Types.ObjectId, ref: 'User', required: true, unique: true },
    razon_dificultad_id:{ type: Schema.Types.ObjectId, ref: 'DificultadLaboral', required: false },
    canales_busqueda_id:[{ type: Schema.Types.ObjectId, ref: 'CanalComunicacion', required: false }],
    tiempo_busqueda:{ type: Number, required: true },
    actividad_actual_id:{ type: Schema.Types.ObjectId, ref: 'ActividadNoLaboral', required: true }
}, { collection: 'noLaboral', timestamps:{ createdAt: 'fecha_registro', updatedAt: 'fecha_actualizacion' } } )

const JoblessModels = {
    DificultadLaboral: model( 'DificultadLaboral', dificultadLaboralSchema ),
    CanalComunicacion: model( 'CanalComunicacion', canalComunicacionSchema ),
    ActividadNoLaboral: model( 'ActividadNoLaboral', actividadNoLaboralSchema ),
    NoLaboral: model( 'NoLaboral', noLaboralSchema )
}

export default JoblessModels
