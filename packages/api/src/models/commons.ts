import { Schema, model } from 'mongoose'

const municipioSchema = new Schema( {
    codigo:{ type: String, unique: true, required: true },
    nombre:{ type: String, required: true },
    departamento_id:{ type: Schema.Types.ObjectId, ref: 'Departamento', required: true },
    region_id:{ type: Schema.Types.ObjectId, ref: 'Region', required: false }
}, { collection: 'municipios' } )

const departamentoSchema = new Schema( {
    codigo:{ type: String, unique: true, required: true },
    nombre:{ type: String, required: true },
    codigo_alfanumerico:{ type: String, required: false },
    pais_id:{ type: Schema.Types.ObjectId, ref: 'Pais', required: true }
}, { collection: 'departamentos' } )

const regionSchema = new Schema( {
    codigo:{ type: String, unique: true, required: true },
    // cod_pais:{ type: Schema.Types.ObjectId, ref: 'Pais',required: false },
    nombre:{ type: String, required: true }
}, { collection: 'regiones' } )

const paisSchema = new Schema( {
    codigo:{ type: String, unique: true, required: true },
    nombre:{ type: String, required: true },
    codigo_alfanumerico:{ type: String, required: false }
}, { collection: 'paises' } )

const CommonsModels = {
    Municipio: model( 'Municipio', municipioSchema ),
    Departamento: model( 'Departamento', departamentoSchema ),
    Region: model( 'Region', regionSchema ),
    Pais: model( 'Pais', paisSchema )
}

export default CommonsModels
