import { Schema, model } from 'mongoose'

const eventoSchema = new Schema( {
    nombre:{ type: String, required: true },
    descripcion:{ type: String, required: false },
    poster:{ type: String, required: true },
    direccion:{ type: String, required: false },
    telefono:{ type: String, required: false },
    email:{ type: String, required: false },
    url:{ type: String, required: false },
    fechas:[{ type: Date, required: false }],
}, { collection: 'eventos', timestamps:{ createdAt: 'created_at', updatedAt: 'updated_at' } } )

const Evento = model( 'Evento', eventoSchema )
export default Evento
