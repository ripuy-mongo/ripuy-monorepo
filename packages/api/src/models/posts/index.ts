import OfferModels from './offer'
import Evento from './event'

const PostModels = {
    Evento,
    ...OfferModels
}
export default PostModels
