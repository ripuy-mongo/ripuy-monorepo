import { Schema, model } from 'mongoose'

const categoriaSchema = new Schema( {
    nombre:{ type: String, unique: true, required: true },
    icon:{ type: String, required: false }
}, { collection: 'categorias' } )

const ofertaSchema = new Schema( {
    descripcion:{ type: String, required: true },
    telefono:{ type: String, required: false },
    correo:{ type: String, required: false },
    categoria_id:{ type: Schema.Types.ObjectId, ref: 'Categoria', required: true },
    solicitante:{ type: JSON, required: true }
}, { collection: 'ofertas', timestamps:{ createdAt: 'fecha_publicacion', updatedAt: 'fecha_actualizacion' } } )

const interesadoSchema = new Schema( {
    oferta_id:{ type: Schema.Types.ObjectId, ref: 'Oferta', required: true },
    egresado_id:{ type: Schema.Types.ObjectId, ref: 'User', required: true },
}, { collection: 'interesados', timestamps:{ createdAt: 'fecha_aplicacion', updatedAt: 'fecha_actualizacion' } } )

const OfferModels = {
    Categoria: model( 'Categoria', categoriaSchema ),
    Oferta: model( 'Oferta', ofertaSchema ),
    Interesado: model( 'Interesado', interesadoSchema )
}
export default OfferModels
