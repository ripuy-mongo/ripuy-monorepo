// TODO: Cambiar a español los valores
enum IesCampusTypeEnum{
    PRINCIPAL = 'PRINCIPAL',
    EXTENSION = 'EXTENSION'
}
export const IesCampusType = [IesCampusTypeEnum.PRINCIPAL, IesCampusTypeEnum.EXTENSION]

enum IesSectorTypeEnum {
    PUBLIC,
    PRIVATE
}
export const IesSectorType = [IesSectorTypeEnum.PRIVATE, IesSectorTypeEnum.PUBLIC]

enum IesTypeEnum {
    TECHNICAL,
    TECHNOLOGICAL,
    PROFESSIONAL
}
export const IesType = [IesTypeEnum.PROFESSIONAL, IesTypeEnum.TECHNICAL, IesTypeEnum.TECHNOLOGICAL]

enum SchoolCalendarTypeEnum{
    A = 'A',
    B ='B'
}
export const SchoolCalendarType = [ SchoolCalendarTypeEnum.A, SchoolCalendarTypeEnum.B ]

enum SchoolTypeEnum{
    OFICIAL = 'OFICIAL',
    NO_OFICIAL = 'NO_OFICIAL'
}
export const SchoolType = [ SchoolTypeEnum.OFICIAL, SchoolTypeEnum.NO_OFICIAL ]

enum SchoolTimeEnum{
    MANANA = 'MANANA',
    TARDE = 'TARDE',
    NOCHE = 'NOCHE',
    FIN_SEMANA = 'FIN_SEMANA'
}
export const SchoolTime = [ SchoolTimeEnum.TARDE, SchoolTimeEnum.MANANA, SchoolTimeEnum.NOCHE, SchoolTimeEnum.FIN_SEMANA ]

enum AcademicModalityEnum{
    PREGRADO = 'PREGRADO',
    POSGRADO = 'POSGRADO',
    DIPLOMADO = 'DIPLOMADO'
}
export const AcademicModality = [AcademicModalityEnum.DIPLOMADO, AcademicModalityEnum.POSGRADO, AcademicModalityEnum.PREGRADO]

enum AcademicPeriodEnum{
    SEMESTRAL = 'SEMESTRAL',
    ANUAL = 'ANUAL'
}
export const AcademicPeriod = [AcademicPeriodEnum.ANUAL, AcademicPeriodEnum.SEMESTRAL]

enum ProgramTimeEnum{
    DIURNO = 'DIURNO',
    NOCTURNO = 'NOCTURNO'
}
export const ProgramTime = [ProgramTimeEnum.DIURNO, ProgramTimeEnum.NOCTURNO]

enum DegreeWorkTypeEnum{
    TESIS = 'TESIS',
    PASANTIA = 'PASANTIA',
    APLICATIVO = 'APLICATIVO'
}
export const DegreeWorkType = [DegreeWorkTypeEnum.APLICATIVO, DegreeWorkTypeEnum.TESIS, DegreeWorkTypeEnum.PASANTIA]
