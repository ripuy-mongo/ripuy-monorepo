import { Schema, model } from 'mongoose'

const ingresoSchema = new Schema( {
    descripcion:{ type: String, required: true }
}, { collection: 'motivoIngreso' } )

const financiacionSchema = new Schema( {
    descripcion:{ type: String, required: true }
}, { collection: 'financiacion' } )

const tipoBecaSchema = new Schema( {
    descripcion:{ type: String, required: true }
}, { collection: 'tipoBecas' } )

const SimpleModels = {
    MotivoIngreso: model( 'MotivoIngreso', ingresoSchema ),
    Financiacion: model( 'Financiacion', financiacionSchema ),
    TipoBeca: model( 'TipoBeca', tipoBecaSchema )
}

export default SimpleModels
