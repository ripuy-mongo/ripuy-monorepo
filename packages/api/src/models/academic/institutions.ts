import { Schema, model } from 'mongoose'
import { IesCampusType, IesSectorType, IesType, SchoolCalendarType, SchoolType, SchoolTime, } from './utils'

const iesSchema = new Schema( {
    codigo:{ type: String, unique: true, required: true },
    nombre:{ type: String, required: true },
    seccional:{ type: Number, enum: IesCampusType, required: true },
    naturaleza_juridica:{ type: Number, required: true }, // TODO: Set enum values, research
    sector:{ type: Number, enum: IesSectorType, required: true },
    nivel_academico:{ type: Number, enum: IesType, required: true },
    municipio_id:{ type: Schema.Types.ObjectId, ref: 'Municipio', required: true },
}, { collection: 'ies' } )

const colegioSchema = new Schema( {
    codigo:{ type: String, unique: true, required: true },
    nombre:{ type: String, required: true },
    municipio_id:{ type: Schema.Types.ObjectId, ref: 'Municipio', required: true },
    calendario:{ type: String, enum: SchoolCalendarType },
    naturaleza:{ type: String, enum: SchoolType },
    jornada:{ type: String, enum: SchoolTime },
}, { collection: 'colegios' }  )

const InstitutionModels = {
    Ies: model( 'Ies', iesSchema ),
    Colegio: model( 'Colegio', colegioSchema )
}

export default InstitutionModels
