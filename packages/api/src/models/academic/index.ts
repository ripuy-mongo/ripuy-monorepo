import SimpleModels from './simpleModels'
import InstitutionModels from './institutions'
import ProgramModels from './academicProgram'
import Academico from './academic'

const AcademicModels = {
    ...InstitutionModels,
    ...SimpleModels,
    ...ProgramModels,
    Academico,
}

export default AcademicModels
