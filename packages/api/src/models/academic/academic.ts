import { Schema, model } from 'mongoose'
import { AcademicModality, AcademicPeriod, DegreeWorkType, IesCampusType, ProgramTime } from './utils'

const academicoSchema = new Schema( {
    user_id:{ type: Schema.Types.ObjectId, ref: 'User', required: true, unique: true },
    fecha_registro:{ type: Date, required: false, default: Date.now() },
    sede:{ type: String, enum: IesCampusType, required: false },
    programa_id:{ type: Schema.Types.ObjectId, ref: 'ProgAcademico', required: true },
    modalidad_academica:{ type: String, enum: AcademicModality, required: false },
    motivo_ingreso_id:{ type: Schema.Types.ObjectId, ref: 'MotivoIngreso', required: false },
    es_primera_opcion:{ type: Boolean, required: false },
    intentos_admision:{ type: Number, required: false },
    fecha_inicio_programa:{ type: Date, required: true },
    fecha_finalizacion_materias:{ type: Date, required: false },
    fecha_grado:{ type: Date, required: false },
    duracion_programa:{ type: Number, required: false },
    periodo:{ type: String, enum: AcademicPeriod, required: false },
    jornada:{ type: String, enum: ProgramTime, required: false },
    promedio_notas:{ type: Number, required: false },
    mension:{ type: Boolean, required: false },
    trabajo_grado:{ type: String, enum: DegreeWorkType, required: false },
    titulo_trabajo_grado:{ type: String, required: false },
    linea_investigacion:{ type: String, required: false },
    financiacion_id:{ type: Schema.Types.ObjectId, ref: 'Financiacion', required: false },
    tipo_beca_id:{ type: Schema.Types.ObjectId, re: 'TipoBeca', required: false },
    colegio_id:{ type: Schema.Types.ObjectId, ref: 'Colegio', required: false }
}, { collection: 'academicos', timestamps:{ createdAt: 'created_at', updatedAt: 'updated_at' } } )

const Academico = model( 'Academico', academicoSchema )
export default Academico
