import { Schema, model } from 'mongoose'

// TODO: is necessary this schema, replace with enum
const nivelAcademicoSchema = new Schema( {
    nombre:{ type: String, required: true }
},  { collection: 'nivelAcademico' } )

const nivelFormacionSchema = new Schema( {
    nombre:{ type: String, required: true },
    nivel_academico:{ type: Schema.Types.ObjectId, ref: 'NivelAcademico', required: false }
}, { collection: 'nivelFormacion' } )

const progAcademicoSchema = new Schema( {
    codigo:{ type: String, unique: true, required: false },
    nombre:{ type: String, unique: true, required: true },
    nivel_formacion:{ type: Schema.Types.ObjectId, ref: 'NivelFormacion', required: false }
}, { collection: 'progAcademicos' } )

const ProgramModels = {
    NivelAcademico: model( 'NicelAcademico', nivelAcademicoSchema ),
    NivelFormacion: model( 'NivelFormacion', nivelFormacionSchema ),
    ProgAcademicos: model( 'ProgAcademico', progAcademicoSchema )
}
export default ProgramModels
