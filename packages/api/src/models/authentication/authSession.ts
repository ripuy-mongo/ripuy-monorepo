import { Schema, model } from 'mongoose'

const authSessionSchema = new Schema( {
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    token: { type: String, required: true },
    expiresIn: { type: Date, required: true },
}, { timestamps: true, collection: 'AuthSession' } )

const AuthSession = model( 'AuthSession', authSessionSchema )

export default AuthSession
