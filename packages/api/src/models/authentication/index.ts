import AuthCodeGrant from './authCodeGrant'
import AuthSession from './authSession'
import RestorePassword from './restorePassword'

const AuthModels = {
    AuthCodeGrant,
    AuthSession,
    RestorePassword
}

export default AuthModels
