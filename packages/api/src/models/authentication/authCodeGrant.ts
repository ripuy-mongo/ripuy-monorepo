import { Schema, model } from 'mongoose'

const authCodeGrant = new Schema( {
    code:{ type: String, required: true, unique: true },
    session:{ type: Schema.Types.ObjectId, ref: 'AuthSession', required: true },
    expiresIn:{ type: Date, required: true }
}, { timestamps: true, collection: 'AuthCodeGrant' } )

const AuthSession = model( 'AuthCodeGrant', authCodeGrant )

export default AuthSession
