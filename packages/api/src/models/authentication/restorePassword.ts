import { Schema, model } from 'mongoose'

const restorePasswordSchema = new Schema( {
    token:{ type: String, required: true, unique: true },
    user:{ type: Schema.Types.ObjectId, required: true },
}, { collection: 'restorePassword', timestamps: true } )

const RestorePassword = model( 'RestorePassword', restorePasswordSchema )

export default RestorePassword
