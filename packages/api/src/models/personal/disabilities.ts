import { Schema, model } from 'mongoose'

const discapacidadSchema = new Schema( {
    nombre:{ type: String, required: true },
    grupo_id:{ type: Schema.Types.ObjectId, ref: 'GrupoDiscpacidad',required: true },
}, { collection: 'discapacidades' } )

const grupoDiscapacidadSchema = new Schema( {
    nombre:{ type: String, required: true }
}, { collection: 'gruposDiscap' } )

const DiscapacidadModels = {
    Discapacidad: model( 'Discapacidad', discapacidadSchema ),
    GrupoDiscapacidad: model( 'GrupoDiscpacidad', grupoDiscapacidadSchema ),
}

export default DiscapacidadModels
