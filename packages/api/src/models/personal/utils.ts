enum IdentityTypeEnum {
    CC = 'CC', CE = 'CE', TI = 'TI'
}
export const IdentityType = [IdentityTypeEnum.CC, IdentityTypeEnum.CE, IdentityTypeEnum.TI]

export enum GenderEnum {
    MASCULINO = 'MASCULINO',
    FEMENINO = 'FEMENINO',
    OTRO = 'OTRO'
}
export const Gender = [GenderEnum.MASCULINO, GenderEnum.FEMENINO, GenderEnum.OTRO]
