import { Schema, model } from 'mongoose'

const clasEtnografica = new Schema( {
    nombre:{ type: String, required: true }
}, { collection: 'clasEtnografica' } )

const ClasEtnografica = model( 'ClasEtnografica', clasEtnografica )

export default ClasEtnografica
