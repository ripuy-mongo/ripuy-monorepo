import { Schema, model } from 'mongoose'
import { Gender, IdentityType } from './utils'

const userSchema = new Schema( {
    identificacion:{ type: String, unique: true, required: true },
    tipo_identificacion:{ type: String, enum: IdentityType, required: true },
    pais_expedicion_id:{ type: Schema.Types.ObjectId, ref: 'Pais', required: false },
    nombres:{ type: String, required: true },
    apellidos:{ type: String, required: true },
    genero:{ type: String, enum: Gender, required: false },
    fecha_nacimiento:{ type: Date, required: false },
    correo:{ type: String, required: true },
    celular:{ type: String, required: true },
    password:{ type: String, required: true },
    role:{ type: String, enum: ['ADMIN', 'GRADUATE'], default: 'GRADUATE', required: true },
    permiso:{ type: Boolean, required: false, default: false },
    fecha_permiso:{ type: Date, required: false },
    primer_ingreso:{ type: Boolean, required: false },
    personal_id:{ type: Schema.Types.ObjectId, ref: 'Personal' },
    academico_id:{ type: Schema.Types.ObjectId, ref: 'Academico' },
    laboral_id:{ type: Schema.Types.ObjectId, ref: 'Laboral' }
}, { collection: `User` } )

userSchema.statics.findByLogin = async function( identification ) {
    return await this.findOne( {
        identificacion: identification,
    } )
}

const User = model( 'User', userSchema )

export default User
