import { Schema, model } from 'mongoose'

const personalSchema = new Schema( {
    user_id:{ type: Schema.Types.ObjectId, ref: 'User', unique: true, required: true },
    municipio_nacimiento_id:{ type: Schema.Types.ObjectId, ref: 'Municipio', required: false },
    municipio_recidencia_id:{ type: Schema.Types.ObjectId, ref: 'Municipio', required: false },
    clase_etnografica_id:{ type: Schema.Types.ObjectId, ref: 'ClasEtnografica', required: false },
    tiene_discapacidad:{ type: Boolean, required: false, default: false },
    discapacidad_id:{ type: Schema.Types.ObjectId, ref: 'Discapacidad', required: false },
    motivo_discapacidad:{ type: String, required: false },
    // fec_fin_sec:{ type: Date, required: false },
    // cod_colegio:{ type: Schema.Types.ObjectId, ref: 'Colegio', required: false },
}, { collection: `personales`, timestamps:{ createdAt: 'fecha_registro', updatedAt: 'fecha_actualizacion' }  } )

const Personal = model( 'Personal', personalSchema )

export default Personal
