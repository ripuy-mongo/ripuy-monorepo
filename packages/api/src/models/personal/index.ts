import User from './user'
import Personal from './personal'
import ClasEtnografica from './ethnographicClass'
import DiscapacidadModels from './disabilities'

const PersonalModels = { User, Personal, ClasEtnografica, ...DiscapacidadModels }

export default PersonalModels
