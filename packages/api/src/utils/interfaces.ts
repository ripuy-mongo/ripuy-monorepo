export interface Context{
    models;
    session: Session
}
export interface Session {
    userId: string; // ID pf user
    user: any;
    role?: string;
    token?: string;
}

export enum Role{
    GRADUATE = 'GRADUATE',
    ADMIN = 'ADMIN'
}

export interface PayloadToken {
    id: string;
    role?: Role;
    exp?: number;
    iat?: number
}

export interface MailOptions {
    from: string;
    to: string;
    subject: string;
    html: string;
}

export interface ArgsCursorPagination {
    take?: number;
    cursor?: { id: string };
}