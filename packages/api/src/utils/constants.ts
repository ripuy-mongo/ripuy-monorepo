import { config } from 'dotenv'
import { join } from 'path'

if( process.env.NODE_ENV === 'production' ){
    config( { path: join( __dirname, `../../`, `.env.prod` ) } )
} else if ( process.env.NODE_ENV === 'test' ) {
    config( { path: join( __dirname, `../../`, `.env.test` ) } )
} else {
    config( { path: join( __dirname, `../../`, `.env.dev` ) } )
}

const RIPUY_FRONT = process.env.RIPUY_FRONT
const RIPUY_COOKIE = process.env.RIPUY_COOKIE
const TOKEN_TIME: string = `1d`
const COOKIE_TIME: string = '20d'
const COOKIE_MAX_AGE: number = 1000 * 60 * 60 * 24 * 28 // 28 Days
const TOKEN_EMAIL_TIME: string = '10m'
const MONGO_URL = process.env.NODE_ENV === 'test' ?
    `mongodb://localhost:${process.env.MONGO_PORT}/${process.env.MONGO_DB}` :
    `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PWD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`
const EMAIL = process.env.EMAIL
const EMAIL_PWD = process.env.EMAIL_PWD
const AWS_ACCESS_KEY = process.env.AWS_ACCESS_KEY
const AWS_SECRET_ACCESS_KEY = process.env.AWS_SECRET_ACCESS_KEY

export {
    TOKEN_TIME,
    COOKIE_TIME,
    RIPUY_COOKIE,
    MONGO_URL,
    COOKIE_MAX_AGE,
    RIPUY_FRONT,
    EMAIL,
    EMAIL_PWD,
    TOKEN_EMAIL_TIME,
    AWS_ACCESS_KEY,
    AWS_SECRET_ACCESS_KEY
}
