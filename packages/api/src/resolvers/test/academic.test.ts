import { queries, mutations } from './fixtures/academic'
import {Role, signJwt} from "../../utils";
import {closeConnection, mongoConnection} from "../../models";
import {apolloTestClient} from "./server/apolloServerTest";

const user = { id: '60590812d8e8e5098e3f6ba7', role: Role.GRADUATE }

const {
    colegios,
    motivosIngreso,
    formasFinanciacion,
    modalidades,
    jornadas,
    periodos,
    sedes,
    informacionAcademica
} = queries

const { updateInformacionAcademica } = mutations

beforeAll( async () => {
    await mongoConnection()
})

afterAll( async () => {
    await closeConnection()
} )

describe( '>> Peticiones información académica', () => {
    let token = signJwt( { ...user }, '1h' )
    let { query, mutate  } = apolloTestClient( { authorization: token } )

    test( 'Debería retornar la lista total de colegios ', async ( done ) => {
        const response: any = await query( colegios.query )
        expect( response.data.colegios.length ).toBeGreaterThan( 2700 )
        expect( response.data ).toEqual( colegios.response.success )
        done();
    } )

    test( 'Debería retornar una paginación de colegios usando cursor id', async ( done ) => {
        const response: any = await query( colegios.query, { variables:{ ...colegios.variables } } )
        expect( response.data.colegios ).toHaveLength( 10 )
        expect( response.data ).toEqual( colegios.response.successPagination )
        done();
    } )

    test( 'Debería retornar una lista de motivos de ingreso al programa', async ( done ) => {
        const response: any = await query( motivosIngreso.query )
        expect( response.data ).toEqual( motivosIngreso.response.success )
        done()
    } )

    test( 'Debería retornar los formas de financiamientos de los estudios', async ( done ) => {
        const response: any = await query( formasFinanciacion.query )
        expect( response.data ).toEqual( formasFinanciacion.response.success )
        done()
    } )

    test( 'Debería retornar las modalidades académicas de los programas', async ( done) => {
        const response: any = await query( modalidades.query )
        expect( response.data ).toEqual( modalidades.response.success )
        done()
    } )

    test( 'Debería retornar las jornadas académicas de los programa', async ( done ) => {
        const response: any = await query( jornadas.query )
        expect( response.data ).toEqual( jornadas.response.success )
        done()
    } )

    test( 'Debería retornar los periodos académicos semestrales', async ( done) => {
        const response: any = await query( periodos.query )
        expect( response.data ).toEqual( periodos.response.success )
        done()
    } )

    test( 'Debería retornar las sedes de la universidad', async ( done ) => {
        const response: any = await query( sedes.query )
        expect( response.data ).toEqual( sedes.response.success )
        done()
    } )

    test( 'Debería retornar error por usuario en session no autorizado para consultar información académica', async ( done ) => {
        token = signJwt( { ...informacionAcademica.context.other }, '1h' )
        query = apolloTestClient( { authorization: token } ).query
        const response: any = await query( informacionAcademica.query, { variables:{ ...informacionAcademica.variables } } )
        expect( response.errors ).toEqual( informacionAcademica.response.errorNotAuthorize )
        done()
    } )

    test( 'Debería retornar información académica de usuario, consultando con usuario admin', async ( done ) => {
        token = signJwt( { ...informacionAcademica.context.admin }, '1h' )
        query = apolloTestClient( { authorization: token } ).query
        const response: any = await query( informacionAcademica.query, { variables:{ ...informacionAcademica.variables } } )
        expect( response.data ).toEqual( informacionAcademica.response.success )
        done()
    } )

    test( 'Debería retornar información académica de usuario, consultando com usuario propietario de la información', async ( done ) => {
        token = signJwt( { ...informacionAcademica.context.owner }, '1h' )
        query = apolloTestClient( { authorization: token } ).query
        const response: any = await query( informacionAcademica.query, { variables:{ ...informacionAcademica.variables } } )
        expect( response.data ).toEqual( informacionAcademica.response.success )
        done()
    } )

    // Mutations

    test( 'Debería retornar error al actualizar información académica sin autorización otorgada por el usuario', async ( done ) => {
        token = signJwt( { ...updateInformacionAcademica.context.admin }, '10m' )
        mutate = apolloTestClient( { authorization: token } ).mutate
        const response: any = await mutate( updateInformacionAcademica.query, {
            variables: {
                where: updateInformacionAcademica.variables.where.wrong,
                data: updateInformacionAcademica.variables.data
            }
        } )
        expect( response.errors ).toEqual( updateInformacionAcademica.response.errorNotAuthorize )
        done()
    } )

    test( 'Debería actualizar la información académica del usuario', async ( done ) => {
        const response: any = await mutate( updateInformacionAcademica.query, {
            variables: {
                where: updateInformacionAcademica.variables.where.right,
                data: updateInformacionAcademica.variables.data
            }
        } )
        expect( response.data ).toEqual( updateInformacionAcademica.response.success )
        done()
    } )

} )