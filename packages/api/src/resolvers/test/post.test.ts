import { queries, mutations } from './fixtures/post'
import {Role, signJwt} from "../../utils";
import {closeConnection, mongoConnection} from "../../models";
import {apolloTestClient} from "./server/apolloServerTest";

const user = { id: '60590812d8e8e5098e3f6ba7', role: Role.GRADUATE }
const { eventos, evento, ofertas, oferta, categorias, interesados, egresados } = queries
const { createOferta, updateOferta, deleteOferta, createInteresado, deleteInteresado } = mutations

beforeAll( async () => { await mongoConnection() })

afterAll( async () => { await closeConnection() } )

describe( '>> Peticiones publicación de ofertas y eventos', () => {
    let token = signJwt( { ...user }, '1h' )
    let { query: queryReq, mutate: mutateReq } = apolloTestClient( { authorization: token } )

    test( 'Debería retornar la lista de todos los eventos', async ( done ) => {
        const response: any = await queryReq( eventos.query )
        expect( response.data?.eventos.length ).toBeGreaterThan( 2 )
        expect( response.data ).toEqual( eventos.response.success )
        done();
    } )

    test( 'Debería retornar un evento consultado por id único', async ( done ) => {
        const response: any = await queryReq( evento.query, { variables: { ...evento.variables.right } } )
        expect( response.data ).toEqual( evento.response.success )
        done()
    } )

    test( 'Debería retornar objeto null por evento con id no existente', async ( done ) => {
        const response: any = await queryReq( evento.query, { variables: { ...evento.variables.wrong } } )
        expect( response.data.evento ).toBeNull()
        done()
    } )

    test( 'Debería retornar la lista total de ofertas laborales', async ( done ) => {
        const response: any = await queryReq( ofertas.query )
        expect( response.data?.ofertas.length ).toBe( 50 )
        expect( response.data ).toEqual( ofertas.response.success )
        done()
    } )

    test( 'Debería retornar una paginación de ofertas laboral a partir de un cursor id', async ( done ) => {
        const response: any = await queryReq( ofertas.query, { variables:{ take: ofertas.variables.take, cursor: ofertas.variables.cursor } } )
        expect( response.data.ofertas ).toHaveLength( 10 )
        expect( response.data ).toEqual( ofertas.response.successPagination )
        done()
    } )

    test( 'Debería retornar una lista de ofertas laboral buscadas por texto', async ( done ) => {
        const response: any = await queryReq( ofertas.query, { variables:{ where: ofertas.variables.where } } )
        expect( response.data.ofertas ).toHaveLength( 2 )
        expect( response.data ).toEqual( ofertas.response.successSearch )
        done()
    } )

    test( 'Debería retornar una oferta buscada por id único', async ( done ) => {
        const response: any = await queryReq( oferta.query, { variables:{ ...oferta.variables.right } } )
        expect( response.data ).toEqual( oferta.response.success )
        done()
    } )

    test( 'Debería retornar objeto null por oferta no existente', async ( done ) => {
        const response: any = await queryReq( oferta.query, { variables:{ ...oferta.variables.wrong } } )
        expect( response.data.oferta ).toBeNull()
        done()
    } )

    test( 'Debería retornar la lista total de interesados a una oferta laboral', async ( done ) => {
        const response: any = await queryReq( interesados.query, { variables: { oferta: interesados.variables.oferta } } )
        expect( response.data ).toEqual( interesados.response.success )
        done()
    } )

    test( 'Debería retornar una lista paginada de interesados a una oferta laboral', async ( done ) => {
        const response: any = await queryReq( interesados.query, { variables:{ ...interesados.variables } } )
        expect( response.data ).toEqual( interesados.response.successPagination )
        done()
    } )

    test( 'Debería retornar la lista de categorías de las ofertas laborales', async ( done ) => {
        const request: any = await queryReq( categorias.query )
        expect( request.data.categorias.length ).toBeGreaterThan( 3 );
        expect( request.data ).toEqual( categorias.response.success )
        done()
    } )

    test( 'Debería retornar la lista total de egresados', async ( done ) => {
        const response: any = await queryReq( egresados.query )
        expect( response.data.egresados.length ).toBeGreaterThan( 59000 )
        expect( response.data ).toEqual( egresados.response.success )
        done()
    } )

    test( 'Debería retornar una lista paginada de egresados usando cursor id', async ( done ) => {
        const response: any = await queryReq( egresados.query, {
            variables:{ take: egresados.variables.take, cursor: egresados.variables.cursor}
        } )
        expect( response.data.egresados ).toHaveLength( 10 )
        expect( response.data ).toEqual( egresados.response.successPagination )
        done()
    } )

    test( 'Debería retornar una lista de egresados buscados por nombre', async ( done ) => {
        const response: any = await queryReq( egresados.query, { variables:{ where: egresados.variables.where } } )
        expect( response.data ).toEqual( egresados.response.successSearch )
        done()
    } )

    // Mutations

    test( 'Debería retornar error al crear una oferta como usuario no administrador', async( done ) => {
        const { query, response, variables } = createOferta
        const res: any = await mutateReq( query, { variables } )
        expect( res.errors ).toEqual( response.errorNotAuthorize )
        done()
    } )

    test( 'Debería crear oferta laboral como administrador ', async ( done ) => {
        const { context, query, variables, response } = createOferta
        mutateReq = apolloTestClient( {
            authorization: signJwt( { ...context.admin }, '3m' )
        } ).mutate
        const res: any = await mutateReq( query, { variables } )
        expect( res.data ).toEqual( response.success )
        done()
    } )

    test( 'Debería retornar error al actualizar una oferta laboral no existente', async ( done ) => {
        const { query, response, variables } = updateOferta
        const res: any = await mutateReq( query, {
            variables: { where: variables.where.wrong, data: variables.data }
        } )
        expect( res.errors ).toEqual( response.errorNotFound )
        done()
    } )

    test( 'Debería actualizar una oferta laboral', async ( done ) => {
        const { query, response, variables } = updateOferta
        const res: any = await mutateReq( query, {
            variables: { where: variables.where.right, data: variables.data }
        } )
        expect( res.data ).toEqual( response.success )
        done()
    } )

    test( 'Debería retornar error al eliminar una oferta no existente', async ( done ) => {
        const { query, variables, response } = deleteOferta
        const res: any = await mutateReq( query, { variables: { where: variables.where.wrong  } } )
        expect( res.errors ).toEqual( response.errorNotFound )
        done()
    } )

    test( 'Debería eliminar una oferta laboral', async ( done ) => {
        const { query, variables, response } = deleteOferta
        const res: any = await mutateReq( query, { variables: { where: variables.where.right  } } )
        expect( res.data ).toEqual( response.success )
        done()
    } )

    test( 'Debería retornar error al crear una aplicación de interés a una oferta laboral no existente', async ( done ) => {
        const { query, response, variables, context } = createInteresado
        mutateReq = apolloTestClient( {
            authorization: signJwt( { ...context.user }, '3m' )
        } ).mutate
        const res: any = await mutateReq( query, { variables: { data: variables.data.wrong }  } )
        expect( res.errors ).toEqual( response.errorNotFound )
        done()
    } )

    test( 'Debería crear una aplicación como interesado a una oferta laboral', async ( done ) => {
        const { query, response, variables } = createInteresado
        const res: any = await mutateReq( query, { variables: { data: variables.data.right }  } )
        expect( res.data ).toEqual( response.success )
        // TODO: validate offer and user objects
        done()
    } )

    test( 'Debería retornar error al eliminar una aplicación como interesado a la cual no se aplico', async ( done ) => {
        const { query, response, variables } = deleteInteresado
        const res: any = await mutateReq( query, { variables: { where: variables.where.right } } )
        expect( res.errors ).toEqual( response.error )
        done()
    } )

    test( 'Debería retornar error al eliminar una aplicación como interesado a una oferta laboral no existente', async ( done ) => {
        const { query, response, variables, context } = deleteInteresado
        mutateReq = apolloTestClient( {
            authorization: signJwt( { ...context.user }, '3m' )
        } ).mutate
        const res: any = await mutateReq( query, { variables: { where: variables.where.wrong } } )
        expect( res.errors ).toEqual( response.error )
        done()
    } )

    test( 'Debería eliminar la aplicación com interesado a una oferta laboral', async ( done ) => {
        const { query, response, variables } = deleteInteresado
        const res: any = await mutateReq( query, { variables: { where: variables.where.right } } )
        expect( res.data ).toEqual( response.success )
        done()
    } )
} )