import { queries, mutations } from './fixtures/professional'
import {Role, signJwt} from "../../utils";
import {closeConnection, mongoConnection} from "../../models";
import {apolloTestClient} from "./server/apolloServerTest";

const user = { id: '60590812d8e8e5098e3f6ba7', role: Role.GRADUATE }
const {
    actividadesActuales,
    canalesComunicacion,
    razonesDificultadTrabajo,
    empresas,
    empresa,
    informacionLaboral,
    informacionNoLaboral
} = queries
const { updateInformacionLaboral, updateInformacionNoLaboral } = mutations

beforeAll( async () => {
    await mongoConnection()
})

afterAll( async () => {
    await closeConnection()
} )

describe( '>> Peticiones información laboral', () => {
    let token = ''
    let { query, mutate } = apolloTestClient( { authorization: token } )

    test( 'Debería retornar error al intentar consultar sin sesión activa', async ( done ) => {
        const response = await query( actividadesActuales.query )
        expect( response.errors ).toBeUndefined()
        done();
    } )

    test( 'Debería retornar el listado de actividades actuales', async ( done ) => {
        token = signJwt( { ...user }, '1h' )
        query = apolloTestClient( { authorization: token } ).query
        const response: any = await query( actividadesActuales.query )
        expect( response.data?.actividadesActuales.length ).toBeGreaterThan( 0 )
        expect( response.data ).toEqual( actividadesActuales.response.success )
        done()
    } )

    test( 'Debería retornar el listado de canales de comunicación', async ( done ) => {
        const response: any = await query( canalesComunicacion.query )
        expect( response.data?.canalesComunicacion.length ).toBeGreaterThan( 0 )
        expect( response.data ).toEqual( canalesComunicacion.response.success )
        done()
    } )

    test( 'Debería retornar el listado de razones de dificultad', async ( done ) => {
        const response: any = await query( razonesDificultadTrabajo.query )
        expect( response.data?.razonesDificultadTrabajo.length ).toBeGreaterThan( 0 )
        expect( response.data ).toEqual( razonesDificultadTrabajo.response.success )
        done()
    } )

    test( 'Debería retornar el listado completo de empresas registradas', async ( done ) => {
        const response: any = await query( empresas.query  )
        expect( response.data?.empresas.length ).toBeGreaterThan( 80 )
        expect( response.data ).toEqual( empresas.response.successOne )
        done()
    } )

    test( 'Debería retornar paginación de las primeras 5 empresas registradas', async ( done ) => {
        const response: any = await query( empresas.query, { variables: { take: empresas.variables.take } }  )
        expect( response.data?.empresas ).toHaveLength( 5 )
        expect( response.data ).toEqual( empresas.response.successOne )
        done()
    } )

    test( 'Debería retornar paginación de empresas a partir de un id', async ( done ) => {
        const response: any = await query( empresas.query, { variables: { ...empresas.variables } }  )
        expect( response.data?.empresas ).toHaveLength( 5 )
        expect( response.data ).toEqual( empresas.response.successTwo )
        done()
    } )

    test( 'Debería retornar null al enviar a consular un empresa no existente', async ( done ) => {
        const response: any = await query( empresa.query, { variables: { ...empresa.variables.wrong } } )
        expect( response.data?.empresa ).toBeNull()
        done()
    } )

    test( 'Debería retornar la empresa buscada por id', async ( done ) => {
        const response: any = await query( empresa.query, { variables: { ...empresa.variables.right } } )
        expect( response.data ).toEqual( empresa.response.success )
        done()
    } )

    test( 'Debería retornar error al consultar con un usuario no autorizado', async ( done ) => {
        token = signJwt( { ...informacionLaboral.context.other }, '1h' )
        query = apolloTestClient( { authorization: token } ).query
        const response: any = await query( informacionLaboral.query, { variables: informacionLaboral.variables } )
        expect( response.errors ).toEqual( informacionLaboral.response.errorNotAuthorize )
        done()
    } )

    test( 'Debería retornar la información laboral, como usuario admin', async ( done ) => {
        token = signJwt( { ...informacionLaboral.context.admin }, '1h' )
        query = apolloTestClient( { authorization: token } ).query
        const response: any = await query( informacionLaboral.query, { variables: informacionLaboral.variables } )
        expect( response.data ).toEqual( informacionLaboral.response.success )
        done()
    } )

    test( 'Debería retornar la información laboral, como usuario de la información', async ( done ) => {
        token = signJwt( { ...informacionLaboral.context.owner }, '1h' )
        query = apolloTestClient( { authorization: token } ).query
        const response: any = await query( informacionLaboral.query, { variables: informacionLaboral.variables } )
        expect( response.data ).toEqual( informacionLaboral.response.success )
        done()
    } )

    test( 'Debería retornar error al consultar con un usuario no autorizado', async ( done ) => {
        token = signJwt( { ...informacionNoLaboral.context.other }, '1h' )
        query = apolloTestClient( { authorization: token } ).query
        const response: any = await query( informacionNoLaboral.query, { variables: informacionNoLaboral.variables } )
        expect( response.errors ).toEqual( informacionNoLaboral.response.errorNotAuthorize )
        done()
    } )

    test( 'Debería retornar la información no laboral, como usuario admin', async ( done ) => {
        token = signJwt( { ...informacionNoLaboral.context.admin }, '1h' )
        query = apolloTestClient( { authorization: token } ).query
        const response: any = await query( informacionNoLaboral.query, { variables: informacionNoLaboral.variables } )
        expect( response.data ).toEqual( informacionNoLaboral.response.success )
        done()
    } )

    test( 'Debería retornar la información no laboral, como usuario de la información', async ( done ) => {
        token = signJwt( { ...informacionNoLaboral.context.owner }, '1h' )
        query = apolloTestClient( { authorization: token } ).query
        const response: any = await query( informacionNoLaboral.query, { variables: informacionNoLaboral.variables } )
        expect( response.data ).toEqual( informacionNoLaboral.response.success )
        done()
    } )

    test( 'Debería retornar error al actualizar información laboral como usuario no autorizado', async ( done ) => {
        const { context, query, variables, response } = updateInformacionLaboral
        mutate = apolloTestClient( {
            authorization: signJwt( { ...context.other }, '3m' )
        } ).mutate
        const res: any = await mutate( query, { variables } )
        expect( res.errors ).toEqual( response.errorNotAuthorize )
        done()
    } )

    test( 'Debería actualizar la información laboral del usuario en sesión', async ( done ) => {
        const { context, query, variables, response } = updateInformacionLaboral
        const { query: queryReq, mutate: mutateReq } = apolloTestClient( {
            authorization: signJwt( { ...context.owner }, '3m' )
        } )
        let res: any = await mutateReq( query, { variables } )
        expect( res.data ).toEqual( response.success )
        res = await queryReq( informacionLaboral.query, { variables: { ...variables.where.user } } )
        expect( res.data ).toEqual( response.successData )
        done()
    } )

    test( 'Debería retornar error al actualizar informaciín no laboral con usuario no autorizado', async ( done ) => {
        const { context, query, variables, response } = updateInformacionNoLaboral
        mutate = apolloTestClient( {
            authorization: signJwt( { ...context.other }, '3m' )
        } ).mutate
        const res: any = await mutate( query, { variables } )
        expect( res.errors ).toEqual( response.errorNotAuthorize )
        done()
    } )

    test( 'Debería actualizar la información no laboral del usuario en sesión', async ( done ) => {
        const { context, query, variables, response } = updateInformacionNoLaboral
        const { query: queryReq, mutate: mutateReq } = apolloTestClient( {
            authorization: signJwt( { ...context.owner }, '3m' )
        } )
        let res: any = await mutateReq( query, { variables } )
        expect( res.data ).toEqual( response.success )
        res = await queryReq( informacionNoLaboral.query, { variables: { ...variables.where.user } } )
        expect( res.data ).toEqual( response.successData )
        done()
    } )
} )