import { apolloTestClient } from './server/apolloServerTest'
import { queries, mutations } from './fixtures/personal'
import {Role, signJwt} from '../../utils';
import {closeConnection, mongoConnection} from '../../models';

const { clasesEtnograficas, discapacidades, egresado, informacionPersonal } = queries
const { updateInformacionAcademica, changeFirstLoginStatus, setPermisos } = mutations
const user = { id: '60590812d8e8e5098e3f6ba7', role: Role.GRADUATE }

beforeAll( async () => {
    await mongoConnection()
})

afterAll( async () => {
    await closeConnection()
} )

describe( '>> Peticiones información personal', () => {
    let token = ''
    let { query: queryReq, mutate: mutateReq } = apolloTestClient( { authorization: token } )

    test( 'Debería retornar error al intentar consultar sin sesión activa', async ( done ) => {
        const response = await queryReq( clasesEtnograficas.query )
        expect( response.errors ).toBeUndefined()
        done();
    } )

    test( 'Debería retornar la lista de clases etnográficas', async ( done ) => {
        token = signJwt( { id: user.id, role: user.role }, '1h' )
        queryReq = apolloTestClient({ authorization: token} ).query
        const response = await queryReq( clasesEtnograficas.query )
        expect( response.data ).toEqual( clasesEtnograficas.response.success )
        done();
    } )

    test( 'Debería retornar la lista de discapacidades' , async ( done ) => {
        const response = await queryReq( discapacidades.query )
        expect( response.data ).toEqual( discapacidades.response.success )
        done()
    } )

    test( 'Debería retornar error por usuario en sesión no autorizado para consultar egresado', async ( done ) => {
        token = signJwt( { ...egresado.context.other  }, '1h' )
        queryReq = apolloTestClient({ authorization: token } ).query
        const response = await queryReq( egresado.query, { variables: { ...egresado.variables.right } } )
        expect( response.errors ).toEqual( egresado.response.errorNotAuthorize )
        done()
    } )

    test( 'Debería retornar error por egresado no existente', async ( done ) => {
        token = signJwt( { ...egresado.context.admin  }, '1h' )
        queryReq = apolloTestClient({ authorization: token } ).query
        const response = await queryReq( egresado.query, { variables: { ...egresado.variables.wrong } } )
        expect( response.errors ).toEqual( egresado.response.errorNotFound )
        done()
    } )

    test( 'Debería retornar egresado consultando como administrador', async ( done ) => {
        const response = await queryReq( egresado.query, { variables: { ...egresado.variables.right } } )
        expect( response.data ).toEqual( egresado.response.success )
        done()
    } )

    test( 'Debería retornar egresado consultando como el mismo usuario en sesión', async ( done ) => {
        token = signJwt( { ...egresado.context.owner  }, '1h' )
        queryReq = apolloTestClient({ authorization: token } ).query
        const response = await queryReq( egresado.query, { variables: { ...egresado.variables.right } } )
        expect( response.data ).toEqual( egresado.response.success )
        done()
    } )

    // Informacion personal

    test( 'Debería retornar error por usuario en sesión no autorizado para consultar información personal', async ( done ) => {
        token = signJwt( { ...informacionPersonal.context.other  }, '1h' )
        queryReq = apolloTestClient({ authorization: token } ).query
        const response = await queryReq( informacionPersonal.query, { variables: { ...informacionPersonal.variables.right } } )
        expect( response.errors ).toEqual( informacionPersonal.response.errorNotAuthorize )
        done()
    } )

    test( 'Debería retornar error por usuario no existente', async ( done ) => {
        token = signJwt( { ...informacionPersonal.context.admin  }, '1h' )
        queryReq = apolloTestClient({ authorization: token } ).query
        const response = await queryReq( informacionPersonal.query, { variables: { ...informacionPersonal.variables.wrong } } )
        expect( response.errors ).toEqual( informacionPersonal.response.errorNotFound )
        done()
    } )

    test( 'Debería retornar información personal consultando como administrador', async ( done ) => {
        const response = await queryReq( informacionPersonal.query, { variables: { ...informacionPersonal.variables.right } } )
        expect( response.data ).toEqual( informacionPersonal.response.success )
        done()
    } )

    test( 'Debería retornar información personal consultando como el mismo usuario en sesión', async ( done ) => {
        token = signJwt( { ...informacionPersonal.context.owner  }, '3m' )
        queryReq = apolloTestClient({ authorization: token } ).query
        const response = await queryReq( informacionPersonal.query, { variables: { ...informacionPersonal.variables.right } } )
        expect( response.data ).toEqual( informacionPersonal.response.success )
        done()
    } )

    test( 'Debería retornar error al actualizar información personal como usuario no propietario de la información', async ( done) => {
        const { query, variables, context, response } = updateInformacionAcademica
        mutateReq = apolloTestClient( {
            authorization: signJwt( { ...context.other }, '3m' )
        } ).mutate
        const res: any = await mutateReq( query, {
            variables: { where: variables.where.right, data: variables.data }
        } )
        expect( res.errors ).toEqual( response.errorNotAuthorize )
        done()
    })

    test( 'Debería retornar error al actualizar información de un usuario que no a otorgado permisos', async ( done) => {
        const { query, variables, context, response } = updateInformacionAcademica
        mutateReq = apolloTestClient( {
            authorization: signJwt( { ...context.admin }, '3m' )
        } ).mutate
        const res: any = await mutateReq( query, {
            variables: { where: variables.where.wrong, data: variables.data }
        } )
        expect( res.errors ).toEqual( response.errorNotAuthorize )
        done()
    })

    test( 'Debería actualizar la información personal, como usuario administrador', async ( done) => {
        const { query, variables, response } = updateInformacionAcademica
        const res: any = await mutateReq( query, {
            variables: { where: variables.where.right, data: variables.data }
        } )
        expect( res.data ).toEqual( response.success )
        done()
    })

    test( 'Debería actualizar la información personal, como usuario propietario', async ( done) => {
        const { query, variables, context, response } = updateInformacionAcademica
        mutateReq = apolloTestClient( {
            authorization: signJwt( { ...context.owner }, '3m' )
        } ).mutate
        const res: any = await queryReq( query, {
            variables: { where: variables.where.right, data: variables.dataOwner }
        } )
        expect( res.data ).toEqual( response.successOwner )
        done()
    })

    test( 'Debería actualizar el estado de primer ingreso', async ( done ) => {
        const { query, response, context } = changeFirstLoginStatus
        mutateReq = apolloTestClient( {
            authorization: signJwt( { ...context.user }, '3m' )
        } ).mutate
        const res = await mutateReq( query )
        expect( res.data ).toEqual( response.success )
        done()
    } )

    test( 'Debería actualizar los permisos a permitido para editar su información', async ( done ) => {
        const { context, query, variables, response } = setPermisos
        mutateReq = apolloTestClient( {
            authorization: signJwt( { ...context.user }, '3m' )
        } ).mutate
        const res: any = await mutateReq( query, { variables:{ data: variables.allow } } )
        expect( res.data ).toEqual( response.successAllow )
        done()
    } )

    test( 'Debería actualizar los permisos a no permitido para editar su información', async ( done ) => {
        const { context, query, variables, response } = setPermisos
        mutateReq = apolloTestClient( {
            authorization: signJwt( { ...context.user }, '3m' )
        } ).mutate
        const res: any = await mutateReq( query, { variables: { data: variables.deny } } )
        expect( res.data ).toEqual( response.successDeny )
        done()
    } )
} )