import { createTestClient } from 'apollo-server-integration-testing'
import server from '../../../graphql'

export interface TestAuthHeaders {
    authorization?: string
    cookie?: string
}

export const apolloTestClient = ( authHeaders: TestAuthHeaders ) => {
    return createTestClient( {
        apolloServer: server as any,
        extendMockRequest:{
            headers:{
                ...authHeaders
            }
        }
    } )
}
