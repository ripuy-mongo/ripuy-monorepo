import { gql } from 'apollo-server-express'
import { Role } from '../../../utils'

const queries = {
    clasesEtnograficas: {
        query: gql`query clases{ clasesEtnograficas{ id nombre } }`,
        response: {
            success: {
                clasesEtnograficas: expect.arrayContaining( [
                    expect.objectContaining( {  nombre: expect.stringContaining( 'Caucásico' ) } ),
                    expect.objectContaining( {  nombre: expect.stringContaining( 'Etiópico o negro' ) } )
                ] )
            }
        }
    },
    discapacidades:{
        query: gql`query discapacidades{ discapacidades{  nombre id } }`,
        response: {
            success: {
                discapacidades: expect.arrayContaining( [
                    expect.objectContaining( {  nombre: expect.stringContaining( 'Ceguera' ) } ),
                    expect.objectContaining( {  nombre: expect.stringContaining( 'Baja vision' ) } ),
                ] )
            }
        }
    },
    egresado: {
        context: {
            admin: { id: '623d327beb256ca76f8a9deb', role: Role.ADMIN },
            owner:{ id: '60590926d8e8e5098e3f6ba9', role: Role.GRADUATE },
            other: { id: '60590b7cd8e8e5098e3f6bad', role: Role.GRADUATE },
        },
        query: gql`query egresado( $id: String! ){
            egresado( where:{ id: $id }){ id nombres }
        }`,
        variables: {
            wrong:{ id: '5f8d9498135a086218361fc8' },
            right:{ id: '60590926d8e8e5098e3f6ba9' }
        },
        response: {
            success: {
                egresado: { id: '60590926d8e8e5098e3f6ba9', nombres: 'Pedro José' }
            },
            errorNotFound: expect.arrayContaining( [ expect.objectContaining( { message: 'User not found' } )] ),
            errorNotAuthorize: expect.arrayContaining( [ expect.objectContaining( { message: 'User not authorized' } )] ),
        }
    },
    informacionPersonal: {
        context: {
            admin: { id: '623d327beb256ca76f8a9deb', role: Role.ADMIN },
            owner:{ id: '60590812d8e8e5098e3f6ba7', role: Role.GRADUATE },
            other: { id: '60590b7cd8e8e5098e3f6bad', role: Role.GRADUATE },
        },
        query: gql`query informacionPersonal( $id: String! ){
            informacionPersonal( where:{ id: $id }){ id }
        }`,
        variables: {
            wrong:{ id: '5f8d9498135a086218361fc8' },
            right:{ id: '60590812d8e8e5098e3f6ba7' }
        },
        response: {
            success: {
                informacionPersonal: { id: '6078dcf05879130034f0d29e' }
            },
            errorNotFound: expect.arrayContaining( [ expect.objectContaining( { message: 'User not found' } )] ),
            errorNotAuthorize: expect.arrayContaining( [ expect.objectContaining( { message: 'User not authorized' } )] ),
        }
    }
}

const mutations = {
    updateInformacionAcademica: {
        context: {
            admin: { id: '623d327beb256ca76f8a9deb', role: Role.ADMIN },
            owner:{ id: '60590812d8e8e5098e3f6ba7', role: Role.GRADUATE },
            other: { id: '60590b7cd8e8e5098e3f6bad', role: Role.GRADUATE },
        },
        query: gql`mutation updatePersonalInformation( $where:UpdateInformacionPersonalWhereInput! $data:UpdateInformacionPersonalDataInput! ){
            updateInformacionPersonal( where: $where data:$data ){
                id nombres genero fecha_nacimiento correo celular
            }
        }`,
        variables: {
            where: {
                right: { user: { id: '60590812d8e8e5098e3f6ba7' } },
                wrong: { user: { id: '60b999b4c6869e0bcde4e6cc' } }
            },
            data: {
                fecha_nacimiento: '1994-12-12', correo: 'test@gmail.com',
                celular: '7777777', genero: 'FEMENINO'
            },
            dataOwner: {
                fecha_nacimiento: '1990-01-25', correo: 'owner@udenar.edu.co',
                celular: '8888888', genero: 'MASCULINO'
            }
        },
        response: {
            success: {
                updateInformacionPersonal: expect.objectContaining( {
                    id: '60590812d8e8e5098e3f6ba7', nombres: 'José Antonio', fecha_nacimiento: '1994-12-12',
                    correo: 'test@gmail.com', celular: '7777777', genero: 'FEMENINO'
                } )
            },
            successOwner: {
                updateInformacionPersonal: expect.objectContaining( {
                    id: '60590812d8e8e5098e3f6ba7', nombres: 'José Antonio', fecha_nacimiento: '1990-01-25',
                    correo: 'owner@udenar.edu.co', celular: '8888888', genero: 'MASCULINO'
                } )
            },
            errorNotAuthorize: expect.arrayContaining( [ expect.objectContaining( { message: 'User not authorized' } )] ),
        }
    },
    changeFirstLoginStatus: {
        context: { user: { id: '60b99aeac6869e0bcde4ed63', role: Role.GRADUATE } },
        query: gql`mutation changeFirstLogin{ changeFirstLoginStatus { status } }`,
        response: {
            success: {
                changeFirstLoginStatus: { status: false }
            }
        }
    },
    setPermisos: {
        context: { user: { id: '60b99aeac6869e0bcde4ed64', role: Role.GRADUATE } },
        query: gql`mutation permisos( $data: SetPermisoData! ){ 
            setPermisos(data: $data){ allowed } 
        }`,
        variables: {
            deny: { allow: false },
            allow: { allow: true }
        },
        response: {
            successAllow: { setPermisos: { allowed: true } },
            successDeny: { setPermisos: { allowed: false } }
        }
    }
}

export { queries, mutations }
