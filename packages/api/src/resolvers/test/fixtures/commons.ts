import { gql } from 'apollo-server-express'

const queries = {
    paises: {
        query: gql`query paises{ paises{ id codigo nombre } }`,
        variables: {},
        response:{
            success: {
                paises: expect.arrayContaining( [ 
                    expect.objectContaining( { nombre: expect.stringContaining( 'México' ) } ),
                    expect.objectContaining( { nombre: expect.stringContaining( 'Ecuador' ) } )
                ] )
            }
        }
    },
    municipios: {
        query: gql`query municipios{ municipios{
            id codigo nombre departamento{id nombre }
        } }`,
        variables: {},
        response:{
            success: {
                municipios: expect.arrayContaining( [
                    expect.objectContaining( { nombre: expect.stringContaining( 'Pasto' ) } ),
                    expect.objectContaining( { nombre: expect.stringContaining( 'La Cruz' ) } )
                ] )
            }
        }
    },
}

export { queries }