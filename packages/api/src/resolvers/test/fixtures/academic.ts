import { gql } from 'apollo-server-express'
import { Role } from '../../../utils'

const queries = {
    sedes: {
        query: gql`query sedes{ sedes }`,
        response: {
            success: { sedes: expect.arrayContaining( [ 'PRINCIPAL', 'EXTENSION' ] ) }
        }
    },
    modalidades: {
        query: gql`query modalidaes{ modalidades }`,
        response: {
            success: { modalidades: expect.arrayContaining( [ 'POSGRADO', 'PREGRADO' ] ) }
        }
    },
    motivosIngreso: {
        query: gql`query ingreso{ motivosIngreso{descripcion} }`,
        response: {
            success: { motivosIngreso: expect.arrayContaining( [
                { descripcion: expect.stringContaining( 'Acceder a mejores oportunidades' ) },
                { descripcion: expect.stringContaining( 'Pasión y entrega por algo ' )  }
            ] ) }
        }
    },
    periodos: {
        query: gql`query periodos{ periodos }`,
        response: {
            success: { periodos: expect.arrayContaining( ['ANUAL', 'SEMESTRAL'] ) }
        }
    },
    jornadas: {
        query: gql`query jornadas{ jornadas }`,
        response: {
            success: { jornadas: expect.arrayContaining( ['DIURNO', 'NOCTURNO'] ) }
        }
    },
    formasFinanciacion: {
        query: gql`query financiacion{ formasFinanciacion{descripcion} }`,
        response: {
            success: { formasFinanciacion: expect.arrayContaining( [
                { descripcion: 'Credito Bancario' },
                { descripcion: 'Beca Academica' }
            ] ) }
        }
    },
    colegios: {
        query: gql`query colegios( $take: Int $cursor: CursorPaginationInput ){ 
            colegios( take:$take cursor:$cursor ){id codigo nombre municipio{id codigo nombre}} 
        }`,
        variables: { take: 10, cursor:{ id: '60b84f8048b45cffd1d5a7b1' } },
        response: {
            success: { colegios: expect.arrayContaining( [
                expect.objectContaining( { nombre: 'COLEGIO JOSE ANTONIO GALAN' } ),
                expect.objectContaining( { nombre: expect.stringContaining( 'LICEO UNIV' ) } )
            ] ) },
            successPagination: {
                colegios: expect.arrayContaining( [
                    expect.objectContaining( { nombre: expect.stringContaining( 'ESCUELA SAN VICENTE' ) } ),
                    expect.objectContaining( { nombre: expect.stringContaining( 'ESCUELA INTEGRAL DOCE DE OCTUBRE' ) } )
                ] )
            }
        }
    },
    informacionAcademica: {
        context: {
            admin: { id: '623d327beb256ca76f8a9deb', role: Role.ADMIN },
            owner:{ id: '60590812d8e8e5098e3f6ba7', role: Role.GRADUATE },
            other: { id: '60590b7cd8e8e5098e3f6bad', role: Role.GRADUATE },
        },
        query: gql`query academico( $user: UserWhereUniqueInput! ){
            informacionAcademica(where:{user: $user}) {
                id fecha_registro programa{id codigo nombre}
            }
        }`,
        variables: { user: { id: '60590812d8e8e5098e3f6ba7' } },
        response: {
            success: {
                informacionAcademica: expect.objectContaining( { id: '60bbc3a62b35e239abeea971' } )
            },
            errorNotAuthorize: expect.arrayContaining( [ expect.objectContaining( { message: 'User not authorized' } )] ),
        }
    }
}

const mutations = {
    updateInformacionAcademica: {
        context: { admin: { id: '623d327beb256ca76f8a9deb', role: Role.ADMIN } },
        query: gql`mutation updateInformacionAcademica($where: InformacionAcademicaWhereInput! $data: InformacionAcademicaDataInput! ){
            updateInformacionAcademica(where:$where data:$data ){
                id trabajo_grado promedio_notas titulo_trabajo_grado fecha_grado 
                financiacion{descripcion} colegio{ nombre }
            }
        }`,
        variables: {
            where:{
                wrong: { user: { id: '60590b7cd8e8e5098e3f6bad' } },
                right: { user: { id: '60590812d8e8e5098e3f6ba7' } },
            },
            data: {
                trabajo_grado: 'TESIS',
                titulo_trabajo_grado: 'UN APLICATIVO DE BASES DE DATOS NOSQL PARA LA GESTIÓN DE INFORMACIÓN DE EGRESADOS DEL PROGRAMA...',
                financiacion: { id: '60b5a46850751614e2522f91' },
                colegio: { id: '60b84f8048b45cffd1d5a689' },
                fecha_grado: '2021-08-10',
                promedio_notas: 4.3
            }
        },
        response: {
            success: {
                updateInformacionAcademica: expect.objectContaining( {
                    trabajo_grado: 'TESIS',
                    titulo_trabajo_grado: expect.stringContaining( 'UN APLICATIVO DE BASES DE DATOS NOSQL' ),
                    financiacion: { descripcion: 'Beca Academica' },
                    colegio: { nombre: expect.stringContaining( 'LOS MOLINOS' ) },
                    fecha_grado: '2021-08-10',
                    promedio_notas: 4.3
                } )
            },
            errorNotAuthorize: expect.arrayContaining( [ expect.objectContaining( { message: 'User not authorized' } )] )
        }
    }
}

export { queries, mutations }