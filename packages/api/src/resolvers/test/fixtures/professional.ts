import { gql } from 'apollo-server-express'
import { Role } from '../../../utils'

const queries = {
    actividadesActuales: {
        query: gql`query activiades{ actividadesActuales{ descripcion } }`,
        response: {
            success: {
                actividadesActuales: expect.arrayContaining( [
                    { descripcion: expect.stringContaining( 'Emprendimiento' ) },
                    { descripcion: expect.stringContaining( 'Estudios' ) }
                ] )
            }
        }
    },
    canalesComunicacion: {
        query: gql`query canales{ canalesComunicacion{ descripcion } }`,
        response: {
            success: {
                canalesComunicacion: expect.arrayContaining( [
                    { descripcion: expect.stringContaining( 'Redes' ) },
                    { descripcion: expect.stringContaining( 'Paginas Web' ) }
                ] )
            }
        }
    },
    razonesDificultadTrabajo: {
        query: gql`query razones{ razonesDificultadTrabajo{ descripcion } }`,
        response: {
            success: {
                razonesDificultadTrabajo: expect.arrayContaining( [
                    { descripcion: expect.stringContaining( 'Oferta Laboral' ) },
                    { descripcion: expect.stringContaining( 'Mi edad no es admitida' ) }
                ] )
            }
        }
    },
    empresas: {
        query: gql`query empresas($take: Int $cursor: CursorPaginationInput){
            empresas(take: $take cursor: $cursor){
                id nombre telefono correo direccion
            }
        }`,
        variables: {
            take: 5,
            cursor: { id: '60bbe8d6fac3a64dfd21d7eb' }
        },
        response: {
            successOne: {
                empresas: expect.arrayContaining( [
                    expect.objectContaining( { nombre: 'Alkosto' } ),
                    expect.objectContaining( { nombre: 'Wisoky - Nienow' } )
                ] )
            },
            successTwo: {
                empresas: expect.arrayContaining( [
                    expect.objectContaining( { nombre: 'Wisoky - Nienow' } ),
                    expect.objectContaining( { nombre: 'Murray, Rice and Lubowitz' } )
                ] )
            }
        }
    },
    empresa: {
        query: gql`query empresa($id: String!){
            empresa( where:{ id: $id } ){ id nombre direccion telefono correo }
        }`,
        variables: {
            right: { id: '60bbe8d8fac3a64dfd21d801' },
            wrong: { id: '5f8d9498135a086218361fc8' }
        },
        response: {
            success: {
                empresa: expect.objectContaining( {
                    nombre: 'Breitenberg - Purdy', correo: 'Brandyn.Wisoky59@gmail.com'
                } )
            }
        }
    },
    informacionLaboral: {
        context: {
            admin: { id: '623d327beb256ca76f8a9deb', role: Role.ADMIN },
            owner:{ id: '60590926d8e8e5098e3f6ba9', role: Role.GRADUATE },
            other: { id: '60590b7cd8e8e5098e3f6bad', role: Role.GRADUATE },
        },
        query: gql`query laboral($id: String!){
            informacionLaboral(where:{user:{id: $id}}){
                id fecha_registro ocupacion role fecha_inicio municipio{id nombre} empresa{id nombre}
            }
        }`,
        variables: { id: '60590926d8e8e5098e3f6ba9' },
        response: {
            success: {
                informacionLaboral: expect.objectContaining( {
                    id: '60dbdab38dd8cb0034a589fd',
                    fecha_registro: '2021-06-30',
                    municipio: expect.objectContaining( { nombre: 'Suan' } ),
                    empresa: expect.objectContaining( { nombre: 'Alkosto' } ),
                } )
            },
            errorNotAuthorize: expect.arrayContaining( [ expect.objectContaining( { message: 'User not authorized' } )] ),
        }
    },
    informacionNoLaboral: {
        context: {
            admin: { id: '623d327beb256ca76f8a9deb', role: Role.ADMIN },
            owner:{ id: '60590943d8e8e5098e3f6baa', role: Role.GRADUATE },
            other: { id: '60590b7cd8e8e5098e3f6bad', role: Role.GRADUATE },
        },
        query: gql`query noLaboral($id: String!){
            informacionNoLaboral( where:{user:{id:$id}} ){
                id fecha_registro tiempo_busqueda actividad_actual{id descripcion} razon_dificultad{id descripcion}
            }
        }`,
        variables: { id: '60590943d8e8e5098e3f6baa' },
        response: {
            success: {
                informacionNoLaboral: expect.objectContaining( {
                    id: '60dbe2338dd8cb0034a58a00',
                    fecha_registro: '2021-06-30',
                    actividad_actual: expect.objectContaining( { descripcion: 'Emprendimiento personal independiente' } ),
                    razon_dificultad: expect.objectContaining( { descripcion: 'Nula o Poca Experiencia Laboral' } ),
                } )
            },
            errorNotAuthorize: expect.arrayContaining( [ expect.objectContaining( { message: 'User not authorized' } )] ),
        }
    }
}

const mutations = {
    updateInformacionLaboral: {
        context: {
            admin: { id: '623d327beb256ca76f8a9deb', role: Role.ADMIN },
            owner:{ id: '60590926d8e8e5098e3f6ba9', role: Role.GRADUATE },
            other: { id: '60590b7cd8e8e5098e3f6bad', role: Role.GRADUATE },
        },
        query: gql`mutation updateInformacionLaboral( $where: InformacionLaboralWhereInput! $data: UpdateInformacionLaboralDataInput! ){
            updateInformacionLaboral( where:$where data:$data ){ id nombres }
        }`,
        variables: {
            where: { user: { id: '60590926d8e8e5098e3f6ba9' } },
            data: {
                empresa: { id: '60bbe8dbfac3a64dfd21d81d' },
                ocupacion: 'Ingeniero de Sistemas',
                role: 'Backend developer',
                tipo_contrato: 'TERMINO_FIJO',
                salario: 'UNO_DOS_SMLV',
                municipio: { id: '5f8d9aa3135a08621836230c' },
                fecha_inicio: '2018-09-11'
            }
        },
        response: {
            success: {
                updateInformacionLaboral: { id: '60590926d8e8e5098e3f6ba9', nombres: 'Pedro José' }
            },
            successData: {
                informacionLaboral: expect.objectContaining( {
                    fecha_registro: '2021-06-30',
                    ocupacion: 'Ingeniero de Sistemas',
                    role: 'Backend developer', fecha_inicio: '2018-09-11',
                    municipio: { id: '5f8d9aa3135a08621836230c', nombre: 'Santafé de Antioquia' },
                    empresa: { id: '60bbe8dbfac3a64dfd21d81d', nombre: 'Kunde, Ritchie and Welch' }
                } )
            },
            errorNotAuthorize: expect.arrayContaining( [ expect.objectContaining( { message: 'User not authorized' } )] ),
        }
    },
    updateInformacionNoLaboral: {
        context: {
            admin: { id: '623d327beb256ca76f8a9deb', role: Role.ADMIN },
            owner:{ id: '60590812d8e8e5098e3f6ba7', role: Role.GRADUATE },
            other: { id: '60590b7cd8e8e5098e3f6bad', role: Role.GRADUATE },
        },
        query: gql`mutation updateInfoNoLaboral($where:InformacionNoLaboralWhereInput! $data:UpdateInformacionNoLaboralDataInput!){
            updateInformacionNoLaboral(where:$where data:$data){
                id nombres
            }
        }`,
        variables: {
            where: { user: { id: '60590812d8e8e5098e3f6ba7' } },
            data: {
                actividad_actual: { id: '5fd2d3828b8a9e7b0125ac22' },
                razon_dificultad: { id: '5fd2d6fd8b8a9e7b0125ac29' },
                tiempo_busqueda: 10
            }
        },
        response: {
            success: {
                updateInformacionNoLaboral: { id: '60590812d8e8e5098e3f6ba7', nombres: 'José Antonio' }
            },
            successData: {
                informacionNoLaboral: expect.objectContaining( {
                    id: '607e36db2cef640034da2fcf',
                    fecha_registro: '2021-04-20',
                    actividad_actual: expect.objectContaining( { descripcion: 'Emprendimiento personal independiente' } ),
                    razon_dificultad: expect.objectContaining( { descripcion: 'No Hay Oferta Laboral' } ),
                } )
            },
            errorNotAuthorize: expect.arrayContaining( [ expect.objectContaining( { message: 'User not authorized' } )] ),
        }
    }
}

export { queries, mutations }