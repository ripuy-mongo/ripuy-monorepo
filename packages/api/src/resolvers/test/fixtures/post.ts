import { gql } from 'apollo-server-express'
import { Role } from '../../../utils'

const queries = {
    eventos:{
        query: gql`query eventos {
            eventos{ id nombre descripcion poster direccion telefono email url fechas }
        }`,
        response: {
            success: {
                eventos: expect.arrayContaining( [
                    expect.objectContaining( { nombre: expect.stringContaining( 'SpaceApps' ) } ),
                    expect.objectContaining( { nombre: expect.stringContaining( '14CCC' ) } )
                ] )
            }
        }
    },
    evento:{
        query: gql`query event( $id: String! ){
            evento(where:{ id:$id }){ id nombre }
        }`,
        variables: {
            right: { id: '6079d0822edc04605226bfb8' },
            wrong: { id: '40be7bc15821f35cab5c48f1' }
        },
        response: {
            success: {
                evento:{ id: '6079d0822edc04605226bfb8', nombre: expect.stringContaining( '14CCC' ) }
            }
        }
    },
    ofertas: {
        query: gql`query ofertas( $take: Int $cursor: CursorPaginationInput $where:  OfertaWhereInput ){
            ofertas( take: $take cursor: $cursor where: $where ){
                id descripcion telefono correo categoria{id nombre} solicitante{nombre}
            }
        }`,
        variables: {
            take: 10,
            cursor: { id: '60be7bc15821f35cab5c48f4' },
            where: { search: 'Se requiere un ingeniero' }
        },
        response: {
            success: {
                ofertas: expect.arrayContaining( [
                    expect.objectContaining( { solicitante: { nombre: 'McDermott - Moen' } } ),
                    expect.objectContaining( { solicitante: { nombre: 'Lang LLC' } } ) ]
                )
            },
            successPagination: {
                ofertas: expect.arrayContaining( [
                    expect.objectContaining( { solicitante: { nombre: 'Hermiston and Sons' } } ),
                    expect.objectContaining( { solicitante: { nombre: 'Will, Stanton and Roberts' } } )
                ] )
            },
            successSearch: {
                ofertas: [
                    expect.objectContaining( { solicitante: { nombre: 'Lang LLC' } } ),
                    expect.objectContaining( { solicitante: { nombre: 'Wisoky - Nienow' } } )
                ]
            }
        }
    },
    oferta:{
        query: gql`query oferta( $id: String! ){
            oferta( where: { id: $id } ){ id descripcion telefono correo categoria{id nombre} solicitante{nombre} } 
        }`,
        variables: {
            right: { id: '60be7bc15821f35cab5c48f4' },
            wrong: { id: '40be7bc15821f35cab5c48f1' }
        },
        response: {
            success: {
                oferta: expect.objectContaining( { id: '60be7bc15821f35cab5c48f4', solicitante:{ nombre: 'Brekke LLC' } } )
            }
        }
    },
    interesados: {
        query: gql`query interesados( $skip:Int $take:Int $oferta: OfertaWhereUniqueInput! ){
            interesados(skip:$skip take:$take where: { oferta: $oferta } ){
                totalCount interesados{user{ nombres} fecha_aplicacion}
            }
        }`,
        variables: { skip: 2, take: 2, oferta: { id: '60be7bc25821f35cab5c48fd' } },
        response: {
            success: {
                interesados: {
                    totalCount: 10, interesados: expect.arrayContaining( [
                        expect.objectContaining( { user: { nombres: 'Estelle' } } ),
                        expect.objectContaining( { user: { nombres: 'Guido' } } )
                    ] )
                }
            },
            successPagination: {
                interesados: {
                    totalCount: 10, interesados: [
                        expect.objectContaining( { user: { nombres: 'Kory' } } ),
                        expect.objectContaining( { user: { nombres: 'Jaquan' } } )
                    ]
                }
            }
        }
    },
    categorias: {
        query: gql`query categorias{ categorias{ nombre } }`,
        response: {
            success: {
                categorias: expect.arrayContaining( [
                    { nombre: 'Frontend' }, { nombre: 'Redes' }
                ] )
            }
        }
    },
    egresados: {
        query: gql`query egresados($take:Int $cursor:CursorPaginationInput $where: EgresadoWhereInput ){
            egresados(take:$take cursor:$cursor where:$where){ id nombres }
        }`,
        variables: {
            take: 10, cursor:{ id: '60b999b8c6869e0bcde4e6e0' }, where: { search: 'Carlos' }
        },
        response: {
            success: {
                egresados: expect.arrayContaining( [
                    { id: '60b9ac0dc6869e0bcde54b49', nombres: 'Joy' },
                    { id: '60ba369c62fa6533cf36d756', nombres: 'Myrl' }
                ] )
            },
            successPagination: {
                egresados: expect.arrayContaining( [
                    { id: '60b999b8c6869e0bcde4e6e1', nombres: 'Bobbie' },
                    { id: '60b999bac6869e0bcde4e6ea', nombres: 'Erik' },
                ] )
            },
            successSearch: {
                egresados: expect.arrayContaining( [
                    { id: '60ba25f162fa6533cf36832e', nombres: 'Carlos' },
                    { id: '60ba332562fa6533cf36cc92', nombres: 'Carlos' }
                ] )
            }
        }
    },
}

const mutations = {
    createOferta: {
        context: { admin: { id: '623d327beb256ca76f8a9deb', role: Role.ADMIN } },
        query: gql`mutation createOffer($data: CreateOfertaDataInput!){
            createOferta(data: $data){
                id descripcion telefono correo fecha_publicacion categoria{nombre}
                solicitante{ nombre telefono correo }
            }
        }`,
        variables: {
            data: {
                descripcion: 'Test creation offer post', categoria: { id: '5feb3656902621f565d07bfb' },
                telefono: '692.750.3634', correo: 'ladys_Kuhlman@hotmail.com',
                solicitante: {
                    nombre: 'Kshlerin - Goyette', telefono: '692.750.3634', correo: 'ladys_Kuhlman@hotmail.com'
                }
            }
        },
        response: {
            success: {
                createOferta: expect.objectContaining( {
                    descripcion: 'Test creation offer post', categoria: { nombre: 'Frontend' },
                    telefono: '692.750.3634', correo: 'ladys_Kuhlman@hotmail.com',
                    solicitante: {
                        nombre: 'Kshlerin - Goyette', telefono: '692.750.3634', correo: 'ladys_Kuhlman@hotmail.com'
                    }
                } )
            },
            errorNotAuthorize: expect.arrayContaining( [ expect.objectContaining( { message: 'Unauthorized admin user' } )] ),
        }
    },
    updateOferta: {
        query: gql`mutation updateOffer($where:OfertaWhereUniqueInput! $data:UpdateOfertaDataInput! ){
            updateOferta(where:$where data:$data){
                id descripcion telefono correo fecha_publicacion categoria{nombre}
                solicitante{ nombre telefono correo }
            }
        }`,
        variables: {
            where: {
                right: { id: '60be7bc05821f35cab5c48e8' },
                wrong: { id: '5f8d8c5a135a086218361eb5' }
            },
            data: {
                descripcion: 'Updated offer', categoria: { id: '5feb3715902621f565d07bfd' },
                telefono: '777777', correo: 'test@correo.com',
                solicitante: {
                    nombre: 'Tom SAS', telefono: '777777', correo: 'test@correo.com'
                }
            }
        },
        response: {
            success: {
                updateOferta: expect.objectContaining( {
                    descripcion: 'Updated offer', categoria: { nombre: 'Full Stack' },
                    telefono: '777777', correo: 'test@correo.com',
                    solicitante: {
                        nombre: 'Tom SAS', telefono: '777777', correo: 'test@correo.com'
                    }
                } )
            },
            errorNotFound: expect.arrayContaining( [ expect.objectContaining( { message: 'Offer not found' } )] ),
            errorNotAuthorize: expect.arrayContaining( [ expect.objectContaining( { message: 'Unauthorized admin user' } )] ),
        }
    },
    deleteOferta: {
        query: gql`mutation deleteOffer( $where:OfertaWhereUniqueInput! ){
            deleteOferta( where:$where ){ id }
        }`,
        variables: {
            where:{
                right: { id: '60be7bc05821f35cab5c48e8' },
                wrong: { id: '5f8d8c5a135a086218361eb5' }
            }
        },
        response: {
            success: { deleteOferta: { id: '60be7bc05821f35cab5c48e8' } },
            errorNotFound: expect.arrayContaining( [ expect.objectContaining( { message: 'Offer not found' } )] ),
        }
    },
    createInteresado: {
        context: { user: { id: '60b99a1bc6869e0bcde4e8fb', role: Role.GRADUATE } },
        query: gql`mutation createApplied( $data:CreateInteresadoDataInput! ){
            createInteresado(data:$data){ id }
        }`,
        variables: {
            data: {
                right: { oferta: { id: '60be7bc15821f35cab5c48f0' } },
                wrong: { oferta: { id: '5f8d9aa3135a086218362229' } }
            }
        },
        response: {
            success: {
                createInteresado:{ id: expect.any( String ) },
            },
            errorNotFound: expect.arrayContaining( [ expect.objectContaining( { message: 'Offer not found' } )] ),
        }
    },
    deleteInteresado: {
        context: { user:{ id: '60b999b8c6869e0bcde4e6e0', role: Role.GRADUATE } },
        query: gql`mutation deleteApplied( $where:DeleteInteresadoWhereInput! ){
            deleteInteresado( where:$where ){ id }
        }`,
        variables: {
            where: {
                right: { oferta: { id: '60be7bc25821f35cab5c48f9' } },
                wrong: { oferta: { id: '5f8d8c5a135a086218361eb3' } }
            }
        },
        response: {
            success: {
                deleteInteresado: { id: '60be80402a18b75f1007f49f' }
            },
            error: expect.arrayContaining( [ expect.objectContaining( { message: 'Offer not found or user not applied the offer' } )] ),
        }
    }
}

export { queries, mutations }