import { apolloTestClient } from './server/apolloServerTest'
import { queries } from './fixtures/commons'
import {Role, signJwt} from "../../utils";
import {closeConnection, mongoConnection} from "../../models";

const { paises, municipios } = queries
const user = { id: '60590812d8e8e5098e3f6ba7', role: Role.GRADUATE }

beforeAll( async () => {
    await mongoConnection()
})

afterAll( async () => {
    await closeConnection()
} )

describe( '>> Queries simples de listados de datos',  ( ) => {
    let token: string = ''
    let { query } = apolloTestClient( { authorization: token } )

    test( 'Debería retornar un error al pedir los países sin tener session', async( done ) => {
        const response = await query( paises.query )
        expect( response.errors ).toBeUndefined()
        done()
    } )

    test( 'Debería retornar la lista de países', async ( done) => {
        token = signJwt( { id: user.id, role: user.role }, '1h' )
        query = apolloTestClient( { authorization: token } ).query
        const response = await query( paises.query )
        expect( response.data ).toEqual( paises.response.success )
        done()
    } )

    test( 'Debería retornar la lista de municipios', async ( done ) => {
        const response = await query( municipios.query )
        expect( response.data ).toEqual( municipios.response.success )
        done()
    } )
} )