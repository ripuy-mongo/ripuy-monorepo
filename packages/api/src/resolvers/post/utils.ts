import { assign } from 'lodash'
import { Context } from '../../utils'

export const setOfferData = async ( offer: any, { models, session }: Context ) => {
    if( offer ) {
        return assign( offer, {
            categoria: offer.categoria_id,
            aplicado: !!await models.Interesado.findOne( { oferta_id: offer._id, egresado_id: session.userId } )
        } )
    }
}