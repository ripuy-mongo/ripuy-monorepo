import { assign, map, get, filter } from 'lodash'
import { Context, Role } from '../../utils'
import { setOfferData } from './utils'
import { getPaginationParams, SortOrder } from '../utils/pagination'

const categorias = async ( _, _args, { models }: Context ) => {
    return models.Categoria.find( {} )
}
const eventos = async ( _, _args, { models }: Context ) => {
    return models.Evento.find( {} )
}
const evento = async ( _, args, { models }: Context ) => {
    return models.Evento.findById( args.where.id )
}
const ofertas = async ( _, args, { models, session }: Context ) => {
    const { limit, filter: cursor } = getPaginationParams( args, SortOrder.desc )
    let filters = cursor
    const { search, applied } = get( args, `where`, {} )
    if( search )
        filters = assign( filters, { descripcion: { $regex: search } } )
    let ofertas = await models.Oferta.find( filters )
        .sort( { fecha_publicacion: 'desc', _id: 1 } )
        .limit( limit )
        .populate( { path: 'categoria_id', model: 'Categoria' } )
        .populate( { path: 'solicitante_id', model: 'Solicitante' } )
    ofertas = await Promise.all( map( ofertas, async ( oferta ) =>
        await setOfferData( oferta, { models, session } )
    ) )
    return applied ? filter( ofertas, { aplicado: true } ) : ofertas
}
const oferta = async ( _, args, { models, session }: Context ) => {
    const oferta = await models.Oferta.findById( args.where.id )
        .populate( { path: 'categoria_id', model: 'Categoria' } )
        .populate( { path: 'solicitante_id', model: 'Solicitante' } )
    return setOfferData( oferta, { models, session } )
}

const interesados = async ( _, args, { models }: Context ) => {
    const { where, skip, take } = args
    const { oferta } = where
    const total = await models.Interesado.countDocuments( { oferta_id: oferta.id } )
    const interesados = await models.Interesado.find( { oferta_id: oferta.id } )
        .sort( { fecha_aplicacion: 'desc', _id: 1 } )
        .skip( skip ? skip : 0 )
        .limit( take ? take : 0 )
        .populate( { path: 'egresado_id', model: 'User' } )
    return {
        totalCount: total,
        interesados: map( interesados, ( { fecha_aplicacion, egresado_id } ) =>
            ( { fecha_aplicacion, user: egresado_id } )
        )
    }
}
const egresados = async ( _, args, { models }: Context ) => {
    // TODO: validate only admin access
    const { limit, filter } = getPaginationParams( args )
    const filters = filter || {}
    const { search } = get( args, 'where', {} )
    if( search )
        assign( filters, { $or: [ { nombres: { $regex: search } }, { apellidos: { $regex: search } }] } )
    return models.User.find( { role: Role.GRADUATE, ...filters  } )
        .sort( { _id: 1 } )
        .limit( limit )
}

export { eventos, evento, ofertas, oferta, interesados, categorias, egresados }
