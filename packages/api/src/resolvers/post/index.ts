import {
    evento, eventos, ofertas, oferta, interesados, categorias, egresados
} from './queries'
import {
    createInteresado,
    createOferta,
    updateOferta,
    deleteOferta,
    deleteInteresado,
    createEvento,
    updateEvento,
    deleteEvento
} from './mutations'

export default {
    Query:{
        evento, eventos, ofertas, oferta, interesados, categorias, egresados
    },
    Mutation:{
        createOferta,
        updateOferta,
        deleteOferta,
        createInteresado,
        deleteInteresado,
        createEvento,
        updateEvento,
        deleteEvento
    }
}
