import { assign, get, identity, pick, pickBy } from 'lodash'
import { Context, Role } from '../../utils'
import { ApolloError } from 'apollo-server-errors'

export const createOferta = async ( _, args, { session, models }: Context ) => {
    const { solicitante, categoria, descripcion, telefono, correo } = get( args, `data`, {} )
    if( session.role !== Role.ADMIN )
        throw new ApolloError( `Unauthorized admin user`, `402` )
    let oferta = await models.Oferta.create( {
        categoria_id: categoria.id, solicitante, descripcion, telefono, correo
    } )
    oferta = await oferta
        .populate( { path: 'categoria_id', model: 'Categoria' } )
        .execPopulate()
    return assign( oferta, {
        categoria: get( oferta, `categoria_id` ),
    } )
}

export const updateOferta = async ( _, args, { session, models }: Context ) => {
    const { categoria, solicitante, descripcion, telefono, correo } = get( args, `data`, {} )
    const { id } = get( args, `where`, {} )
    if( session.role !== Role.ADMIN )
        throw new ApolloError( `Unauthorized admin user`, `402` )
    const oferta = await models.Oferta.findByIdAndUpdate( id, pickBy( {
        categoria_id: get( categoria, `id` ),
        solicitante, descripcion, telefono, correo
    }, identity ), { new: true } ).populate( { path: 'categoria_id', model: 'Categoria' } )
    if( !oferta )
        throw new ApolloError( `Offer not found`, `403` )
    return assign( oferta, {
        categoria: get( oferta, `categoria_id` ),
    } )
}

export const deleteOferta = async ( _, args, { session, models }: Context ) => {
    const { id } = get( args, `where`, {} )
    if( session.role !== Role.ADMIN )
        throw new ApolloError( `Unauthorized admin user`, `402` )
    const oferta = await models.Oferta.findByIdAndDelete( id )
    if( !oferta )
        throw new ApolloError( `Offer not found`, `403` )
    return oferta
}

export const createInteresado = async ( _, args, { session, models }: Context ) => {
    const { oferta } = get( args, `data`, {} )
    const offer = await models.Oferta.findById( oferta.id )
    if( !offer )
        throw new ApolloError( `Offer not found`, `403` )
    const createdInteresado = await models.Interesado.create( { oferta_id: oferta.id, egresado_id: session.userId } )
    return pick( createdInteresado, ['id'] )
}

export const deleteInteresado = async ( _, args, { session, models }: Context ) => {
    const { oferta } = get( args, `where`, {} )
    const deletedApplied = await models.Interesado.findOneAndDelete( { egresado_id: session.userId, oferta_id: oferta?.id } )
    if( !deletedApplied )
        throw new ApolloError( 'Offer not found or user not applied the offer', '403' )
    return { id: deletedApplied.id }
}

export const createEvento = async ( _, args, { session, models }: Context ) => {
    if( session.role !== Role.ADMIN )
        throw new ApolloError( `Unauthorized admin user`, `402` )
    return await models.Evento.create( get( args, 'data' ) )
}

export const updateEvento = async ( _, args, { session, models }: Context ) => {
    if( session.role !== Role.ADMIN )
        throw new ApolloError( `Unauthorized admin user`, `402` )
    const { id } = get( args, 'where', {} )
    const data = get( args, 'data', {} )
    const event = await models.Evento.findByIdAndUpdate( id, data, { new: true } )
    if ( !event )
        throw new ApolloError( 'Event not found', '403' )
    return event
}

export const deleteEvento = async ( _, args, { session, models }: Context ) => {
    if( session.role !== Role.ADMIN )
        throw new ApolloError( `Unauthorized admin user`, `402` )
    const { id } = get( args, 'where', {} )
    const event = await models.Evento.findByIdAndDelete( id )
    if ( !event )
        throw new ApolloError( 'Event not found', '403' )
    return pick( event, ['id'] )
}
