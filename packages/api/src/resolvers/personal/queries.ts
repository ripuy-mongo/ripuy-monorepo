import { get, map, set, assign } from 'lodash'
import { Context } from '../../utils'
import { validateRole } from '../utils/validateRole'

const clasesEtnograficas = async ( _, _args, { models }: Context ) => {
    return models.ClasEtnografica.find( {} )
}

const claseEtnografica = async ( _, args, { models } ) => {
    return models.ClasEtnografica.findById( args.where.id )
}

const discapacidades = async ( _, _args, { models }: Context ) => {
    const discapacidades = await models.Discapacidad.find( {} ).populate( 'grupo_id' )
    return map( discapacidades, ( disc ) => set( disc, `grupo`, disc.grupo_id ) )
}

const informacionPersonal = async ( _, args, { session, models }: Context ) => {
    const { id } = get( args, `where`, {} )
    await validateRole( session, id, models )
    
    const personal = await models.Personal.findOne( { user_id: id } )
        .populate( { path: `municipio_nacimiento_id`, model: `Municipio` } )
        .populate( { path: `municipio_recidencia_id`, model: `Municipio` } )
        .populate( { path: `clase_etnografica_id`, model: `ClasEtnografica` } )
        .populate( { path: `discapacidad_id`, model: `Discapacidad` } )
    if( personal )
        return assign( personal, {
            municipio_nacimiento: personal?.municipio_nacimiento_id,
            municipio_recidencia: personal?.municipio_recidencia_id,
            clase_etnografica: personal?.clase_etnografica_id,
            discapacidad: personal?.discapacidad_id
        } )
}

const egresado = async ( _, args, { session, models }: Context ) => {
    const { id } = get( args, `where`, {} )
    await validateRole( session, id, models )
    return await models.User.findById( id )
}

export {
    claseEtnografica,
    clasesEtnograficas,
    discapacidades,
    informacionPersonal,
    egresado
}
