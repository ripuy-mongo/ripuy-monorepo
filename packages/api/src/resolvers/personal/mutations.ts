import { assign, get, pickBy, identity } from 'lodash'
import { Context } from '../../utils'
import { validateRole } from '../utils/validateRole'

const getDataParams = ( { where, data } ) => {
    const { user } = where
    const {
        municipio_recidencia, municipio_nacimiento,
        clase_etnografica, tiene_discapacidad, discapacidad, motivo_discapacidad
    } = data
    const params = pickBy( {
        user_id: user.id,
        municipio_nacimiento_id: get( municipio_nacimiento, `id` ),
        municipio_recidencia_id: get( municipio_recidencia, `id` ),
        clase_etnografica_id: get( clase_etnografica, `id` ),
        tiene_discapacidad
    }, identity )
    if ( tiene_discapacidad ){
        assign( params, {
            discapacidad_id: get( discapacidad, `id` ), motivo_discapacidad
        } )
    }
    return params
}

export const updateInformacionPersonal = async ( _, args, { session, models }: Context ) => {
    const { user: userId } = get( args, `where`, {} )
    await validateRole( session, userId.id, models )
    const dataParams = getDataParams( args )
    const { correo, fecha_nacimiento, genero, celular } = get( args, `data`, {} )
    const user = await models.User.findByIdAndUpdate( userId.id, { correo, celular, genero, fecha_nacimiento }, { new: true } )
    const personal = await models.Personal.findOneAndUpdate(  { user_id: user.id }, dataParams, { new: true } )
    if( !personal )
        await models.Personal.create( dataParams )
    return user
}

export const changeFirstLoginStatus = async( _, _args, { session, models }: Context ) => {
    const user = await models.User.findByIdAndUpdate( session.userId, { primer_ingreso: false }, { new: true } )
    return { status: user.primer_ingreso }
}

export const setPermisos = async ( _, args, { session, models }: Context ) => {
    const { allow } = args.data
    const user = await models.User.findOneAndUpdate( { _id: session.userId }, { $set: { permiso: allow } }, { new: true } )
    return { allowed: user.permiso }
}