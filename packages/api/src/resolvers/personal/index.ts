import { clasesEtnograficas, claseEtnografica, discapacidades, informacionPersonal, egresado } from './queries'
import { updateInformacionPersonal, changeFirstLoginStatus, setPermisos } from './mutations'
export default {
    Query: {
        clasesEtnograficas,
        claseEtnografica,
        discapacidades,
        informacionPersonal,
        egresado
    },
    Mutation: {
        updateInformacionPersonal,
        changeFirstLoginStatus,
        setPermisos
    }
}
