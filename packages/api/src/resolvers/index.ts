import personalResolvers from './personal'
import professional from './professional'
import Post from './post'
import Commons from './commons'
import Academic from './academic'
import { DateResolver, DateTimeResolver, EmailAddressResolver, JSONResolver, URLResolver, UUIDResolver } from 'graphql-scalars'

export default [
    { Date: DateResolver },
    { DateTime: DateTimeResolver },
    { Email: EmailAddressResolver },
    { URL: URLResolver },
    { UUID: UUIDResolver },
    { Json: JSONResolver },
    personalResolvers,
    professional,
    Post,
    Commons,
    Academic
]

