import { get, assign, head } from 'lodash'
import { Context } from '../../utils'
import { validateRole } from '../utils/validateRole'
import { getPaginationParams } from '../utils/pagination'

const empresas = async ( _, args, { models }: Context ) => {
    const { limit, filter } = getPaginationParams( args )
    return models.Empresa.find( filter )
        .sort( { _id: 1 } )
        .limit( limit )
}

const empresa = async ( _, args, { models }: Context ) => {
    return models.Empresa.findById( args.where.id )
}

const informacionLaboral = async ( _, args, { session, models }: Context ) => {
    const { user: userId } = get( args, `where`, {} )
    await validateRole( session, userId.id, models )
    const laboral = await models.Laboral.findOne( { user_id: userId.id } )
        .populate( { path: `municipio_id`, model: `Municipio` } )
        .populate( { path: `empresa_id`, model: `Empresa` } )
    if ( laboral )
        return assign( laboral, {
            municipio: laboral?.municipio_id,
            empresa: laboral?.empresa_id
        } )
}

const informacionNoLaboral = async ( _, args, { session, models }: Context ) => {
    const { user: userId } = get( args, `where`, {} )
    await validateRole( session, userId.id, models )
    const noLaboral = await models.NoLaboral.findOne( { user_id: userId.id } )
        .populate( { path: `razon_dificultad_id`, model: `DificultadLaboral` } )
        .populate( { path: `canales_busqueda_id`, model: `CanalComunicacion` } )
        .populate( { path: `actividad_actual_id`, model: `ActividadNoLaboral` } )
    if ( noLaboral )
        return assign( noLaboral, {
            razon_dificultad: noLaboral?.razon_dificultad_id,
            // TODO: renombrar a canales_busqueda o refactorizas no como array en el modelo mongo
            canal_busqueda: head( noLaboral?.canales_busqueda_id ),
            actividad_actual: noLaboral?.actividad_actual_id
        } )
}

const actividadesActuales = async ( _, _args, { models }: Context ) => {
    return models.ActividadNoLaboral.find( {} )
}

const canalesComunicacion = async ( _, _args, { models }: Context ) => {
    return models.CanalComunicacion.find( {} )
}

const razonesDificultadTrabajo = async ( _, _args, { models }:Context ) => {
    return models.DificultadLaboral.find( {} )
}

export { 
    empresas, 
    empresa,
    actividadesActuales, 
    canalesComunicacion, 
    razonesDificultadTrabajo,
    informacionLaboral,
    informacionNoLaboral
}
