import { get } from 'lodash'
import { Context } from '../../utils'
import { getLaboralDataParams, getNoLaboralDataParams } from './utils'
import { validateRole } from '../utils/validateRole'

export const updateInformacionLaboral = async ( _, args, { session, models }: Context ) => {
    const { user } = get( args, `where`, {} )
    if( user.id !== session.userId )
        await validateRole( session, user.id, models )
    const laboral = await models.Laboral.findOneAndUpdate(
        { user_id: user.id }, await getLaboralDataParams( get( args, `data`, {} ), user.id )
    )
    if( !laboral )
        await models.Laboral.create(
            await getLaboralDataParams( get( args, `data`, {} ), user.id )
        )
    return models.User.findById( user.id )
}

export const updateInformacionNoLaboral = async ( _, args, { session, models }: Context ) => {
    const { user } = get( args, `where`, {} )
    if( user.id !== session.userId )
        await validateRole( session, user.id, models )
    const noLaboral = await models.NoLaboral.findOneAndUpdate(
        { user_id: user.id }, await getNoLaboralDataParams( get( args, `data`, {} ), user.id )
    )
    if( !noLaboral )
        await models.NoLaboral.create(
            await getNoLaboralDataParams( get( args, `data`, {} ), user.id )
        )
    return models.User.findById( user.id )
}
