import { get, map, pickBy, identity } from 'lodash'

export const getLaboralDataParams = ( data, userId: string ) => {
    const {
        empresa, ocupacion, role, tipo_contrato, salario, fecha_inicio,
        fecha_finalizacion, municipio, horas_trabajo, grado_satisfaccion
    } = data
    return pickBy( {
        empresa_id: get( empresa, 'id' ), ocupacion, municipio_id: get( municipio, `id` ),
        user_id: userId, role, fecha_inicio, horas_trabajo, tipo_contrato, salario, fecha_finalizacion, grado_satisfaccion,
    }, identity )
}

export const getNoLaboralDataParams = ( data, userId: string ) => {
    const { actividad_actual, canales_busqueda, tiempo_busqueda, razon_dificultad } = data
    return pickBy( {
        actividad_actual_id: get( actividad_actual, 'id' ),
        canales_busqueda_id: map( canales_busqueda, 'id' ),
        razon_dificultad_id: get( razon_dificultad, `id` ),
        user_id: userId,
        tiempo_busqueda,
    }, identity )
}
