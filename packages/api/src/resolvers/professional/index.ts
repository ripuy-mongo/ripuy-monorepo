import {
    empresas,
    empresa,
    actividadesActuales,
    canalesComunicacion,
    razonesDificultadTrabajo,
    informacionLaboral,
    informacionNoLaboral
} from './queries'
import {
    updateInformacionLaboral,
    updateInformacionNoLaboral
} from'./mutations'

export default {
    Query:{
        empresas,
        empresa,
        actividadesActuales,
        canalesComunicacion,
        razonesDificultadTrabajo,
        informacionLaboral,
        informacionNoLaboral
    },
    Mutation:{
        updateInformacionLaboral,
        updateInformacionNoLaboral
    }
}
