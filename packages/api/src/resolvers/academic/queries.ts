import { Context } from '../../utils'
import { AcademicModality, AcademicPeriod, IesCampusType, ProgramTime } from '../../models/academic/utils'
import { get, assign } from 'lodash'
import { validateRole } from '../utils/validateRole'
import { getPaginationParams } from '../utils/pagination'

const sedes = () => IesCampusType
const modalidades = () => AcademicModality
const motivosIngreso = ( _, _args, { models }: Context ) => {
    return models.MotivoIngreso.find( {} )
} 
const periodos = () => AcademicPeriod
const jornadas = () => ProgramTime
const formasFinanciacion = ( _, _args, { models }: Context ) => {
    return models.Financiacion.find( {} )
}
const colegios = async( _, args, { models }: Context ) => {
    const { limit, filter } = getPaginationParams( args )
    return models.Colegio.find( filter )
        .sort( { _id: 1 } )
        .limit( limit )
}
const informacionAcademica = async ( _, args, { session, models }: Context ) => {
    const { user: userId } = get( args, `where`, {} )
    await validateRole( session, userId.id, models )
    const informacion = await models.Academico.findOne( { user_id: userId.id } )
        .populate( { path: 'programa_id', model: 'ProgAcademico' } )
        .populate( { path: 'motivo_ingreso_id', model: 'MotivoIngreso' } )
        .populate( { path: 'financiacion_id', model: 'Financiacion' } )
        .populate( { path: 'colegio_id', model: 'Colegio' } )
    if ( informacion )
        return assign( informacion, {
            programa: informacion.programa_id,
            motivo_ingreso: informacion.motivo_ingreso_id,
            financiacion: informacion.financiacion_id,
            colegio: informacion.colegio_id
        } )
}

export {
    sedes,
    modalidades,
    motivosIngreso,
    periodos,
    jornadas,
    formasFinanciacion,
    colegios,
    informacionAcademica
}