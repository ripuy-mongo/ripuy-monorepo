import { Context } from '../../utils'
import { assign, get, pickBy, identity } from 'lodash'
import { validateRole } from '../utils/validateRole'

const getAcademicData = ( data: any ) => {
    const {
        sede, modalidad_academica, motivo_ingreso, es_primera_opcion,
        intentos_admision, fecha_inicio_programa, fecha_finalizacion_materias,
        fecha_grado, periodo, jornada, promedio_notas, trabajo_grado, titulo_trabajo_grado,
        linea_investigacion, financiacion, colegio
    } = data
    return pickBy( {
        sede, modalidad_academica, es_primera_opcion,
        intentos_admision, fecha_inicio_programa, fecha_finalizacion_materias,
        fecha_grado, periodo, jornada, promedio_notas, trabajo_grado, titulo_trabajo_grado,
        linea_investigacion,
        motivo_ingreso_id: get( motivo_ingreso, 'id' ),
        financiacion_id: get( financiacion, 'id' ),
        colegio_id: get( colegio, 'id' )
    }, identity )
}

const updateInformacionAcademica = async ( _, args, { models, session }: Context ) => {
    const { user: userId } = get( args, `where`, {} )
    await validateRole( session, userId.id, models )
    const academico = await models.Academico.findOneAndUpdate(
        { user_id: userId.id }, getAcademicData( args.data ),
        { new: true } )
        .populate( { path: 'programa_id', model: 'ProgAcademico' } )
        .populate( { path: 'motivo_ingreso_id', model: 'MotivoIngreso' } )
        .populate( { path: 'financiacion_id', model: 'Financiacion' } )
        .populate( { path: 'colegio_id', model: 'Colegio' } )

    if ( !academico )
        throw new Error( 'Not found academic information to user' )
    return assign( academico, {
        programa: academico.programa_id,
        motivo_ingreso: academico.motivo_ingreso_id,
        financiacion: academico.financiacion_id,
        colegio: academico.colegio_id
    } )
}

export { updateInformacionAcademica }