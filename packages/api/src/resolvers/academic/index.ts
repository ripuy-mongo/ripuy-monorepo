import {
    jornadas,
    colegios,
    formasFinanciacion,
    periodos,
    motivosIngreso,
    modalidades,
    sedes,
    informacionAcademica
} from './queries'
import { updateInformacionAcademica } from './mutations'
export default {
    Query:{
        jornadas,
        colegios,
        formasFinanciacion,
        periodos,
        motivosIngreso,
        modalidades,
        sedes,
        informacionAcademica
    },
    Mutation:{
        updateInformacionAcademica
    }
}