import { paises, municipios } from './queries'

export default {
    Query: {
        paises, municipios
    }
}
