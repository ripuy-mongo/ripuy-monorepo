import { Context } from '../../utils'
import { map, get, set } from 'lodash'
import { getPaginationParams } from '../utils/pagination'

const paises = async ( _, _args, { models }: Context ) => {
    return models.Pais.find( {} )
}

const municipios = async ( _, args, { models }: Context ) => {
    const { limit, filter } = getPaginationParams( args )
    const municipios = await models.Municipio.find( filter )
        .sort( { _id: 1 } )
        .limit( limit )
        .populate( {
            path: 'departamento_id',
            model: 'Departamento',
            populate: {
                path: 'pais_id',
                model: 'Pais'
            }
        } )
        .populate( { path: 'region_id', model: 'Region' } )
    return map( municipios, ( municipio ) => {
        if( municipio.departamento_id && municipio.departamento_id.pais_id ) {
            set( municipio, 'departamento', municipio.departamento_id )
            set( municipio.departamento, 'pais', municipio.departamento_id.pais_id )
        }
        if( get( municipio, `region_id` ) )
            set( municipio, 'region', municipio.region_id )
        return municipio
    } )
}

export {
    paises,
    municipios
}
