import { ArgsCursorPagination } from '../../utils'

interface Pagination {
    filter: { _id: any } | undefined;
    limit: number;
}

export enum SortOrder {
    asc = 'asc',
    desc = 'desc',
}

export const getPaginationParams = ( args: ArgsCursorPagination, order: SortOrder = SortOrder.asc ): Pagination => {
    const { take, cursor } = args
    const pagination: Pagination = { filter: undefined, limit: take ? take : 0 }
    if ( cursor && order == SortOrder.desc )
        pagination.filter = { _id: { $lt: cursor.id } }
    else if( cursor )
        pagination.filter = { _id: { $gt: cursor.id } }
    return pagination
}