import { Role, Session } from '../../utils'
import { ApolloError } from 'apollo-server-errors'

export const validateRole = async ( session: Session, userId: number, models ) => {
    const user = await models.User.findById( userId )
    if( !user )
        throw new ApolloError( `User not found`, `404` )
    if( session.userId !== user.id )
        if( session.role !== Role.ADMIN || user.permiso === false )
            throw new ApolloError( `User not authorized`, `402` )
}
