import { ApolloServer } from 'apollo-server-express'
import resolvers from './resolvers'
import schema from './schema'
import { models } from './models'
import { userSession } from './plugins/authentication'
import { ApolloServerExpressConfig } from 'apollo-server-express/src/ApolloServer'
import { Context } from './utils'

const config: ApolloServerExpressConfig = {
    playground: true,
    typeDefs: schema,
    resolvers,
    context: async ( { req } ): Promise<Context> => {
        if( req.method === 'POST' ) {
            return {
                models,
                session: await userSession( req, models )
            }
        }
    }
}
const server = new ApolloServer( config )

export default server
