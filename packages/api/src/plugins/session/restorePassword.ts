import { AWSSendEmail, sendEmail } from '../sendEmail/sendEmail'
import { signJwt, TOKEN_EMAIL_TIME } from '../../utils'

const restorePassword = async ( identification: string, models: any ) => {
    if( !identification )
        throw { statusCode: 400, message: 'Identification not provided' }
    const user = await models.User.findByLogin( identification )
    if( !user )
        throw { statusCode: 404, message: `User not found with identification "${identification}"` }
    const token = signJwt( { id: user.id }, TOKEN_EMAIL_TIME )
    if ( process.env.NODE_ENV === 'production' )
        await AWSSendEmail( user.correo, token )
    else
        await sendEmail( user.correo, token )
    await models.RestorePassword.create( { token, user: user.id } )
}

export { restorePassword }
