import { models } from '../../models'
import { PayloadToken, validatePassword } from '../../utils'

const loginValidation = async ( data: any ): Promise<PayloadToken> => {
    const { password, identification } = data
    if( !password || !identification ) {
        throw { code: 403, message: 'Password and identification are required to start session' }
    }
    const user = await models.User.findByLogin( identification )
    if( !user )
        throw { code: 404, message: 'User not found' }
    const isValid = await validatePassword( password, user.password )
    if( !isValid )
        throw { code: 403, message: 'Invalid password' }
    return {
        id: user.id,
        role: user.role
    }
}

export { loginValidation }
