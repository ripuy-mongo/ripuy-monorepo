import { COOKIE_TIME, PayloadToken, Session, signJwt, verifyJwt } from '../../utils'
import { models } from '../../models'
import { v1 as uuid } from 'uuid'
import moment from 'moment'
import { Request } from 'express'
import { isEmpty, isUndefined } from 'lodash'
import { userSession } from './userSession'
import { AuthenticationError } from 'apollo-server-errors'

const createAuthSession = async ( expiresIn: string, data: PayloadToken ) => {
    const session = await models.AuthSession.find( { user: data.id, expiresIn:{ $gte: moment() } } )
    if( isEmpty( session ) || isUndefined( session ) ){
        const token = signJwt( { id: data.id, role: data.role }, expiresIn )
        const payloadToken = verifyJwt( token )
        return await models.AuthSession.create( {
            user: data.id,
            token,
            expiresIn: moment( payloadToken.exp ),
        } )
    }
    return undefined
}

const createCodeGrant = async ( session: any ) => {
    return await models.AuthCodeGrant.create( {
        code: uuid(),
        expiresIn: moment().add( 2, 'minutes' ),
        session: session.id
    } )
}

const validateAuthSession = async ( token: string ) => {
    const session = await models.AuthSession.find( { token } )
    if( isEmpty( session ) || isUndefined( session ) ){
        throw new AuthenticationError( 'Session not valid' )
    }
}

const updateAuthSession = async( code: string ) => {
    const authCodeGrant = await models.AuthCodeGrant.findOne( {
        code
    } ).populate( 'session' )
    let sessionResponse = undefined
    if( authCodeGrant ){
        const { session }: any = authCodeGrant
        if( session ){
            const user: any = await models.User.findById( session.user )
            const token = signJwt( { id: user.id, role: user.role }, COOKIE_TIME )
            const payloadToken = await verifyJwt( token )
            const response = await models.AuthSession.updateOne(
                { _id: session.id },
                { expiresIn: moment( payloadToken.exp ), token }
            )
            if( response.ok === 1 ){
                sessionResponse = token
            }
        }
    }
    await models.AuthCodeGrant.deleteOne( { code } )
    return sessionResponse
}

const authCloseSession = async ( req: Request ) => {
    const user: Session = await userSession( req,  models )
    if( user ) {
        await models.AuthSession.deleteOne( { token: user.token, user: user.userId } )
    }
    return user
}

export {
    createAuthSession,
    validateAuthSession,
    createCodeGrant,
    updateAuthSession,
    authCloseSession
}
