import { AuthenticationError } from 'apollo-server-express'
import { Request } from 'express'
import { get } from 'lodash'
import { verifyJwt, Session, RIPUY_COOKIE } from '../../utils'
import { validateAuthSession } from './authSession'


/**
 * Función obtener la Cookie almacenada en la petición del servicio
 * @param headers
 * @param cookie
 */
const getCookiesFromHeader = ( headers: any, cookie: string ): any => {
    const cookies: string = get( headers, `cookie` )
    if( cookies ) {
        const match = cookies.match( new RegExp( '(^| )' + cookie + '=([^;]+)' ) )
        return match ? match[2] : null
    }
}

// TODO: create interface to payload
const getTokenPayload = ( req: Request ): any => {
    const { headers } = req
    const token: string = get( headers, `Authorization`, get( headers, `authorization` ) ) as string
    const cookie = getCookiesFromHeader( req.headers, RIPUY_COOKIE )
    if( cookie ){
        return { payload: verifyJwt( cookie ), token: cookie }
    } else if( token ){
        return { payload: verifyJwt( token ), token }
    }
    return { payload: undefined, token }
}

const getUserSession = async( userId: string, models: any ) => {
    const user = await models.User.findById( userId )
    if( !user )
        throw new AuthenticationError( 'User not found' )
    return user
}

const userSession = async ( req: Request, models: any ): Promise<Session> => {
    try {
        const { payload, token } = getTokenPayload( req )
        if( !payload )
            throw new Error( 'Token expired or not provided' )
        if( process.env.NODE_ENV !== 'offline' && process.env.NODE_ENV !== 'test' )
            await validateAuthSession( token )
        const user = await getUserSession( payload.id, models )
        if( user ) {
            return {
                user: user,
                userId: user.id,
                role: user.role,
                token
            }
        }
        return undefined
    } catch ( error ) {
        console.error( error.message )
        return undefined
    }
}

export { userSession }
