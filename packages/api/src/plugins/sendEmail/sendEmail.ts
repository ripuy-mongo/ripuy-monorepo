import { AWS_ACCESS_KEY, AWS_SECRET_ACCESS_KEY, EMAIL, RIPUY_FRONT } from '../../utils'
import { getHtmlEmail } from './template'
import { transporter } from './config'
import AWS from 'aws-sdk'

const SES_CONFIG = {
    accessKeyId: AWS_ACCESS_KEY,
    secretAccessKey: AWS_SECRET_ACCESS_KEY,
    region: 'us-west-1',
}
const AWS_SES = new AWS.SES( SES_CONFIG )

const AWSSendEmail = async ( emailTo: string, token: string  ) => {
    const url: string = `${RIPUY_FRONT}/restore/password?token=${token}`
    const params = {
        Source: EMAIL,
        Destination: {
            ToAddresses: [ emailTo ],
        },
        ReplyToAddresses: [],
        Message: {
            Body: {
                Html: {
                    Charset: 'UTF-8',
                    Data: getHtmlEmail( url )
                },
            },
            Subject: {
                Charset: 'UTF-8',
                Data: `RESTORE PASSWORD SYSTEM RIPUY UDENAR`,
            }
        },
    }
    return AWS_SES.sendEmail( params ).promise()
}

const sendEmail = async ( emailTo: string, token: string ) => {
    const url: string = `${RIPUY_FRONT}/restore/password?token=${token}`
    const options = {
        from: EMAIL, to: emailTo,
        subject: `RESTORE PASSWORD SYSTEM RIPUY UDENAR`,
        html: getHtmlEmail( url )
    }
    await transporter.sendMail( options, ( err: Error ) => {
        if( err ) {
            console.error( err.message )
            throw err
        }
    } )
}

export { sendEmail, AWSSendEmail }
