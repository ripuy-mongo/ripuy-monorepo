import { createTransport } from 'nodemailer'
import { EMAIL, EMAIL_PWD } from '../../utils'

const transporter = createTransport( {
    host: 'smtp.mailtrap.io',
    port: 2525,
    auth: {
        user: EMAIL,
        pass: EMAIL_PWD
    }
} )

export { transporter }

// service: 'gmail',
// host: 'smtp.gmail.com',
