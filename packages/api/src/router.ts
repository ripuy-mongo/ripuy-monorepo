import { Router } from 'express'
import { loginValidation } from './plugins/authentication/loginValidation'
import { generatePasswordHash, PayloadToken, RIPUY_COOKIE, RIPUY_FRONT, Session, TOKEN_TIME, verifyJwt } from './utils'
import { authCloseSession, createAuthSession, createCodeGrant, updateAuthSession } from './plugins/authentication/authSession'
import { get, omit } from 'lodash'
import { userSession } from './plugins/authentication'
import { models } from './models'
import { restorePassword } from './plugins/session/restorePassword'

const router = Router()

/**
 * Route to create session
 * body:
 *  - response_type: "code" value to web
 *  - identification: Id identification in user form login
 *  - password: password of user
 * response:
 *  - code_grant: code of created session
 *  - first_login: if the user has already logged into the application for the first time
 */
router.post( '/login', async ( req, res ) => {
    try {
        const { response_type } = req.body
        const { id, role }: PayloadToken = await loginValidation( req.body )
        if( response_type === 'code' ) {
            const session: any = await createAuthSession( TOKEN_TIME, { id, role } )
            if( session ){
                const codeGrant: any = await createCodeGrant( session )
                if( codeGrant ){
                    const user = models.User.findById( id )
                    res.json( { code: 200, code_grant: codeGrant.code, first_login: !!user.primer_ingreso } )
                    return
                }
            }
        }
        res.status( 400 ).json( { code: 400, message: 'Wrong response type' } )
        return
    } catch ( error ){
        const { code, message } = error
        console.error( message )
        res.status( code ? code : 500 ).json( { code: 500, error: { message } } )
    }

} )

/**
 * Route to set cookie session in client
 * Method: GET
 * params:
 *  - code_grant: code generated in login
 *  - redirect_uri: Url to redirect and set the cookie session
 */
router.get( '/token', async( req, res ) => {
    const { code_grant, redirect_uri }: any = get( req, `query`, {} )
    if( !code_grant ){
        res.status( 400 )
        res.json( { successOne: false, message: 'Invalid code grant' } )
        return
    }
    const sessionToken = await updateAuthSession( code_grant )
    if( sessionToken ){
        const now = new Date()
        now.setTime( now.getTime() + 30 * 24 * 60 * 60 * 1000 ) // days * hours * minutes * seconds * milliseconds
        res.cookie( RIPUY_COOKIE, sessionToken, { expires: now, httpOnly: true, secure: true, sameSite: 'none' } )
        res.redirect( 302, redirect_uri )
        return
    }
    res.status( 400 )
    res.json( { successOne: false, message: `Invalid grant` } )
    return
} )

/**
 * Route to validate user session
 * Method: GET
 * Headers:
 *  - ripuy_app cookie
 * Response:
 *  - user session data
 */
router.get( '/user', async( req, res ) => {
    const user: Session = await userSession( req,  models )
    if( user ){
        res.json( omit( user.user, ['id', 'password'] ) )
        return
    }
    res.status( 401 )
    res.json( { successOne: false, message: `Not authorized` } )
    return
} )

/**
 * Route to clean cookie
 */
router.get( '/logout', async ( req, res ) => {
    await authCloseSession( req )
    res.clearCookie( RIPUY_COOKIE )
    res.redirect( encodeURI( RIPUY_FRONT ) )
    return
} )

/**
 * Route to send restore password email
 */
router.post( '/password/email', async ( req, res ) => {
    try {
        const { identification } = req.body
        await restorePassword( identification, models )
        res.status( 200 )
        res.json( { code: 200, body: 'OK' } ); return
    } catch ( error ){
        const { statusCode, message } = error
        res.status( statusCode ? statusCode : 500 )
        res.json( { error:{ message } } ); return
    }
} )

/**
 * Route that restore a user password
 * headers:
 *  -authorization: token sent in email url
 * body:
 *  - newPassword
 */
router.post( '/password/restore', async ( req, res ) => {
    try {
        const { newPassword, token } = req.body
        const { id }: PayloadToken = verifyJwt( token )
        const user = await models.User.findById( id )
        if( !user ) {
            res.status( 404 ).json( { statusCode: 404, error: `User not found` } )
            return
        }
        const restorePasswordRecord = await models.RestorePassword.findOneAndDelete( { token } )
        if( !restorePasswordRecord ) {
            res.status( 403 ).json( { statusCode: 403, error: 'Malformed or expired token' } )
            return
        }
        const hash = await generatePasswordHash( newPassword )
        await models.User.findByIdAndUpdate( user.id, {  password: hash }, { new: true } )
        // TODO: Close or delete all sessions
        res.status( 200 ).json( { statusCode: 200, message: 'Successful password restore' } )
        return
    } catch ( error ){
        console.error( error.message )
        res.status( 500 ).json( { statusCode: 500, error: error.message } )
        return
    }
} )

router.get( '/', async ( _, res) => {
    return res.status( 200 ).json( 'Hola Mundo!!' )
} )

export default router
