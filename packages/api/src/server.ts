import express from 'express'
import cookie from 'cookie-parser'
import { json, urlencoded } from 'body-parser'
import router from './router'
import server from './graphql'
import { mongoConnection } from './models'
import { includes } from 'lodash'
import { createServer } from 'http'

const PORT = 8180
const allowedOrigins = [
    'https://dev.ripuy.edu.co'
]

const allowedOriginsProd = [
    'https://ripuy.edu.co'
]

const app = express()

server.applyMiddleware( {
    app,
    path: '/graphql',
    cors: {
        credentials: true,
        origin: true
    }
} )

app.use( ( request, response, next ) => {
    const origin = request.headers.origin
    if ( origin && ( origin !== `*` && origin != `null` ) ) {
        const origins = process.env.NODE_ENV === 'production' ? allowedOriginsProd : allowedOrigins
        if ( includes( origins, request.headers.origin ) ) {
            response.header( `Access-Control-Allow-Origin`, request.headers.origin )
        } else {
            response.statusCode = 400
            response.header( `Access-Control-Allow-Origin`, `*` )
            response.json( { successOne: false, message: `Not allow by cors, origin not valid.` } )
        }
    } else {
        response.header( `Access-Control-Allow-Origin`, `*` )
    }
    response.header( `Access-Control-Allow-Credentials`, `true` )
    response.header( `Access-Control-Allow-Headers`, `Origin, X-Requested-With, Content-Type, Accept` )
    next()
    app.options( `*`, ( _, response ) => {
        response.header( `Access-Control-Allow-Methods`, `GET, POST, OPTIONS` )
        response.send()
    } )
} )
app.use( cookie() )
app.use( json() )
app.use( urlencoded( { extended: true } ) )
app.use( router )

const httpServer = createServer( app )

mongoConnection().then( async () => {
    httpServer.listen( { port: PORT }, () =>
        console.log( `Apollo server on http://localhost:${PORT}` )
    )
} ).catch( ( err ) => console.log( err.message ) )
