## Development nginx

```bash
# Create container and start
docker-compose up -d

# Start container
docker start development

# Stop container
docker stop development
```
