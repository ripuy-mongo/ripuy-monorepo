#!/usr/bin/env bash

PATH_BACKUP="../../db/backup"

mongo --version &>/dev/null
if [ $? -lt 0 ]; then
  echo ">> DependenciesError"
  echo "  You must install the mongodb shell locally in order to run the test suite."
  exit 1;
fi

echo ">> Cleaning test db and collections..."
mongo ripuy-test --eval "printjson(db.dropDatabase())"
echo "-------------------------------------"

echo ">> Restoring ripuy-test db locally..."
mongorestore $PATH_BACKUP --gzip &>/dev/null

echo "Restoring ripuy-test db finished successfully."
echo "-------------------------------------"
exit 0