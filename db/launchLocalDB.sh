#!/bin/bash

launch(){
  USERS_EXIST=$(mongo --username $MONGO_INITDB_ROOT_USERNAME --password $MONGO_INITDB_ROOT_PASSWORD --authenticationDatabase admin --eval 'db.User.count()' ripuy-udenar --quiet)

  if [[ $USERS_EXIST -le 0 ]]; then
      restoreDB
  fi
  echo "==> ripuy-udenar db ready."
}


restoreDB(){
  echo "==> Restoring ripu-udenar DB..."
  mongo --username $MONGO_INITDB_ROOT_USERNAME --password $MONGO_INITDB_ROOT_PASSWORD --authenticationDatabase admin ripuy-udenar --eval 'db.dropDatabase()'
  mongorestore --username $MONGO_INITDB_ROOT_USERNAME --password $MONGO_INITDB_ROOT_PASSWORD --authenticationDatabase admin dump/ --gzip
  echo "==> Done!!"
}